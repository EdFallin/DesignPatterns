﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Adapter
{
    /*  this class serves as the internal interface that works with the external of Adaptable; 
     *  I'm pretending that I need numbers as strings all the time, and to get values for shapes  */

    public class AdapterShapes
    {
        int ROUGH_PI = 3;


        /* here is the external class being adapted, retained as an instance for this internal class */
        private ExternalIntegers _mathable = new ExternalIntegers();

        /*  the following methods all convert from the "internal" text representation of numbers to ints, 
         *  then pass those ints to the ExternalIntegers instance _mathable for the actual math  */

        public string Circumference(string radiusText) {
            /* formula: 2(pi)(radius) */

            /* translating, sometimes part of the pattern */
            int radius = int.Parse(radiusText);

            /* delegating operations to capabilities of the external class */
            _mathable.Value = 2;
            _mathable.Multiply(ROUGH_PI);
            _mathable.Multiply(radius);

            string circumference = _mathable.Value.ToString();
            return (circumference);
        }

        public string SquarePerimeter(string sideText) {
            /* formula: 4(side) */

            int side = int.Parse(sideText);

            _mathable.Value = side;

            /* using primitive approach so I can use my .Add() */
            _mathable.Add(side);
            _mathable.Add(side);
            _mathable.Add(side);

            string perimiter = _mathable.Value.ToString();

            return (perimiter);
        }

        public string TriangleArea(string baseText, string heightText) {
            /* formula: (baseSide)(height)/2 */

            int base_ = int.Parse(baseText);
            int height = int.Parse(heightText);

            _mathable.Value = base_;
            _mathable.Multiply(height);
            _mathable.Divide(2);

            string area = _mathable.Value.ToString();

            return (area);
        }

        public string CircleArea(string radiusText) {
            /* formula: (pi)(radius)^2 */

            int radius = int.Parse(radiusText);

            _mathable.Value = radius;
            _mathable.Raise(2);
            _mathable.Multiply(ROUGH_PI);

            string area = _mathable.Value.ToString();

            return (area);
        }

        public string RingArea(string outerRadiusText, string innerRadiusText) {
            /* formula: outer circle's area minus inner's area */

            string outerAreaText = CircleArea(outerRadiusText);
            string innerAreaText = CircleArea(innerRadiusText);

            int outerArea = int.Parse(outerAreaText);
            int innerArea = int.Parse(innerAreaText);

            _mathable.Value = outerArea;
            _mathable.Subtract(innerArea);

            string area = _mathable.Value.ToString();

            return (area);
        }
    }
}
