﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Adapter
{
    /*  this class encapsulates some functionality that my hypothetical internal code needs to adapt; 
     *  it's a very basic wrapper for ~int functionality  */

    public class ExternalIntegers
    {
        public int Value { get; set; }

        public void Add(int operand) {
            Value += operand;
        }

        public void Subtract(int operand) {
            Value -= operand;
        }

        public void Multiply(int operand) {
            Value *= operand;
        }

        public void Divide(int operand) {
            Value /= operand;  // int div returns ints only 
        }

        public void Raise(int operand) {
            // args to .Pow() are upcast silently; hard cast to Value is lossy 
            Value = (int)Math.Pow(Value, operand);
        }
    }
}
