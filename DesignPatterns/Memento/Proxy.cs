﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Memento
{
    /* a Proxy, more or less, used only because C# does not allow 
     * differentiating friend and other classes within the same assembly; 
     * not a native part of the Memento pattern, but used to support that part 
     * of the pattern in which non-topic objects can handle Memento instances¹ */

    public partial class Topic
    {
        public class Proxy
        {
            private Memento _memento = null;

            /* non-Topic code could create a Proxy directly with some nonce Topic, 
             * but that code can't see or use any Memento directly */
            public Proxy(Topic parent) {
                _memento = parent._latestMemento;
            }
        }
    }

    /* ¹see Topic's main file */
}
