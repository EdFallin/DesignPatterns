﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Memento
{
    /* a class that aggregates a Memento object so it can restore a prior state; 
     * embodies a line made up of adjoined shorter straight lines, as a series of 3-d points; 
     * to keep my work simpler, assumed not to cross itself */

    public class PolyLine
    {
        /* the straight lines making this polyline */
        LinkedList<Point> _points = new LinkedList<Point>();

        public Point Start {
            get {
                return (_points.First.Value ?? Point.Empty);
            }
        }

        public Point End {
            get {
                return (_points.Last.Value ?? Point.Empty);  // O(1); list needs it handy 
            }
        }

        public Point this[int offset]  /* verified */  {
            get {
                /* O(n) access of targeted point; fastest possible with linked list */

                LinkedListNode<Point> target = _points.First;

                for (int i = 0; i < offset; i++) {
                    target = target.Next;
                }

                return (target.Value);
            }
        }

        public void Draw(Point point)  /* passed */  {
            _points.AddLast(point);
        }

        public void Redraw(Point start, Point end, List<Point> points) {
            /* draw somewhere within the polyline, replacing some points; 
             * this algo will throw if first or last overall are addressed */

            /* get point range to replace */
            LinkedListNode<Point> node = _points.Find(start);  // O(n), search needed 
            LinkedListNode<Point> before = node.Previous;
            LinkedListNode<Point> after = _points.Find(end).Next;  // O(n), search needed 

            /* get nodes to replace; need nodes here, since I'm avoiding Linq */
            LinkedListNode<Point> next = null;  // set within loop 

            /* remove the old nodes, inclusive of ~start and ~end */
            while (node != after) {
                next = node.Next;  // getting this before delinked 
                _points.Remove(node);
            }

            foreach (Point point in points) {
                _points.AddBefore(after, point);  // ensures order and site 
            }
        }

        public void Redraw(Point past, Point present) {
            /* move a single point within the polyline */
            LinkedListNode<Point> target = _points.Find(past);  // O(n), as must actually be searched 
            target.Value = present;
        }
    }

    //public class Point : IEquatable<Point>
    //{
    //    public int X { get; }
    //    public int Y { get; }
    //    public int Z { get; }

    //    public Point(int x, int y, int z) {
    //        this.X = x;
    //        this.Y = y;
    //        this.Z = z;
    //    }

    //    public static Point Empty {
    //        get {
    //            Point empty = new Point(int.MinValue, int.MinValue, int.MinValue);
    //            return (empty);
    //        }
    //    }

    //    /* implemented for better testing feedback */
    //    public override string ToString()  /* verified */  {
    //        string asString = $"X: { this.X }, Y: { this.Y }, Z: { this.Z }";
    //        return (asString);
    //    }

    //    /* IEquatable<>, and used by override .Equals() */
    //    public bool Equals(Point other)  /* passed */  {
    //        bool doesEqual
    //            = (this.X == other.X)
    //            && (this.Y == other.Y)
    //            && (this.Z == other.Z);

    //        return (doesEqual);
    //    }

    //    /* override with object arg used by operator overrides */
    //    public override bool Equals(object obj)  /* passed */  {
    //        if (!(obj is Point)) {
    //            return (false);
    //        }

    //        return (this.Equals((Point)obj));
    //    }

    //    public override int GetHashCode()  /* passed */  {
    //        /* simple hash coding, ensuring that X, Y, and Z are not interchangeable */
    //        int x = this.X;
    //        int yTo2nd = this.Y * this.Y;
    //        int zTo3rd = this.Z * this.Z * this.Z;

    //        return (x * yTo2nd * zTo3rd);  // overflow --> negative, not exception 
    //    }

    //    /* operator overrides, using override .Equals( object ) implicitly when needed */
    //    public static bool operator ==(Point self, Point other)  /* passed */  {
    //        return (Object.Equals(self, other));
    //    }

    //    public static bool operator !=(Point self, Point other)  /* passed */  {
    //        return (!Object.Equals(self, other));
    //    }
    //}
}
