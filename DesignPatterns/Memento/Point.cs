﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Memento
{
    public class Point : IEquatable<Point>
    {
        public int X { get; }
        public int Y { get; }
        public int Z { get; }

        public Point(int x, int y, int z) {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public static Point Empty {
            get {
                Point empty = new Point(int.MinValue, int.MinValue, int.MinValue);
                return (empty);
            }
        }

        /* implemented for better testing feedback */
        public override string ToString()  /* verified */  {
            string asString = $"X: { this.X }, Y: { this.Y }, Z: { this.Z }";
            return (asString);
        }

        /* IEquatable<>, and used by override .Equals() */
        public bool Equals(Point other)  /* passed */  {
            bool doesEqual
                = (this.X == other.X)
                && (this.Y == other.Y)
                && (this.Z == other.Z);

            return (doesEqual);
        }

        /* override with object arg used by operator overrides */
        public override bool Equals(object obj)  /* passed */  {
            if (!(obj is Point)) {
                return (false);
            }

            return (this.Equals((Point)obj));
        }

        public override int GetHashCode()  /* passed */  {
            /* simple hash coding, ensuring that X, Y, and Z are not interchangeable */
            int x = this.X;
            int yTo2nd = this.Y * this.Y;
            int zTo3rd = this.Z * this.Z * this.Z;

            return (x * yTo2nd * zTo3rd);  // overflow --> negative, not exception 
        }

        /* operator overrides, using override .Equals( object ) implicitly when needed */
        public static bool operator ==(Point self, Point other)  /* passed */  {
            return (Object.Equals(self, other));
        }

        public static bool operator !=(Point self, Point other)  /* passed */  {
            return (!Object.Equals(self, other));
        }
    }
}
