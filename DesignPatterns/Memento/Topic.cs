﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Memento
{
    /* a persistent object that goes through many state changes */

    /* only Topic should know about contents of a Memento; in C# the only simple way 
     * to do this is make sure both are in a separate assembly from consuming code */

    public class Topic
    {
        #region Definitions

        public const string Zero = "Zero";
        public const string One = "One";
        public const string Two = "Two";
        public const string Four = "Four";
        public const string Eight = "Eight";
        public const string Sixteen = "Sixteen";
        public const string ThirtyTwo = "ThirtyTwo";
        public const string SixtyFour = "SixtyFour";
        public const string OneTwentyEight = "OneTwentyEight";

        #endregion Definitions


        #region Fields

        Material _state = 0;
        string[] _flagWords = null;

        #endregion Fields


        #region Properties

        /* Memento of the latest state change only */
        public TopicMemento Memento { get; private set; } = null;

        #endregion Properties


        #region Constructors

        public Topic() {
            _flagWords = new string[] {
                Zero, One, Two, Four, Eight,
                Sixteen, ThirtyTwo, SixtyFour, OneTwentyEight
            };
        }

        #endregion Constructors


        #region Methods

        public int Add(string name) {
            if (!_flagWords.Contains(name)) {
                throw new ArgumentException("Input must be one of the predefined values.", nameof(name));
            }

            /* convert input */
            Material flag = (Material)Enum.Parse(typeof(Material), name);

            /* save change for reversing */
            TopicMemento memento = new TopicMemento(TopicMemento.AddSubtract.Added, flag);
            this.Memento = memento;

            /* alter object state, convert it, output it */
            _state |= flag;  // alter and pass back 

            return ((int)_state);
        }

        public int Subtract(string name) {
            if (!_flagWords.Contains(name)) {
                throw new ArgumentException("Input must be one of the predefined values.", nameof(name));
            }

            /* convert input */
            Material flag = (Material)Enum.Parse(typeof(Material), name);

            /* save change for reversing */
            TopicMemento memento = new TopicMemento(TopicMemento.AddSubtract.Subtracted, flag);
            this.Memento = memento;

            /* alter object state, convert it, output it; 
             * removing nondestructively of other bits must be ~ then & */
            flag = ~flag;
            _state &= flag;  // alters and pass back 

            return ((int)_state);
        }

        public int Restore(DateTime through, List<TopicMemento> mementi) {
            /* reverse state Memento by Memento, using opposite of method from original change */

            /* get Mementos after ~through */
            List<TopicMemento> reversibles = mementi
                .Where(x => (x.Moment > through))
                .ToList();

            /* reverse Mementos backwards until _state is reverted to targeted point; 
             * this also produces new Mementos each time, which here are ignored */
            for (int i = reversibles.Top(); i >= 0; i--) {
                TopicMemento reversible = reversibles[i];

                if (reversible.Added != 0) {
                    string asName = Enum.GetName(typeof(Material), reversible.Added);
                    this.Subtract(asName);
                }
                else if (reversible.Subtracted != 0) {
                    string asName = Enum.GetName(typeof(Material), reversible.Subtracted);
                    this.Add(asName);
                }
            }

            /* convey */
            return ((int)_state);
        }

        #endregion Methods
    }
}
