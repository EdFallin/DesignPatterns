﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Memento
{
    /* Memento is an object that preserves state of Topic at a given, 
     * or information to restore that state; here, the latter */

    /* only Topic should know about contents of a Memento; in C# the only simple way 
     * to do this is make sure both are in a separate assembly from consuming code */

    public class Memento
    {
        public enum AddSubtract
        {
            Added,
            Subtracted,
        }

        internal Flaggables Added { get; set; } = 0;
        internal Flaggables Subtracted { get; set; } = 0;

        public DateTime Moment { get; private set; }

        public Memento(AddSubtract addSubtract, Flaggables changed) {
            if (addSubtract == AddSubtract.Added) {
                this.Added = changed;
            }
            else {
                this.Subtracted = changed;
            }

            this.Moment = DateTime.Now;
        }
    }
}
