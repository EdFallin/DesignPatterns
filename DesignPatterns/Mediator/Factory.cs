﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Mediator
{
    public class Factory : AColleague
    {
        public Factory(string name) : base(name) {
            /* no operations */
        }

        public void Produce(string product, int number) {
            this.State = $"{ this.Name } produced { number } { product }{ (number != 1 ? S : NIL) }";

            /* more state */
            this.Item = product;
            this.Number = number;

            /* the Mediator pattern; colleague informs its retained mediator object when its state changes */
            this.Mediator.StateChanged(this);
        }
    }
}
