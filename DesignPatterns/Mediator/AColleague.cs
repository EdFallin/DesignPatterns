﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Mediator
{
    /* in the Mediator pattern, this is the abstract base class for all colleague 
     * classes whose interactions are handled by the mediator class */

    public abstract class AColleague
    {
        #region Definitions

        /* making my life easier in text-based implemn */

        public const string S = "s";
        public readonly string NIL = string.Empty;

        #endregion Definitions

        /* in the Mediator pattern, this is the reference to the mediator object 
         * that colleague classes use to inform the colleage about their state */
        public AMediator Mediator { get; set; }


        #region Local properties

        /* in my simple learning implemn, this provides info for state texts */
        public string Name { get; set; }

        /* State is whatever the colleague is set to be by consumer code or by 
         * the mediator in response to changes on other colleagues */
        public string State { get; set; }

        /* these elements are more state, which may get used in interactions with other colleagues */
        public string Item { get; set; }

        public int Number { get; set; }

        /* an accumulator of past states and their origins, to help demonstrate interactions */
        public string History { get; set; }

        #endregion Local properties


        #region Constructors

        public AColleague(string name) {
            this.Name = name;
        }

        #endregion Constructors

    }
}