﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Mediator
{
    public class CommerceMediator : AMediator
    {
        #region Fields: colleagues

        /* the colleagues whose interactions are mediated; in my implemn, mediator class is not instantiating colleagues */

        List<Store> _stores = new List<Store>();
        List<Warehouse> _warehouses = new List<Warehouse>();
        List<Factory> _factories = new List<Factory>();

        #endregion Fields: colleagues


        #region Properties: colleagues

        /* although it's not spelled out in the reftext, if your mediator isn't initing the colleagues, then when they are 
         * linked to the mediator, their references to it must also be set */

        /* as a half-assed approach to this since learnings, I'm exposing my List<>s to consumers as arrays 
         * so they can't be added to, and exposing add-item methods on the instance instead */

        public Store[] Stores {
            get {
                return (_stores.ToArray());
            }
        }

        public Warehouse[] Warehouses {
            get {
                return (_warehouses.ToArray());
            }
        }

        public Factory[] Factories {
            get {
                return (_factories.ToArray());
            }
        }

        #endregion Properties: colleagues


        #region Methods: adding colleagues

        public void AddStore(Store store) {
            store.Mediator = this;
            _stores.Add(store);
        }

        public void AddWarehouse(Warehouse warehouse) {
            warehouse.Mediator = this;
            _warehouses.Add(warehouse);
        }

        public void AddFactory(Factory factory) {
            factory.Mediator = this;
            _factories.Add(factory);
        }

        #endregion Methods: adding colleagues


        /* Mediator pattern: this method responds to state changes of colleagues 
         * by changing state on others, or invoking state-changing methods on them */

        public override void StateChanged(AColleague colleague) {
            /* stores interact directly with warehouses, and warehouses with factories, but effects may propagate; 
             * each type of interaction handled with its own method, invoked based on arg type */

            List<Type> types = new List<Type> { typeof(Store), typeof(Warehouse), typeof(Factory) };
            List<Action<AColleague>> responders = new List<Action<AColleague>>
                { this.OnStoreChange, this.OnWarehouseChange, this.OnFactoryChange };  // Action is a generic delegate type 

            /* find the right method and invoke it */
            for (int i = 0; i < types.Count; i++) {
                if (colleague.GetType() == types[i]) {
                    responders[i].Invoke(colleague);
                }
            }
        }

        /* Mediator pattern: the following three methods are factorings of the various interactions; 
         * the interactions are both state and method calls, which seems about right; 
         * the mediator's requesting more info from the originating colleague is 
         * implemented in effect by the mediator using colleague properties */

        /* placing the history steps before the Mediator pattern state ops to show the transitions that result in those states; 
         * history steps follow Mediator pattern, but are not current state, which is probably normal focus of the pattern */

        private void OnFactoryChange(AColleague colleague) {
            Factory factory = colleague as Factory;

            foreach (Warehouse warehouse in this.Warehouses) {
                /* for demonstration */
                warehouse.History += ON + factory.State + INTER;
                warehouse.History = warehouse.History.Trim();

                /* Mediator pattern */
                warehouse.Receive(factory.Item, factory.Number);
            }
        }

        private void OnWarehouseChange(AColleague colleague) {
            /* type of warehouse action causes actions on different sets of colleagues */

            Warehouse warehouse = colleague as Warehouse;

            if (warehouse.State.Contains("received")) {
                foreach (Factory factory in this.Factories) {
                    /* for demonstration */
                    factory.History += ON + warehouse.State + INTER;
                    factory.History = factory.History.Trim();

                    /* Mediator pattern */
                    factory.State = $"{ factory.Name } ready to make { warehouse.Number } { warehouse.Item }{ (warehouse.Number != 1 ? S : NIL) }";
                }
            }

            if (warehouse.State.Contains("shipped")) {
                foreach (Store store in this.Stores) {
                    /* for demonstration */
                    store.History += ON + warehouse.State + INTER;
                    store.History = store.History.Trim();

                    /* Mediator pattern */
                    store.State = $"{ store.Name } receiving { warehouse.Number } { warehouse.Item }{ (warehouse.Number != 1 ? S : NIL) }";
                }
            }
        }

        private void OnStoreChange(AColleague colleague) {
            Store store = colleague as Store;

            foreach (Warehouse warehouse in this.Warehouses) {
                /* for demonstration */
                warehouse.History += ON + store.State + INTER;
                warehouse.History = warehouse.History.Trim();

                /* Mediator pattern */
                warehouse.Ship(store.Item, store.Number);
            }
        }

    }
}
