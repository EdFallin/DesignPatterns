﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Mediator
{
    /* a Warehouse is a mediated object; none of its methods refer to a Store */

    public class Warehouse : AColleague
    {
        public Warehouse(string name) : base(name) {
            /* no operations */
        }

        public void Receive(string product, int number) {
            this.State = $"{ this.Name } received { number } { product }{ (number != 1 ? S : NIL) }";

            /* more state */
            this.Item = product;
            this.Number = number;

            /* the Mediator pattern */
            this.Mediator.StateChanged(this);
        }

        public void Ship(string product, int number) {
            this.State = $"{ this.Name } shipped { number } { product }{ (number != 1 ? S : NIL) }";

            /* more state */
            this.Item = product;
            this.Number = number;

            /* the Mediator pattern */
            this.Mediator.StateChanged(this);
        }
    }
}
