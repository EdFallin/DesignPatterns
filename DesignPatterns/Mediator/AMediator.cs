﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Mediator
{
    /* in the Mediator design pattern, this is the (optional) abstract superclass for the mediator themselves, 
     * each concrete subclass of which controls interactions and state among all stateful mediated objects */

    public abstract class AMediator
    {
        #region Definitions

        /* making my life easier in text-based implemn */

        public const string S = "s";
        public readonly string NIL = string.Empty;
        public const string ON = " On: ";
        public const string INTER = ";";

        #endregion Definitions


        /* in the Mediator pattern, this method is the crux of the mediator class' surface, and possibly all it exposes; 
         * in response to information provided here, it internally acts on colleague objects via their surfaces */
        public abstract void StateChanged(AColleague colleague);
    }

}
