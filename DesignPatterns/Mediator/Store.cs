﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Mediator
{
    /* Store is one of the mediated objects; none of its methods refer to a Warehouse */

    public class Store : AColleague
    {
        public Store(string name) : base(name) {
            /* no operations */
        }

        public void Sell(string product, int number) {
            this.State = $"{ this.Name } sold { number } { product }{ (number != 1 ? S : NIL) }";

            /* more state */
            this.Item = product;
            this.Number = number;

            /* the Mediator pattern: alert the mediator instance, which implements consequent interactions */
            this.Mediator.StateChanged(this);
        }
    }
}