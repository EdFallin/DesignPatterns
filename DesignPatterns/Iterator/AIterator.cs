﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Iterator
{
    /* in the Iterator design pattern, this class supports the iteration operations and retains 
     * a reference to the iterated class, which in turn provides this class to iteration consumers */

    public abstract class AIterator<T>
    {
        /* no internals, because implementation may be heavily concrete-class specific */


        /* these 3 methods allow an iteration loop of this general form: 
         *     while HasMore(), MoveNext(), then look at Current() */

        public abstract bool HasMore();

        public abstract void MoveToNext();

        public abstract T Current();

        /* optional method, but useful for repeat iteration */
        public abstract void Reset();

    }
}
