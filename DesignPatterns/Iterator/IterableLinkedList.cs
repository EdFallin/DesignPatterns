﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Iterator
{
    /* a primitive version of a linked list, implementing only what's needed for these learnings */

    public class IterableLinkedList<T> : AIterable<T>
    {
        Node<T> _first;
        Node<T> _last;

        public IterableLinkedList(T first) {
            _last = _first = new Node<T>(first);  // head and tail of list are the same here 
        }

        public void Add(T content) {
            /* linking and redefining tail for next Add() */
            _last.Next = new Node<T>(content);
            _last = _last.Next;
        }

        /* to allow iterator to iterate; for my minimum implemn, should be enough */
        public Node<T> First {
            get {
                return (_first);
            }
        }

        /* the only method specifically required for Iterator pattern */
        public override AIterator<T> GetIterator() {
            return (new LinkedListIterator<T>(this));
        }

    }

    /* very limited node class, for singly-linked list; if this were defined as 
     * internal to the linked list<T>, <T> would not be necessary in node header 
     * even though used in definition, because defined in encompassing scope */

    public class Node<T>
    {
        public Node<T> Next { get; set; }
        public T Content { get; set; }

        public Node(T content) {
            this.Content = content;
        }
    }
}
