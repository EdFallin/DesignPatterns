﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Iterator
{
    public class ArrayIterator<T> : AIterator<T>
    {
        IterableArray<T> _iterableArray;
        int _at;

        public ArrayIterator(IterableArray<T> iterableArray) {
            _iterableArray = iterableArray;
            _at = 0;  // at first element; thus exact equivalent to handling in linked list 
        }

        /* these 3 implemns, along with initing _at to 0 in ctor, can be used to 
         * perform a loop over the collection until it is empty */

        public override T Current() {
            return (_iterableArray[_at]);
        }

        public override bool HasMore() {
            return (_at < _iterableArray.Count);
        }

        public override void MoveToNext() {
            _at++;
        }

        /* quasi-optional, but needed in the absence of other state */
        public override void Reset() {
            _at = 0;
        }
    }
}
