﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Iterator
{
    /* in the Iterator pattern, this is the root class for all objects 
     * that can be iterated, matched to an abstract iterator class */

    public abstract class AIterable<T>
    {
        /* returns the iterator class; abstract, not virtual, because 
         * concretes must be used in each iterable-iterator pairing */
        public abstract AIterator<T> GetIterator();
    }
}
