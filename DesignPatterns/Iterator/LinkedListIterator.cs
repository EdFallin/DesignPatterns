﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Iterator
{
    public class LinkedListIterator<T> : AIterator<T>
    {
        IterableLinkedList<T> _list;
        Node<T> _current;

        public LinkedListIterator(IterableLinkedList<T> list) {
            _list = list;
            _current = _list.First;  // at first element 
        }


        public override T Current() {
            return (_current.Content);
        }

        public override bool HasMore() {
            /* Current() called _after_ HasMore(), so this and _not_ .Next 
             * of null, which would cause skip of last element */
            return (_current != null);
        }

        public override void MoveToNext() {
            _current = _current.Next;
        }

        /* quasi-optional, but needed in the absence of other state */
        public override void Reset() {
            _current = _list.First;
        }
    }
}
