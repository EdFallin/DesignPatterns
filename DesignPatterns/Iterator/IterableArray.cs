﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Iterator
{
    /* a primitive list-type version of an array, without full functionality, since not needed here */

    public class IterableArray<T> : AIterable<T>
    {
        T[] _array = null;

        /* in a library implemn, the two properties could be defined as 
         * 'internal' if I didn't want other classes direct array access */

        public T this[int at] {
            /* trapping omitted */
            get { return (_array[at]); }
            set { _array[at] = value; }
        }

        public int Count {
            get {
                return (_array.Length);
            }
            /* no setter; passer */
        }

        public IterableArray(int size) {
            _array = new T[size];
        }

        /* Iterator pattern implementation; iterator class must have instance of iterable to iterate over */
        public override AIterator<T> GetIterator() {
            return (new ArrayIterator<T>(this));
        }
    }
}
