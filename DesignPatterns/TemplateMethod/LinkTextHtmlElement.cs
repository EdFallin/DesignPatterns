﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.TemplateMethod
{
    /* in the Template Method pattern, this is one of the concrete classes which implements the abstract step methods 
     * of the template method, causing that method when called to produce their own specific version of the output */
    public class LinkTextHtmlElement : AHtmlElement
    {
        #region Definitions

        const string LINK_TAG = "a";  // 'a' == "anchor" 
        const string HREF = "href";

        #endregion Definitions


        #region Fields

        string _url;
        string _text;

        #endregion Fields


        #region Constructors

        public LinkTextHtmlElement(string id, string linkText, string url) : base(LINK_TAG, id, false) {  // last arg == "not self-closing" 
            _url = url;
            _text = linkText;
        }

        #endregion Constructors


        #region Overrides of step methods: Template Method pattern

        protected override List<Duple> Attributes() {
            List<Duple> attributes = new List<Duple>();
            attributes.Add(new Duple(HREF, _url));

            return (attributes);
        }

        protected override string Content() {
            return (_text);
        }

        /* Template Method: for differentiation, links won't allow applying any styles */
        protected override string Styles() {
            return (null);
        }

        #endregion Overrides of step methods: Template Method pattern
    }
}
