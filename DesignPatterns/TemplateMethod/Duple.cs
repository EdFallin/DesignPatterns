﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.TemplateMethod
{
    /// <summary>
    /// A mutable alternative to KeyValuePair<> and Duple<>.
    /// </summary>
    public class Duple
    {
        public string Key { get; set; }
        public string Value { get; set; }

        public Duple(string key, string value) {
            this.Key = key;
            this.Value = value;
        }

        public override string ToString() {
            string asString = $"{ this.Key.ToLower() }=\"{ this.Value }\"";
            return (asString);
        }
    }
}
