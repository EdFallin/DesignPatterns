﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.TemplateMethod
{
    public class InputTextHtmlElement : AHtmlElement
    {
        #region Definitions

        const string TAG = "input";
        const string TYPE = "type";
        const string TEXT = "text";
        const string NAME = "name";    // semi-required for input elements; allows server to easily digest returns 
        const string CLASS = "class";  // optional, but used for DOM traversal, applying CSS, etc. 

        #endregion Definitions


        #region Fields

        string _name;
        string _class;

        #endregion Fields


        #region Constructors

        public InputTextHtmlElement(string id, string name, string @class = null) : base(TAG, id, true) {  // @class allows use of keyword; last arg = "_is_ self-closing" 
            _name = name;
            _class = @class;
        }

        #endregion Constructors


        #region Overrides of step methods: Template Method pattern

        protected override List<Duple> Attributes() {
            List<Duple> attributes = new List<Duple>();
            attributes.Add(new Duple(TYPE, TEXT));
            attributes.Add(new Duple(NAME, _name));

            /* optional attribute */
            if (!string.IsNullOrWhiteSpace(_class)) {
                attributes.Add(new Duple(CLASS, _class));
            }

            return (attributes);
        }

        /* no text in input elements, at least not normally */
        protected override string Content() {
            return (null);
        }

        /* Template Method: skipping for differentiation; let's assume styles come from class */
        protected override string Styles() {
            return (null);
        }

        #endregion Overrides of step methods: Template Method pattern
    }
}
