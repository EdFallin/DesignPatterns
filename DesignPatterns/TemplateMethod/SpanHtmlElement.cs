﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.TemplateMethod
{
    /* in the Template Method pattern, this is another concrete class that implements 
     * step methods so the template method produces particular results; 
     * this class is for <Span>...<> blocks, so it focuses on styles */

    public class SpanHtmlElement : AHtmlElement
    {
        #region Definitions

        const string SPAN_TAG = "span";

        #endregion Definitions


        #region Fields

        string _text;
        string[] _styles;

        #endregion Fields


        #region Constructors

        public SpanHtmlElement(string id, string text, string[] styles) : base(SPAN_TAG, id, false) {  // last arg = "not self-closing" 
            _text = text;
            _styles = styles;
        }

        #endregion Constructors


        #region Overrides of step methods: Template Method pattern

        /* Template Method: for differentiation, no attributes apart from styles, found later here */
        protected override List<Duple> Attributes() {
            return (new List<Duple>());  // not null 
        }

        protected override string Content() {
            return (_text);
        }

        /* Template Method: for differentiation, defining styles for this concrete subclass only */
        protected override string Styles() {
            /* from an array to a delimited list; styles assumed already to be in 'key:value' syntax, but without ';' delimiters */
            string asString = string.Join("; ", _styles);
            return (asString);
        }

        #endregion Overrides of step methods: Template Method pattern
    }
}
