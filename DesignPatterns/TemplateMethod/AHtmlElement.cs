﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.TemplateMethod
{
    /* in the Template Method pattern, this is the class which defines the template method, 
     * both its basic structure and its abstract step methods */

    public abstract class AHtmlElement
    {
        #region Properties

        public string Tag { get; protected set; }
        public string Id { get; set; }
        protected bool IsSelfClosing { get; set; } = false;

        #endregion Properties


        #region Constructors

        public AHtmlElement(string tag, string id, bool isSelfClosing) {
            this.Tag = tag;
            this.Id = id;
            this.IsSelfClosing = isSelfClosing;
        }

        #endregion Constructors


        #region Public methods: the Template Method itself

        /* Template Method: the template method itself, more or less returning an Html tag pair with contents */
        virtual public string MakeElement() {
            /* defining a well-formed HTML tag, more or less, in its basic two flavors, [tag pair with content between] and [self-closing tag]; 
             * subclasses can define certain aspects peculiar to their particular type of tag, using the methods provided, as given by the pattern */

            StringBuilder element = new StringBuilder();

            element.Append(this.OpenTagStart());
            element.Append(this.Tag);
            element.Append($@" id=""{ this.Id }""");

            string styles = this.Styles();
            if (!string.IsNullOrWhiteSpace(styles)) {
                element.Append($@" style=""{ styles }""");
            }

            List<Duple> attributes = this.Attributes();
            string attribsAsString = AttributesToString(attributes);

            if (!string.IsNullOrWhiteSpace(attribsAsString)) {
                element.Append($@" { attribsAsString }");
            }

            /* ugly non-optimal, but perhaps best for illustrating Template Method concept */
            if (!this.IsSelfClosing) {
                element.Append(this.OpenTagEnd());

                element.Append(this.Content());

                element.Append(this.CloseTagStart());
                element.Append(this.Tag);
                element.Append(this.OpenTagEnd());
            }
            else {
                element.Append(this.SelfCloseEnd());
            }


            return (element.ToString());
        }

        #endregion Public methods: the Template Method itself


        #region Private & protected methods: concrete, or abstract for subclasses to override

        protected string OpenTagStart() {
            return ("<");
        }

        /* Template Method: a method to be overridden as needed by concrete subclasses for specific implemns of Html element */
        protected abstract string Styles();

        /* Template Method: a method to be overridden as needed by concrete subclasses for specific implemns of Html element */
        protected abstract string Content();

        protected string CloseTagStart() {
            return ("</");
        }

        protected string SelfCloseEnd() {
            return (" />");
        }

        protected string OpenTagEnd() {
            return (">");
        }

        /* Template Method: a method to be overridden as needed by concrete subclasses for specific implemns of Html element */
        protected abstract List<Duple> Attributes();

        private string AttributesToString(List<Duple> attributes) {
            /* no null-guarding here; subclass is assumed to return a valid List<>, perhaps empty */

            string asString = string.Join(" ", attributes);  // calls .ToString() on each object 
            return (asString);
        }

        #endregion Private & protected methods: concrete, or abstract for subclasses to override

    }
}
