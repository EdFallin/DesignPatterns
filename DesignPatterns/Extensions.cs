﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
    public static class Extensions
    {
        public static int Top<T>(this List<T> subject)  /* verified */  {
            return (subject.Count - 1);
        }

        public static int Top(this string subject)  /* verified */  {
            return (subject.Length - 1);
        }

        public static int Top(this StringBuilder subject)  /* verified */  {
            return (subject.Length - 1);
        }

        public static string AppendLine(this string subject, string text)  /* verified */  {
            string newLine = Environment.NewLine;
            subject += (text + newLine);
            return (subject);
        }

        public static byte[] ToBytes(this string subject)  /* verified */  {
            /* standard string-to-bytes conversion, in 3 steps: 
             *     string --> char[] with .ToCharArray(); 
             *     new byte[] of [char[].Length *2] (2 bytes to a char); 
             *     Buffer.BlockCopy() of char[] to byte[] */

            char[] asChars = subject.ToCharArray();
            byte[] asBytes = new byte[asChars.Length * 2];
            Buffer.BlockCopy(asChars, 0, asBytes, 0, asBytes.Length);

            return (asBytes);
        }

        public static string AsString(this byte[] subject)  /* verified */  {
            char[] asChars = new char[subject.Length / 2];  // assumed an even length 
            Buffer.BlockCopy(subject, 0, asChars, 0, subject.Length);

            string asString = new string(asChars);
            return (asString);
        }

        public static string AsString(this object subject)  /* verified */  {
            /* try as string, try as byte[] */

            /* try as string */
            string asString = subject as string;

            if (asString != null) {
                return (asString);
            }

            /* try as byte */
            byte[] asBytes = subject as byte[];

            if (asBytes != null) {
                return (asBytes.AsString());
            }

            /* can't be converted in this implemn */
            throw new ArgumentException();
        }

        public static Dictionary<TKey, TValue> FromSets<TKey, TValue>  /* passed */  (
            this Dictionary<TKey, TValue> dictionary, IEnumerable<TKey> keys, IEnumerable<TValue> values) {

            int maxMatch = Math.Min(keys.Count(), values.Count());

            for (int i = 0; i < maxMatch; i++) {
                TKey key = keys.ElementAt(i);
                TValue value = values.ElementAt(i);

                if (!dictionary.ContainsKey(key)) {
                    dictionary[key] = value;
                }
            }

            return (dictionary);
        }

        public static Dictionary<TKey, TValue> FromTuples<TKey, TValue>  /* passed */  (
            this Dictionary<TKey, TValue> self, IEnumerable<(TKey, TValue)> tuples) {
            // deconstruction for cleanest syntax 
            foreach ((TKey key, TValue value) in tuples) {
                self.Add(key, value);
            }

            return self;
        }

        public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(this (TKey, TValue)[] self)  /* passed */  {
            Dictionary<TKey, TValue> dictionary = new Dictionary<TKey, TValue>();

            foreach ((TKey key, TValue value) in self) {
                dictionary.Add(key, value);
            }

            return dictionary;
        }

        public static void AddRange<T>(this List<T> self, params T[] items)  /* verified */  {
            self.AddRange(items);  // already an array, in effect 
        }
    }
}
