﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Decorator
{
    public class EmbiggeningNumber : ANumberDecorator
    {
        public EmbiggeningNumber(ANumber number) : base(number) {
            /* no operations */
        }

        /* returns an embiggened integer, but the same each time¹; 
         * as with other Decorators, refers to aggregated instance, _not_ superclass; 
         * Decorators _only_ reimplement surface root-class methods that they affect */
        public override int CalculateValue() {
            int raw = _number.CalculateValue();
            _number.Value = raw + 7;

            return (_number.Value);
        }
    }

    /* ¹internal calls to .Value by any downstream Decorators cause unexpected overchanging of _value; 
     *  something to watch out for in real Decorator uses if methods called change values */
}
