﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Decorator
{
    public class EvenOddNumber : ANumberDecorator
    {
        /* explicit reference to a Number exists on ANumberDecorator superclass */
        bool coerceEven = true;
        bool coerceOdd = false;

        public EvenOddNumber(ANumber number) : base(number) {
            /* no operations */
        }

        /* in the Decorator pattern, _only_ methods defined on the overall root (here, ANumber)
         * are implemented, on the surface at least; their ops are forwarded to the _aggregated_ instance, 
         * _not_ superclass, or else implemented with reference to the aggregated instance */
        public override int CalculateValue() {
            /* get value and coerce according to instance state, using simple addition */
            int value = _number.CalculateValue();

            if (coerceOdd) {
                if (value % 2 == 0) {
                    value += 1;
                }
            }

            if (coerceEven) {
                if (value % 2 != 0) {
                    value += 1;
                }
            }

            /* swap even / odd state for next time */
            coerceOdd = coerceEven;
            coerceEven = !coerceOdd;

            /* return altered number */
            return (value);
        }
    }
}
