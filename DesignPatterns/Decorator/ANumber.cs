﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Decorator
{
    /* in the Decorator pattern, this is the abstraction for the base class that will be decorated, 
     * the root both for one or more concrete classes, and for the Decorator root; 
     * all subclass instances, Decorator or otherwise, will appear to be instances of this class, 
     * and all must have a surface that matches this one exactly, for full interchangeability  */

    public abstract class ANumber
    {
        /* no changes should ever be made to .Value in getter or setter */
        public int Value { get; set; }

        public abstract int CalculateValue();
    }
}
