﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Decorator
{
    public class EnumaNumber : ANumberDecorator
    {
        /* explicit reference to a Number exists on ANumberDecorator superclass */

        int _iteration = 0;

        public EnumaNumber(ANumber number) : base(number) {
            /* no operations */
        }

        /* in Decorator classes, _only_ methods defined on the overall root (here, ANumber) are implemented; 
         * each is either forwarded to the _aggregated_ instance if no change, _not_ the superclass, 
         * or else is implemented in Decorator with reference to the aggregated instance */
        public override int CalculateValue() {
            int raw = _number.CalculateValue();
            string asString = raw.ToString();

            char digit = asString[_iteration++ % asString.Length];  // incremented after calculation 
            int value = int.Parse(digit.ToString());

            return (value);
        }
    }
}
