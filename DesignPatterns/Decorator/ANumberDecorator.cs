﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Decorator
{
    /* in the Decorator pattern, this is the root decorator class that others inherit from; 
     * this is the class that retains an instance of the object being decorated */

    public abstract class ANumberDecorator : ANumber
    {
        protected ANumber _number;  // 'protected' == accessible to subclasses 

        /* unlike ANumber or its concrete Number, the Decorator root class takes an ANumber at init */
        public ANumberDecorator(ANumber number) {
            _number = number;
        }

        /* forcing subclasses to implement as though inheriting directly from ANumber */
        public abstract override int CalculateValue();

    }
}
