﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Decorator
{
    /* in the Decorator pattern, this is a concrete class that will be aggregated 
     * in the Decorator objects when not decorating another Decorator instance */

    public class Number : ANumber
    {
        public Number(int value) {
            this.Value = value;
        }

        public override int CalculateValue() {
            return (this.Value);
        }

        /* operators implemented to try it out, but not used in actual pattern 
         * because it's impossible to make 'override' static methods */
        #region Casting operator methods

        public static implicit operator Number(int value)  /* passed */  {
            return (new Number(value));
        }

        public static implicit operator int(Number number)  /* passed */  {
            return (number.Value);
        }

        #endregion Casting operator methods

    }
}
