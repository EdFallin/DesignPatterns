﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Facade
{
    public class CountOperation
    {
        public decimal Calculate(IEnumerable<decimal> numbers)  /* passed */  {
            decimal result = numbers.Count();
            return result;
        }
    }
}
