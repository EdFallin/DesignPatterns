﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Facade
{
    public class SumOperation
    {
        public decimal Calculate(IEnumerable<decimal> numbers)  /* passed */  {
            decimal result = numbers.Sum();
            return result;
        }
    }
}
