﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Facade
{
    public class DivideOperation
    {
        public decimal Calculate(decimal dividend, decimal divisor)  /* passed */  {
            return dividend / divisor;
        }
    }
}
