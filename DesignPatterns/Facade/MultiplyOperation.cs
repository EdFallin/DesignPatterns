﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Facade
{
    public class MultiplyOperation
    {
        public decimal Calculate(IEnumerable<decimal> numbers)  /* passed */  {
            // first arg is the initial value, second is a 
            // Func<> that accumulates x's to accumulator 
            decimal result = numbers.Aggregate(
                1.0m, 
                (accumulator, x) => accumulator *= x
            );

            return result;
        }
    }
}
