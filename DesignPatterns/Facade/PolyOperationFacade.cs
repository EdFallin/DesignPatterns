﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Facade
{
    /// <summary>
    /// This class is the Facade class in the Facade Pattern, encapsulating the most common uses of the operation classes, 
    /// coordinating their usages internally (but not hiding their definitions if needed individually).
    /// </summary>
    public class PolyOperationFacade
    {
        public (decimal total, decimal count, decimal average) TotalCountAndAverage(IEnumerable<decimal> numbers)  /* passed */  {
            SumOperation sum = new SumOperation();
            CountOperation volume = new CountOperation();
            DivideOperation divide = new DivideOperation();

            decimal total = sum.Calculate(numbers);
            decimal count = volume.Calculate(numbers);
            decimal average = divide.Calculate(total, count);

            return (total, count, average);
        }

        public (decimal lengthSplit, decimal totalSplit, decimal productSplit) LengthTotalAndProductSplits(
            IEnumerable<decimal> left, IEnumerable<decimal> right)  /* passed */  {

            CountOperation counter = new CountOperation();
            SumOperation totaller = new SumOperation();
            MultiplyOperation multiplier = new MultiplyOperation();

            (decimal length, decimal total, decimal product) leftSide = (
                counter.Calculate(left),
                totaller.Calculate(left),
                multiplier.Calculate(left)
            );

            (decimal length, decimal total, decimal product) rightSide = (
                counter.Calculate(right),
                totaller.Calculate(right),
                multiplier.Calculate(right)
            );

            SubtractOperation minuser = new SubtractOperation();

            (decimal, decimal, decimal) result = (
                minuser.Calculate(leftSide.length, rightSide.length),
                minuser.Calculate(leftSide.total, rightSide.total),
                minuser.Calculate(leftSide.product, rightSide.product)
            );

            return result;
        }

        /// <summary>
        /// Averages the sum of the values in the groups by the number of groups.
        /// </summary>
        public (decimal totalGroups, decimal averageOfGroups) AverageOverGroups(params IEnumerable<decimal>[] groups)  /* passed */  {
            /* some of this algorithm is silly, because I'm writing 
             * this to use my objects as extensively as possible */

            // throughput and outputs 
            decimal count = 0m;
            decimal total = 0m;
            decimal average = 0m;

            //* no-args return path, avoiding ÷ by 0 *//
            if (groups.Length == 0) {
                return (count, average);
            }

            //* main path, operating on all groups *//
            CountOperation counter = new CountOperation();
            SumOperation totaller = new SumOperation();

            List<decimal> sums = new List<decimal>();

            // reducing and gathering to sums 
            foreach (IEnumerable<decimal> group in groups) {
                decimal sum = totaller.Calculate(group);
                sums.Add(sum);
            }

            // operating over sums 
            count = counter.Calculate(sums);
            total = totaller.Calculate(sums);

            DivideOperation divider = new DivideOperation();

            // operating over throughput 
            average = divider.Calculate(total, count);

            // output 
            return (count, average);
        }
    }
}
