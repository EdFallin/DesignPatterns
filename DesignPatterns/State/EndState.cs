﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.State
{
    public class EndState : AState
    {
        /*  generally, actual state representation should be handled in and through ~system, rather than in state classes  */
        public override string GetStringForState(SystemCapsule system) {
            /*  no call to ~base's method, because the functionality is different  */

            /*  using ~system's state representation, following the pattern  */
            system._stringForState = "ending state";
            return (system._stringForState);
        }

        public override void IncrementStatesCompleted(SystemCapsule system) {
            base.IncrementStatesCompleted(system);

            /*  no changes to state here; in this hypothetical, EndState is the 
             *  final state of the system, unless a new SystemCapsule started from scratch  */
        }
    }
}
