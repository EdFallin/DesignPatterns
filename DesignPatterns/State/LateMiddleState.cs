﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.State
{
    public class LateMiddleState : AState
    {
        /*  generally, actual state representation should be handled in and through ~system, rather than in state classes  */
        public override string GetStringForState(SystemCapsule system) {
            /*  no call to ~base's method, because the functionality is different  */

            /*  using ~system's state representation, following the pattern  */
            system._stringForState = "late-middle state";
            return (system._stringForState);
        }

        public override void IncrementStatesCompleted(SystemCapsule system) {
            base.IncrementStatesCompleted(system);   /*  required for ~base functionality to happen  */

            /*  here, some state-changing operations would occur  */

            /*  after the change ops occur successfully, this step is key to this pattern (even if not implemented this way)  */
            EndState nextState = new EndState();
            system._state = nextState;

            system._stringForState = "<between states>";   //**  for my peace of mind only  **//
        }
    }
}
