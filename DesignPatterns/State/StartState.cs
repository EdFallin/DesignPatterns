﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.State
{
    public class StartState : AState
    {
        /*  the actual "state", really a representation, is stored in the context object as the pattern suggests  */
        public override string GetStringForState(SystemCapsule system) {
            /*  no call to ~base's method, because the functionality is different  */

            /*  using ~system's state representation, following the pattern  */
            system._stringForState = "starting state";
            return (system._stringForState);
        }

        /*  this method represents some state-changing operation that occurs in *this* state, in a real usage of this pattern  */
        public override void IncrementStatesCompleted(SystemCapsule system) {
            base.IncrementStatesCompleted(system);   /*  required for the default functionality to work (the incrementing)  */

            /*  here, some state-changing operations would occur  */

            /*  after the change ops occur successfully, this step is key to this pattern (even if not implemented this way)  */
            EarlyMiddleState nextState = new EarlyMiddleState();
            system._state = nextState;

            system._stringForState = "<between states>";   /*  just for my peace of mind  */
        }
    }
}
