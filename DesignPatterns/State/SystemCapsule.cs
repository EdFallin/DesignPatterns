﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.State
{
    /*  SystemCapsule is the class in this pattern that matches the "context" object described in the reftext  */

    public class SystemCapsule
    {
        /*  internal means "within this assembly", but what it amounts to 
         *  is "other related classes can see it, but not foreign classes" */
        internal AState _state = null;
        internal int _statesCompleted = 0;

        internal string _stringForState = null;   

        public SystemCapsule(AState initialState) {
            _state = initialState;
        }

        /*  in this pattern, most or all context ops are redirected to the state objects, 
         *  with the context object provided as an arg, since it contains the actual state representation  */
        public string GetStringForSystemState() {
            string output;   /*  not _stringForState, because that breaks the pattern, and it would matter in real code  */

            output = _state.GetStringForState(this);   /*  the concrete methods refer to the local variable, but that's only sillyish in a hypo case like this  */
            return (output);
        }

        /*  again, *this is passed to a state, and operations by the state are actually performed using *this' members; 
         *  this method represents state-changing operations in a state class, unlike the get-string method  */
        public void StepForward() {
            _state.IncrementStatesCompleted(this);
        }
    }
}
