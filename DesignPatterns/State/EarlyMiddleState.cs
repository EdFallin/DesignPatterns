﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.State
{
    public class EarlyMiddleState : AState
    {
        /*  generally, actual state representation should be handled in and through ~system, rather than in state classes  */
        public override string GetStringForState(SystemCapsule system) {
            /*  no call to ~base's method, because the functionality is different  */

            /*  using ~system's state representation, following the pattern  */
            system._stringForState = "early-middle state";
            return (system._stringForState);
        }

        public override void IncrementStatesCompleted(SystemCapsule system) {
            base.IncrementStatesCompleted(system);   /*  required for the actual incrementing to happen  */

            /*  here, some state-changing operations would occur  */

            /*  after the change ops occur successfully, this step is key to this pattern (even if not implemented this way)  */
            LateMiddleState nextState = new LateMiddleState();
            system._state = nextState;

            system._stringForState = "<between states>";   /*  just for my peace of mind  */
        }
    }
}
