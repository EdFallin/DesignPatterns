﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.State
{
    /*  this is the class that SystemCapsule uses to enact all its state-specific ops  */

    public abstract class AState   /*  A is for abstract  */
    {
        /*  in the pattern, state methods have default ops (basically, empty ones) 
         *  so inheriting classes only need to override the ones that match their state  */
        public virtual string GetStringForState(SystemCapsule system) { return (null); }

        public virtual void IncrementStatesCompleted(SystemCapsule system) {
            system._statesCompleted++;

            /*  incrementing the value is the default, but this represents any real-life methods that would 
             *  cause a state change, so concrete subclasses have to actually change the state for the context object  */
        }
    }
}
