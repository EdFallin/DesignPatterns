﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Command
{
    public class DumpLongCommand : ACommand
    {
        List<Tuple<int, string>> _dumpeds = new List<Tuple<int, string>>();

        public DumpLongCommand() {
            this.Name = "Dump long words";
        }

        public override void Enact(Subject subject) {
            /* get and project long words */
            var whatever = subject
                .Where(x => (x.Length > 6))
                .Select((string x, int i) => new { Offset = i, Word = x });

            /* dump long words but retain on side */
            foreach (var element in whatever) {
                subject.RemoveAt(element.Offset);

                Tuple<int, string> tuple = Tuple.Create(element.Offset, element.Word);
                _dumpeds.Add(tuple);
            }
        }

        public override void Reverse(Subject subject) {
            /* reinsert each dumped item at its original offset, with very last handled differently */

            for (int i = 0; i < _dumpeds.Count; i++) {
                Tuple<int, string> tuple = _dumpeds[i];

                /* last dumped may be after the last retained item */
                if (i + 1 == _dumpeds.Count) {
                    if (tuple.Item1 >= subject.Count) {
                        subject.Add(tuple.Item2);
                        break;  // early exit solely if added at end 
                    }
                }

                /* common case */
                subject.Insert(tuple.Item1, tuple.Item2);
            }

        }

        public override ACommand ToCopy() {
            DumpLongCommand copy = new DumpLongCommand();
            copy._dumpeds = this._dumpeds;  // repointing 

            return (copy);
        }
    }
}
