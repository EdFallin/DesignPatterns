﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Command
{
    public class Menu
    {
        const string UNDO = "Undo";
        const string REDO = "Redo";

        #region Properties and dependencies

        public Dictionary<string, ACommand> Commands { get; protected set; } = null;


        public string[] MenuItems { get; protected set; }

        /* supporting Undo and Redo; shared across both those classes */
        public List<ACommand> Dones { get; set; } = new List<ACommand>();
        public List<ACommand> Undones { get; set; } = new List<ACommand>();

        #endregion Properties and dependencies


        #region Methods

        /* an initialization method */
        public void AssignCommands(List<ACommand> commands)  /* passed */  {
            /* convert input to O(1) retrievables, plus displayable names; 
             * link any UndoCommand and RedoCommand to shared lists */

            // collections 
            this.Commands = commands
                .ToDictionary(x => x.Name);

            this.MenuItems = this.Commands.Keys
                .ToArray();

            // linking to shared lists 
            if (this.Commands.ContainsKey(UNDO)) {
                UndoCommand undo = (UndoCommand)this.Commands[UNDO];
                undo.Dones = this.Dones;
                undo.Undones = this.Undones;
            }

            if (this.Commands.ContainsKey(REDO)) {
                RedoCommand redo = (RedoCommand)this.Commands[REDO];
                redo.Dones = this.Dones;
                redo.Undones = this.Undones;
            }
        }

        /* crux method: represents selecting a menu item */
        public void InvokeItem(string name, StringBuilder subject, object content = null)  /* verified */  {
            ACommand command = this.Commands[name];  // assumed always present 

            if (name == "Add text") {
                AddTextCommand addText = command as AddTextCommand;
                addText.Content = content;
            }

            command.Enact(subject);

            /* don't save undoes and redoes, in this implementation */
            if ((name != UNDO) && (name != REDO)) {
                this.Dones.Add(command);
            }
        }

        #endregion Methods
    }
}
