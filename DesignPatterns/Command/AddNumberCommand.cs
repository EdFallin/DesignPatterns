﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Command
{
    public class AddNumberCommand : ACommand
    {
        const string SPACE = " ";

        public override void Enact(object content, StringBuilder subject) {
            if (!(content is int || content is double)) {
                throw new ArgumentException("Content must be a number.", nameof(content));
            }

            this.Content = content;  // for redoing 

            subject.Append(SPACE);
            subject.Append(content);
        }

        public override void Reverse(StringBuilder subject) {
            string topic = subject.ToString();
            string content = this.Content.ToString();

            if (!topic.EndsWith(content)) {
                return;  // not a throw 
            }

            topic = topic.Remove(topic.Top() - content.Length);

            subject.Clear();
            subject.Append(topic.Trim());
        }
    }
}
