﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Command
{
    public class AddOneToTenCommand : ACommand
    {
        string _memento = null;

        string[] _words = {
            "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"
        };

        public AddOneToTenCommand() {
            this.Name = "Add word from \"one\" to \"ten\"";
        }

        public override string Enact(string subject) {
            _memento = subject;  // value copy 

            for (int i = 0; i < _words.Length; i++) {
                string word = _words[i];

                if (!subject.Contains(word)) {
                    subject += " " + word;
                    return (subject);
                }
            }

            /* fall-back */
            return (subject);
        }

        public override string Reverse(string subject) {
            return (_memento);
        }

        public override ACommand ToCopy() {
            AddOneToTenCommand copy = new AddOneToTenCommand();
            copy._memento = this._memento;  // repointing 

            return (copy);
        }
    }
}
