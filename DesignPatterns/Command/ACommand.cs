﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Command
{
    /* an abstract class representing things that can be done on the string, 
     * with metadata to allow use in a menu and undo / redo system */

    public abstract class ACommand
    {
        public string Name { get; protected set; }

        public abstract void Enact(StringBuilder subject);

        public abstract void Reverse(StringBuilder subject);
    }
}
