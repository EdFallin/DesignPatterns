﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Command
{
    public class AddDateCommand : ACommand
    {
        const string SPACE = " ";

        public override void Enact(object content, StringBuilder subject) {
            if (!(content is DateTime)) {
                throw new ArgumentException("Content must be a date-time.", nameof(content));
            }

            this.Content = content;  // for redoing 

            subject.Append(SPACE);
            subject.Append(((DateTime)content).ToString("d"));
        }

        public override void Reverse(StringBuilder subject) {
            string topic = subject.ToString();
            string content = ((DateTime)this.Content).ToString("d");

            if (!topic.EndsWith(content)) {
                return;  // not a throw 
            }

            topic = topic.Remove(topic.Top() - content.Length);

            subject.Clear();
            subject.Append(topic.Trim());
        }
    }
}
