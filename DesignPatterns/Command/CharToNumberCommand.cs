﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Command
{
    public class CharToNumberCommand : ACommand
    {
        const string SPACE = " ";

        public CharToNumberCommand() {
            this.Name = "Chars to numbers";
        }

        public override void Enact(StringBuilder subject)  /* passed */  {
            /* changes the first 9 letters of the lower-case alphabet with 1 to 9 */

            char[] chars = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i' };
            char[] numbers = { '1', '2', '3', '4', '5', '6', '7', '8', '9' };

            for (int i = 0; i < subject.Length; i++) {
                char atI = subject[i];

                // skip chars not in 'a'-'i' 
                if (!chars.Contains(atI)) {
                    continue;
                }

                // replace chars in the range with numbers 
                int charsOffset = atI - 97;  // can use mathed char values as indices; 'a' == 97 
                subject[i] = numbers[charsOffset];
            }
        }

        public override void Reverse(StringBuilder subject)  /* passed */  {
            /* traverse over ~subject, convert digits to 'a'-'i' */

            for (int i = 0; i < subject.Length; i++) {
                char atI = subject[i];

                if (char.IsDigit(atI)) {
                    string asString = new string(atI, 1);  // Char.ToString() does same thing! 
                    int asInt = int.Parse(asString);

                    /* 'a' == 97, so int + 96 --> char */
                    char asChar = (char)(96 + asInt);
                    subject[i] = asChar;
                }
            }
        }
    }
}
