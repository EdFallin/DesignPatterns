﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Command
{
    /* this and RedoCommand implement undo and redo of ACommand items, but not undo-as-redo; 
     * these classes subclass ACommand so they can be integrated cleanly into the Menu code */

    public class UndoCommand : ACommand
    {
        /* both getter and setter, so can be shared with Redo */
        public List<ACommand> Dones { get; set; }
        public List<ACommand> Undones { get; set; }

        public UndoCommand() {
            this.Name = "Undo";
        }

        public override void Enact(StringBuilder subject)  /* verified */  {
            /* get last done item and .Reverse() on it, move it to .Undones, its content to .Contents */

            // getting ACommand to undo, reversing it 
            ACommand last = this.Dones.Last();
            last.Reverse(subject);

            // moving undone command across lists 
            this.Dones.Remove(last);  // removing by identity 
            this.Undones.Add(last);
        }

        public override void Reverse(StringBuilder subject)  /* verified */  {
            throw new NotSupportedException(
                ".Reverse() is not supported on UndoCommand; use RedoCommand instead."
                );
        }
    }
}
