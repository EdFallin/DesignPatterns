﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;

namespace DesignPatterns.Command
{
    public class DumpNumericCommand : ACommand
    {
        List<Tuple<int, string>> _dumpeds = new List<Tuple<int, string>>();

        public DumpNumericCommand() {
            this.Name = "Dump numbers";
        }

        public override void Enact(Subject subject) {
            string numeric = @"\A\d+\Z";  // entire line (text, here) is digits 

            /* get and project "words" that are actually numbers */
            var whatever = subject
                .Where(x => (Regex.IsMatch(x, numeric)))
                .Select((string x, int i) => new { Offset = i, Word = x });

            /* dump numbers but retain on side */
            foreach (var element in whatever) {
                subject.RemoveAt(element.Offset);

                Tuple<int, string> tuple = Tuple.Create(element.Offset, element.Word);
                _dumpeds.Add(tuple);
            }
        }

        public override void Reverse(Subject subject) {
            /* reinsert each dumped item at its original offset, with very last handled differently */

            for (int i = 0; i < _dumpeds.Count; i++) {
                Tuple<int, string> tuple = _dumpeds[i];

                /* last dumped may be after the last retained item */
                if (i + 1 == _dumpeds.Count) {
                    if (tuple.Item1 >= subject.Count) {
                        subject.Add(tuple.Item2);
                        break;  // early exit solely if added at end 
                    }
                }

                /* common case */
                subject.Insert(tuple.Item1, tuple.Item2);
            }

        }

        public override ACommand ToCopy() {
            DumpNumericCommand copy = new DumpNumericCommand();
            copy._dumpeds = this._dumpeds;  // repointing 

            return (copy);
        }
    }
}
