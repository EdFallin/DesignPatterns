﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Command
{
    /* a class operated on by the command objects */

    public class Subject : IList<string>, IEnumerable<string>
    {
        #region Fields

        private List<string> _strings = new List<string>();

        #endregion Fields


        #region Constructors

        public Subject() {
            _strings = WordSalad.Words.ToList();
        }

        public Subject(Subject subject) {
            /* clumsy value-copy mechanism */
            _strings = subject._strings
                .ToArray()
                .ToList();
        }

        public Subject(List<string> strings) {
            /* repointing; no value-copy */
            _strings = strings;
        }

        #endregion Constructors


        #region Indexers

        public string this[int offset] {
            get {
                if (offset < 0 || offset >= _strings.Count) {
                    return (string.Empty);
                }

                return (_strings[offset]);
            }
            set {
                if (offset < 0 || offset >= _strings.Count) {
                    return;
                }

                _strings[offset] = value;
            }
        }

        public int Count {
            get {
                throw new NotImplementedException();
            }
        }

        public bool IsReadOnly {
            get {
                throw new NotImplementedException();
            }
        }

        public void Add(string item) {
            throw new NotImplementedException();
        }

        public void Clear() {
            throw new NotImplementedException();
        }

        public bool Contains(string item) {
            throw new NotImplementedException();
        }

        public void CopyTo(string[] array, int arrayIndex) {
            throw new NotImplementedException();
        }

        #endregion Indexers


        #region IList

        /* a simple Adapter pattern implemn that reminds me of Proxy pattern */

        public int IndexOf(string item) {
            return (_strings.IndexOf(item));
        }

        public void Insert(int index, string item) {
            _strings.Insert(index, item);
        }

        public bool Remove(string item) {
            return (_strings.Remove(item));
        }

        public void RemoveAt(int index) {
            _strings.RemoveAt(index);
        }

        #endregion IList


        #region IEnumerable

        public IEnumerator<string> GetEnumerator() {
            for (int i = 0; i < _strings.Count; i++) {
                yield return _strings[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator() {
            for (int i = 0; i < _strings.Count; i++) {
                yield return _strings[i];
            }
        }

        #endregion IEnumerable

    }

    public static class WordSalad
    {
        #region Fields

        static string[] _pineWords = {
            "Pine", "tree", "Japanese", "red", "pine", "Pinus", "densiflora", "North", "Korea", "Scientific", "classification",
            "Pinus", "L", "Subgenera", "Subgenus", "Strobus", "Subgenus", "Pinus", "See", "Pinus", "classification", "for", "complete",
            "geographic", "distribution", "Range", "of", "Pinus", "A", "pine", "is", "any", "conifer", "in", "the", "genus", "Pinus",
            "Plant", "List", "compiled", "by", "the", "Royal", "Botanic", "Gardens", "Kew", "and", "Missouri", "Botanical", "Garden",
            "and", "many", "more", "synonyms", "Contents", "hide", "1", "Etymology", "2", "Taxonomy", "nomenclature", "and",
            "61", "Farming", "62", "Food", "63", "Folk", "Medicine", "7", "See", "also", "8", "Notes", "9", "References", "10",
            "some", "have", "traced", "to", "the", "Indo", "European", "base", "source", "of", "English", "pituitary", "Before",
            "by", "way", "of", "Middle", "English", "firre", "In", "some", "European", "languages", "Germanic", "cognates", "of",
            "fura", "fure", "furu", "Swedish", "fura", "furu", "Dutch", "vuren", "and", "German", "Föhre", "but", "in", "modern",
            "English", "fir", "is", "now", "restricted", "to", "fir", "Abies", "and", "Douglas", "fir", "Pseudotsuga", "Taxonomy",
            "The", "genus", "is", "divided", "into", "three", "subgenera", "which", "can", "be", "distinguished", "by", "cone", "seed",
            "and", "leaf", "characters", "Pinus", "subg", "Pinus", "the", "yellow", "or", "hard", "pine", "group", "generally",
            "with", "harder", "wood", "and", "two", "or", "three", "needles", "per", "fascicle", "4", "Pinus", "subg", "Ducampopinus",
            "the", "foxtail", "or", "pinyon", "group", "Pinus", "subg", "Strobus", "the", "white", "or", "soft", "pine", "group",
            "grown", "as", "timber", "or", "cultivated", "as", "ornamental", "plants", "in", "parks", "and", "gardens", "A",
            "number", "of", "such", "introduced", "species", "have", "become", "invasive", "5", "and", "threaten", "native",
            "evergreen", "coniferous", "resinous", "trees", "or", "rarely", "shrubs", "growing", "3", "80", "m", "10", "260",
            "ft", "tall", "with", "the", "majority", "of", "species", "reaching", "15", "45", "m", "50", "150", "ft", "tall",
            "The", "smallest", "are", "Siberian", "dwarf", "pine", "and", "Potosi", "pinyon", "and", "the", "tallest", "is",
            "a", "8179", "m", "26835", "ft", "tall", "ponderosa", "pine", "located", "in", "southern", "Oregon's", "Rogue",
            "thick", "and", "scaly", "but", "some", "species", "have", "thin", "flaky", "bark", "The", "branches", "are",
            "produced", "in", "regular", "pseudo", "whorls", "actually", "a", "very", "tight", "spiral", "but", "appearing",
            "producing", "just", "one", "such", "whorl", "of", "branches", "each", "year", "from", "buds", "at", "the", "tip",
            "of", "the", "year's", "new", "shoot", "but", "others", "are", "multinodal", "producing", "two", "or", "more",
            "scales", "are", "arranged", "in", "Fibonacci", "number", "ratios", "citation", "needed", "The", "new", "spring",
            "can", "be", "found", "in", "the", "White", "Mountains", "of", "California", "7", "An", "older", "tree", "now",
            "cut", "down", "was", "dated", "at", "4900", "years", "old", "It", "was", "discovered", "in", "a", "grove", "beneath",
            "Wheeler", "Peak", "and", "it", "is", "now", "known", "as", "Prometheus", "after", "the", "Greek", "immortal",
            "pine", "Pinus", "sylvestris", "Pines", "have", "four", "types", "of", "leaf", "Seed", "leaves", "cotyledons",
            "on", "seedlings", "born", "in", "a", "whorl", "of", "4", "24", "Juvenile", "leaves", "which", "follow", "immediately",
            "to", "five", "years", "rarely", "longer", "Scale", "leaves", "similar", "to", "bud", "scales", "small", "brown",
            "6", "commonly", "2", "5", "needles", "together", "each", "fascicle", "produced", "from", "a", "small", "bud", "on",
            "a", "dwarf", "shoot", "in", "the", "axil", "of", "a", "scale", "leaf", "These", "bud", "scales", "often", "remain",
            "spring", "though", "autumn", "in", "a", "few", "pines", "falling", "as", "soon", "as", "they", "have", "shed",
            "their", "pollen", "The", "female", "cones", "take", "15", "3", "years", "depending", "on", "species", "to", "mature",
            "cones", "are", "3", "60", "cm", "long", "Each", "cone", "has", "numerous", "spirally", "arranged", "scales", "with",
            "two", "seeds", "on", "each", "fertile", "scale", "the", "scales", "at", "the", "base", "and", "tip", "of", "the",
            "open", "In", "others", "the", "seeds", "are", "stored", "in", "closed", "serotinous", "cones", "for", "many", "years",
            "until", "an", "environmental", "cue", "triggers", "the", "cones", "to", "open", "releasing", "the", "seeds",
            "The", "most", "common", "form", "of", "serotiny", "is", "pyriscence", "in", "which", "a", "resin", "binds", "the",
            "a", "European", "black", "pine", "Pinus", "nigra", "woodland", "Portugal", "Pines", "grow", "well", "in", "acid",
            "soils", "some", "also", "on", "calcareous", "soils", "most", "require", "good", "soil", "drainage", "preferring",
            "sandy", "soils", "but", "a", "few", "lodgepole", "pine", "will", "tolerate", "poorly", "drained", "wet", "soils",
            "A", "few", "are", "able", "to", "sprout", "after", "forest", "fires", "Canary", "Island", "pine", "Some", "species",
            "of", "pines", "bishop", "pine", "need", "fire", "to", "regenerate", "and", "their", "populations", "slowly", "decline",
            "imposed", "by", "elevation", "and", "latitude", "Siberian", "dwarf", "pine", "mountain", "pine", "whitebark", "pine",
            "Turkish", "pine", "and", "gray", "pine", "are", "particularly", "well", "adapted", "to", "growth", "in", "hot",
            "Some", "birds", "notably", "the", "spotted", "nutcracker", "Clark's", "nutcracker", "and", "Pinyon", "jay", "are",
            "sometimes", "eaten", "by", "some", "Lepidoptera", "butterfly", "and", "moth", "species", "see", "list", "of",
            "growth", "and", "maturation", "and", "may", "enable", "fungi", "to", "decompose", "nutritionally", "scarce",
            "pollen", "is", "also", "involved", "in", "moving", "plant", "matter", "between", "terrestrial", "and", "aquatic",
        };

        #endregion Fields


        #region Properties

        public static string[] Words {
            get {
                return (_pineWords);
            }
        }

        #endregion Properties

    }
}
