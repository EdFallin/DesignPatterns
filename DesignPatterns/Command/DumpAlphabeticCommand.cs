﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;

namespace DesignPatterns.Command
{
    public class DumpAlphabeticCommand : ACommand
    {
        string _memento = null;

        public DumpAlphabeticCommand() {
            this.Name = "Dump word lexemes";
        }

        public override string Enact(string subject) {
            /* after memento, dump letter-only lexemes; leave extra spaces for reversing by index */

            _memento = subject;  // value copy 

            string wordRegex = @"\b[A-Za-z]+\b";  // entire word is letters 
            subject = Regex.Replace(subject, wordRegex, string.Empty);  // no 'null's here 

            return (subject);
        }

        public override string Reverse(string subject) {
            return (_memento);
        }

        public override ACommand ToCopy() {
            DumpAlphabeticCommand copy = new DumpAlphabeticCommand();
            copy._memento = this._memento;

            return (copy);
        }
    }
}
