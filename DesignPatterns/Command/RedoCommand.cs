﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Command
{
    /* this and UndoCommand implement undo and redo of ACommand items, but not undo-as-redo; 
     * these classes subclass ACommand so they can be integrated cleanly into the Menu code */

    public class RedoCommand : ACommand
    {
        public List<ACommand> Dones { get; set; }
        public List<ACommand> Undones { get; set; }

        public RedoCommand() {
            this.Name = "Redo";
        }

        public override void Enact(StringBuilder subject)  /* verified */  {
            /* get last-undone item, call and .Enact() it, move it (back) to end of .Dones */

            // getting ACommand to redo, enacting it 
            ACommand last = this.Undones.Last();
            last.Enact(subject);

            // moving across lists 
            this.Undones.Remove(last);  // removing by identity 
            this.Dones.Add(last);
        }

        public override void Reverse(StringBuilder subject)  /* verified */  {
            throw new NotSupportedException(
                "Reverse() is not supported on RedoCommand; use UndoCommand instead."
                );
        }
    }
}
