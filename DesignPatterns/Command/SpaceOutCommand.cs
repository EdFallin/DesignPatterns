﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DesignPatterns.Command
{
    public class SpaceOutCommand : ACommand
    {
        public SpaceOutCommand() {
            this.Name = "Space out text";
        }

        public override void Enact(StringBuilder subject)  /* passed */  {
            /* add a space between each char (including existing spaces); trying this the non-regex way */

            int top = subject.Length * 2;

            for (int i = 1; i < top; i += 2) {
                subject.Insert(i, ' ');
            }
        }

        public override void Reverse(StringBuilder subject)  /* passed */  {
            /* remove extra spaces added, though only if not run twice, reversed before, etc. */

            string olds = @"(.) ";  // any single char (including a space) followed by a space 
            string news = @"$1";    // the entire paren'd value (the char before the space literal) 

            // remove extra spaces 
            string asString = subject.ToString();
            asString = Regex.Replace(asString, olds, news);

            // reinit same object with different contents 
            subject.Clear();
            subject.Append(asString);
        }
    }
}
