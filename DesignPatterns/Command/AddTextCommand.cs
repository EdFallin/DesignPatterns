﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Command
{
    public class AddTextCommand : ACommand
    {
        const string SPACE = " ";

        public object Content { get; set; } = null;

        public AddTextCommand() {
            this.Name = "Add text";
        }

        public override void Enact(StringBuilder subject)  /* passed */  {
            subject.Append(SPACE);
            subject.Append(this.Content);
        }

        public override void Reverse(StringBuilder subject)  /* passed */  {
            string asText = subject.ToString();
            string contentText = this.Content.ToString();

            if (!asText.EndsWith(contentText)) {
                return;  // no throw; instead, no change 
            }

            // drop the added text 
            asText = asText.Remove(asText.Top() - contentText.Length);
            asText = asText.Trim();

            // reinit same object with different contents 
            subject.Clear();
            subject.Append(asText);
        }
    }
}
