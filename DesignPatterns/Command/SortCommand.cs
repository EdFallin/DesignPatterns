﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Command
{
    /* sorting, with arbitrarily bad reversal mechanism; quality code not needed for pattern test */

    public class SortCommand : ACommand
    {
        Subject _preEnactSubject = null;

        public SortCommand() {
            this.Name = "Sort words";
        }

        public override void Enact(Subject subject) {
            _preEnactSubject = new Subject(subject);  // value-copy; for .Reverse() 

            List<string> passer = subject
                .OrderBy(x => x)
                .ToList();  // half-assed value-copy 

            subject = new Subject(passer);  // repointing only 
        }

        public override void Reverse(Subject subject) {
            subject = _preEnactSubject;  // repointing; only works if undo/redo as stack 
        }

        public override ACommand ToCopy() {
            SortCommand copy = new SortCommand();
            copy._preEnactSubject = this._preEnactSubject;  // repointing only 

            return (copy);
        }
    }
}
