﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Bridge
{
    /*  this class represents some external code with needed functionality that has its own API  */

    public class ExternalImplementationTextual
    {
        public char[] FirstCharToChars(char topic) {
            int number = (int)topic;  // explicit conversion for readability 

            char[] chars = new char[number];

            for (int i = 0; i < number; i++) {
                chars[i] = topic;
            }

            return (chars);
        }
    }
}
