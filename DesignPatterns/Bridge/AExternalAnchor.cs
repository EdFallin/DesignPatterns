﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Bridge
{
    /*  this is the base class for code that wrangles the functionality needed across the bridge, 
     *  and which can be implemented in 2 or more ways by external code in actual use  */

    public abstract class AExternalAnchor
    {
        public abstract string ExternalStringMethod(string topic);
    }
}
