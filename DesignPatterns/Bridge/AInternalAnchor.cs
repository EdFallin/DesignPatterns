﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Bridge
{
    /*  this is the base class for internal code that relies on the bridged-to functionality; 
     *  internal classes for this concern inherit from this one  */

    public abstract class AInternalAnchor
    {
        AExternalAnchor _farEnd = null;  // this is the far end of the bridge 

        public AExternalAnchor FarEnd {
            get {
                return _farEnd;
            }

            set {
                _farEnd = value;
            }
        }

        protected string InternalStringMethod(string topic) {
            /*  this is "crossing the bridge": cleanly using whichever external implementation here  */
            string result = FarEnd.ExternalStringMethod(topic);
            return (result);
        }
    }
}
