﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Bridge
{
    /*  as a subclass of the far-end anchor, this class forwards requests to the external code  */

    public class ExternalAnchorNumeric : AExternalAnchor
    {
        public override string ExternalStringMethod(string topic) {
            /*  the last char should be a digit, then ->> its square root as string  */

            string lastChar = new string(topic[topic.Length - 1], 1);
            double asDouble = double.Parse(lastChar);
            double squareRoot = ExternalImplementationNumeric.SquareRootOfFloat(asDouble);

            return (squareRoot.ToString());
        }
    }
}
