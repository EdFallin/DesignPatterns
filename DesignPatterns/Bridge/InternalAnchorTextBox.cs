﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;

namespace DesignPatterns.Bridge
{
    /*  this is a concrete subclass of the near-end class, with a UI-style surface  */

    public class InternalAnchorTextBox : AInternalAnchor
    {
        public TextBox TextBoxFromString(string topic) {
            /*  utilizing the functionality that has "crossed the bridge"  */
            string text = base.InternalStringMethod(topic);

            TextBox box = new TextBox();
            box.Text = text;

            return (box);
        }
    }
}
