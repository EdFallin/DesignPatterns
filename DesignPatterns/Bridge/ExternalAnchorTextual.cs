﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Bridge
{
    /*  as a subclass of the far-end anchor, this class forwards requests to the external code  */

    public class ExternalAnchorTextual : AExternalAnchor
    {
        ExternalImplementationTextual external = new ExternalImplementationTextual();

        public override string ExternalStringMethod(string topic) {
            /*  first char ->> a string of that char, as long as its ASCII number  */

            char[] asChars = external.FirstCharToChars(topic[0]);
            string result = new string(asChars);

            return (result);
        }
    }
}
