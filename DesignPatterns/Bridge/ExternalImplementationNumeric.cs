﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Bridge
{
    /*  this class represents some external code with needed functionality that has its own API; 
     *  this one of the two is static just to emphasize the encapsulated differentiation  */

    public static class ExternalImplementationNumeric
    {
        public static double SquareRootOfFloat(double topic) {
            return (Math.Sqrt(topic));
        }
    }
}
