﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;

namespace DesignPatterns.Bridge
{
    /*  this is a concrete subclass of the near-end class, with a UI-style surface  */

    public class InternalAnchorLabel : AInternalAnchor
    {
        public Label LabelFromString(string topic) {
            /*  using the functionality that has "crossed the bridge"  */
            string content = base.InternalStringMethod(topic);

            Label label = new Label();
            label.Content = content;

            return (label);
        }
    }
}
