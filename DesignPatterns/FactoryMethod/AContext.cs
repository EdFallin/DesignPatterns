﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.FactoryMethod
{
    /* in the Factory Method pattern, this is the base class for that will have associated 
     * concrete product classes, all to be treated at the same high level of abstraction */

    /* my model here is a content class AContext, representing maybe text, formatted text, or an image, 
     * which produces a display widget AProduct depending on its type; the latter supports editing; 
     * this is basically my QuickBoard idea, perhaps simpler than what I have there now */

    public abstract class AContext
    {
        /* this is the Factory Method itself */
        public abstract AProduct MakeProduct();

        public abstract void Change();
    }
}
