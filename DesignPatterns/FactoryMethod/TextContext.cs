﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.FactoryMethod
{
    /* in the Factory Method pattern, this and other __Context classes 
     * are those that produce matching custom products */

    public class TextContext : AContext
    {
        public string Text { get; set; }

        public TextContext(string text) {
            this.Text = text;
        }

        public override void Change()  /* verified */  {
            this.Text += " and more";
        }

        /* the concrete Factory Method: returns the matching concrete product */
        public override AProduct MakeProduct()  /* verified */  {
            TextProduct product = new TextProduct();
            product.Context = this;

            return (product);
        }
    }
}
