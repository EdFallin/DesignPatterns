﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.FactoryMethod
{
    /* this and the other __Product classes are the concretes that are 
     * produced by their matching __Context concretes */

    public class FormattedTextProduct : AProduct
    {
        public FormattedTextContext Context { get; set; }

        public override object Display()  /* verified */  {
            string leader = "[[start-formatted-text]]";
            string trailer = "[[end-formatted-text]]";

            string display = leader + this.Context.FormattedText + trailer;
            return (display);
        }
    }
}
