﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.FactoryMethod
{
    /* in the Factory Method pattern, concrete AProduct subclasses are produced by concrete AContexts; 
     * both of any kind are used via their abstract superclass by most or all code */

    /* in my model, this is a display widget that's specialized for the content type; 
     * I'm not going to write any editing structure, which would mean a second Factory Method 
     *     either on the AContext or on the AProduct, chained to another abstract object; 
     * this is basically my QuickBoard idea, perhaps simpler than what I have there now */

    public abstract class AProduct
    {
        /* to vaguely imitate displaying, Display() returns an object containing the "displayed" contents */

        public abstract object Display();
    }
}
