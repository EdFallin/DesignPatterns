﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.FactoryMethod
{
    /* this and the other __Product classes are the concretes that are 
     * produced by their matching __Context concretes */

    public class ImageProduct : AProduct
    {
        public ImageContext Context { get; set; }

        public override object Display()  /* verified */  {
            byte[][] bracerBytes = GetBracers();
            byte[] display = BuildBracedOutput(bracerBytes);

            /* convey */
            return (display);
        }

        #region Dependencies of Display()

        private static byte[][] GetBracers()  /* verified */  {
            string[] bracers = { "[[start-image]]", "[[end-image]]" };
            byte[][] bracerBytes = new byte[2][];

            for (int i = 0; i < bracers.Length; i++) {
                string bracer = bracers[i];

                byte[] asBytes = bracer.ToBytes();  // extension 

                /* retain */
                bracerBytes[i] = asBytes;
            }

            return (bracerBytes);
        }

        private byte[] BuildBracedOutput(byte[][] bracerBytes)  /* verified */  {
            /* get passers for simplicity */
            int leaderLength = bracerBytes[0].Length;
            int imageLength = this.Context.Image.Length;
            int trailerLength = bracerBytes[1].Length;

            int combinedLength = leaderLength + imageLength + trailerLength;
            int beforeTrailerLength = leaderLength + imageLength;

            /* build output block by block */
            byte[] output = new byte[combinedLength];

            Buffer.BlockCopy(bracerBytes[0], 0, output, 0, leaderLength);
            Buffer.BlockCopy(this.Context.Image, 0, output, leaderLength, imageLength);
            Buffer.BlockCopy(bracerBytes[1], 0, output, beforeTrailerLength, trailerLength);

            return (output);
        }

        #endregion Dependencies of Display()
    }
}
