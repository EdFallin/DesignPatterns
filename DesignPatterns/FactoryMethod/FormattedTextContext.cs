﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.FactoryMethod
{
    /* in the Factory Method pattern, this and other __Context classes 
     * are those that produce matching custom products */

    public class FormattedTextContext : AContext
    {
        /* the idea here is that the text contains formatting codes inlined into its contents */

        public string FormattedText { get; set; }

        public FormattedTextContext(string formattedText) {
            this.FormattedText = formattedText;
        }

        public override void Change()  /* verified */  {
            this.FormattedText += " [start-bold]plus[end-bold]";
        }

        /* the concrete Factory Method: returns the matching concrete product */
        public override AProduct MakeProduct()  /* verified */  {
            FormattedTextProduct product = new FormattedTextProduct();
            product.Context = this;

            return (product);
        }
    }
}
