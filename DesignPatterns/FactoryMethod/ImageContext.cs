﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.FactoryMethod
{
    /* in the Factory Method pattern, this and other __Context classes 
     * are those that produce matching custom products */

    public class ImageContext : AContext
    {
        public byte[] Image { get; set; }  // in theory, an image in its rawest form; actually text or a number 

        public ImageContext(byte[] image) {
            this.Image = image;
        }

        public override void Change()  /* verified */  {
            /* zeroing out every 3rd byte; effect is to turn every 3rd char into a null (\0) */

            for (int i = 0; i < this.Image.Length; i++) {
                if (i % 3 == 0) {
                    this.Image[i] = 0;
                }
            }
        }

        /* the concrete Factory Method: returns the matching concrete product */
        public override AProduct MakeProduct()  /* verified */  {
            ImageProduct product = new ImageProduct();
            product.Context = this;

            return (product);
        }
    }
}
