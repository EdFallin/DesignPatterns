﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Flyweight
{
    /* in the Flyweight pattern, typically some context is needed; 
     * to keep it simple in this learnings code, I'm just putting objects 
     * into the structure that represent state, such as an Id or 
     * Css class for a region encompassing one or more tag pairs */

    public class HtmlStartContext : AElement
    {
        public string Context { get; set; }

        public HtmlStartContext(string context) {
            this.Context = context;
        }
    }

    /* although not an ATag, also a Flyweight, as end-context objects have no state */
    public class HtmlEndContext : AElement
    {
        /* no operations defined; just terminates whatever context currently defines state */

        static HtmlEndContext _instance = null;

        protected HtmlEndContext() {
            /* no ops or content */
        }

        public static HtmlEndContext Instance() {
            if (_instance == null) {
                _instance = new HtmlEndContext();
            }

            return (_instance);
        }
    }
}
