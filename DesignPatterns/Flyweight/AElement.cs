﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Flyweight
{
    /* the root object for all elements used in the Flyweight pattern and its encompassing structure, 
     * which isn't defined in the pattern but which normally supports both content and context; 
     * basically I'm implementing the Composite pattern here, in a very light way; 
     * this appears as typical with Flyweight as does Singleton */

    public abstract class AElement
    {
        /* nothing defined, as this class simply allows different class hierarchies to be used as one; 
         * could instead be an interface, though abstract class allows adding functionality easily */
    }
}
