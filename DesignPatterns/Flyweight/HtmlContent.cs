﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Flyweight
{
    /* this class defines Html content that is not tags, which are ATag subclass flyweights; 
     * kept out of the lineage of ATag just so this ToHtml() doesn't have unused string arg */

    public class HtmlContent : AElement
    {
        public string Content { get; set; }

        public HtmlContent(string content) {
            this.Content = content;
        }

        public string ToHtml()  /* verified */  {
            return (this.Content);
        }
    }
}
