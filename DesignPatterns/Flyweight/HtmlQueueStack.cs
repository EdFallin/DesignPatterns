﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Flyweight
{
    /* in the Flyweight pattern, this class is the structure using flyweight instances and providing them context for use; 
     * in this case, I'm pairing a queue and stack, with the latter used as a primitive way of localizing state; 
     * although this is kind of complex, the reftext's Flyweight example also employs a complex structure ( B-tree! ) */

    public class HtmlQueueStack : IEnumerable<string>
    {
        #region Fields

        Queue<AElement> _queue = new Queue<AElement>();
        Stack<HtmlStartContext> _stack = new Stack<HtmlStartContext>();

        #endregion Fields


        #region Methods

        public void Add(AElement element) {
            _queue.Enqueue(element);
        }

        /* I'm not defining removing here, since this is learnings only */

        #endregion Methods


        #region IEnumerable

        /* factored operations for cleaner enumerator code */
        public string Next()  /* verified */  {
            AElement element = _queue.Dequeue();

            /* state elements: push and pop on stack */
            if (element is HtmlStartContext) {
                _stack.Push(element as HtmlStartContext);
                return (null);
            }

            if (element is HtmlEndContext) {
                _stack.Pop();
                return (null);
            }

            /* elements, either ATag Flyweights or HtmlContents; 
             * not quite a Composite, since they're different; 
             * if not HtmlContext, AElement is (only) one of these */
            ATag tag = element as ATag;

            if (tag != null) {
                return (tag.ToHtml(_stack.Peek().Context));  // apply topmost state on stack 
            }


            HtmlContent content = element as HtmlContent;

            if (content != null) {
                return (content.ToHtml());
            }

            /* fall-through; should never be reached */
            return (null);
        }

        public IEnumerator<string> GetEnumerator()  /* passed */  {
            while (_queue.Count > 0) {
                yield return (this.Next());
            }
        }

        IEnumerator IEnumerable.GetEnumerator() {
            while (_queue.Count > 0) {
                yield return (this.Next());
            }
        }

        #endregion IEnumerable
    }
}
