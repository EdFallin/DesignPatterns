﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Flyweight
{
    /* in the Flyweight pattern, these concrete tags are the Flyweight classes themselves; 
     * in keeping with the pattern, these depend on their context for Css class, Html Id, etc. */

    public class DivOpenTag : ATag
    {
        static DivOpenTag _instance = null;

        /// <summary>
        /// Attribute passed when this class is used must be one or more pairings of the form name = \"value\".
        /// </summary>
        protected DivOpenTag() {
            _stencil = "<div @attribute@>";
        }

        public static new ATag Instance() {
            if (_instance == null) {
                _instance = new DivOpenTag();
            }

            return (_instance);
        }
    }


    public class DivCloseTag : ATag
    {
        static DivCloseTag _instance = null;

        protected DivCloseTag() {
            _stencil = "</div>";
        }

        public static new ATag Instance() {
            if (_instance == null) {
                _instance = new DivCloseTag();
            }

            return (_instance);
        }
    }


    public class SpanOpenTag : ATag
    {
        static SpanOpenTag _instance = null;

        /// <summary>
        /// Attribute passed when this class is used must take the form of a plain CSS-class name. 
        /// </summary>
        protected SpanOpenTag() {
            _stencil = "<span class=\"@attribute@\">";
        }

        public static new ATag Instance() {
            if (_instance == null) {
                _instance = new SpanOpenTag();
            }

            return (_instance);
        }
    }


    public class SpanCloseTag : ATag
    {
        static SpanCloseTag _instance = null;

        protected SpanCloseTag() {
            _stencil = "</span>";
        }

        public static new ATag Instance() {
            if (_instance == null) {
                _instance = new SpanCloseTag();
            }

            return (_instance);
        }
    }

}
