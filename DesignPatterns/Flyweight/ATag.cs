﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Flyweight
{
    /* this is the abstract root for the individual Flyweight classes in the Flyweight design pattern; 
     * it provides them a shared light implementation, and defines the Singleton pattern for each, 
     * which is common with the Flyweight pattern; this class inherits from others to allow integration 
     * into the context-providing structure, in the Composite pattern, which is also common with Flyweight */

    public abstract class ATag : AElement // in turn, : AElement 
    {
        /* since all state is maintained by the context, I'm not retaining anything except 
 * the stencil, and subclasses don't need to override anything */

        protected string _stencil;

        protected ATag() {
            /* no operations; implemented in subclasses by defining contents of _stencil */
        }

        /* prompt for the Singleton pattern, useful with Flyweight pattern */
        public ATag Instance() {
            throw new NotImplementedException();
        }

        public string ToHtml(string context)  /* verified */  {
            return (_stencil.Replace("@attribute@", context));
        }
    }

}
