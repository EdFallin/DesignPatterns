﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.ChainOfResponsibility
{
    /* in the Chain Of Responsibility pattern, this is the class that all chained objects inherit from to allow 
     * them to respond to a request or to pass it along to any successor (or produce a default response) */

    public abstract class AInfoNode
    {
        /* reference to any next objecct in the Chain Of Responsibility */
        protected AInfoNode _successor = null;

        /* the method that follows the chain; subclasses override this _without_ calling 
         * it on ~base in order to answer a question when it reaches them */
        public virtual string Answer(string question)  /* verified */  {
            if (_successor == null) {
                return ("There is no answer.");
            }

            return (_successor.Answer(question));
        }

        /* these two methods turn nodes into links in the chain */
        public void LinkTo(AInfoNode successor)  /* verified */  {
            this._successor = successor;
        }

        public void LinkFrom(AInfoNode predecessor)  /* verified */  {
            predecessor._successor = this;
        }

    }
}
