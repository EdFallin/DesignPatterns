﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.ChainOfResponsibility
{
    public class NumberInfoNode : AInfoNode
    {
        /* answer or pass to next link via the abstract base class' implemn */
        public override string Answer(string question)  /* verified */  {
            /* only answers if entire ~question is digits */
            bool allAreDigits = true;

            foreach (char c in question) {
                if (!char.IsDigit(c)) {
                    allAreDigits = false;
                    break;  // stop at first 
                }
            }

            if (allAreDigits) {
                return ("Numbers are the answer.");
            }

            /* only if this object can't answer */
            return (base.Answer(question));
        }
    }
}
