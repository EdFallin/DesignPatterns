﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.ChainOfResponsibility
{
    public class SentenceInfoNode : AInfoNode
    {
        public override string Answer(string question)  /* verified */  {
            /* if this object can answer, it answers 
             * and the rest of the chain is skipped */
            if (question.Contains(' ')) {
                return ("Your question was a sentence.");
            }

            /* if this object can't answer, use base class' implemn 
             * to pass to next link or to provide default response */
            return (base.Answer(question));
        }
    }
}
