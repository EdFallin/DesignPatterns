﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.ChainOfResponsibility
{
    public class WordInfoNode : AInfoNode
    {
        public override string Answer(string question)  /* verified */  {
            /* if ~question is one all-letters word, answer; 
             * otherwise pass to next link, via base implemn */

            bool allCharsAreValid = true;

            foreach (char c in question) {
                if (c == ' ' || char.IsDigit(c)) {
                    allCharsAreValid = false;
                    break;  // stop at first 
                }
            }

            if (allCharsAreValid) {
                return ("Word!");
            }

            /* to next link if can't answer */
            return (base.Answer(question));
        }
    }
}
