﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Visitor
{
    /* a concrete Visitor subclass that performs its own distinct operations on the traversed structure; 
     * in this case, distinct in effect, but still the same problem domain as the other Visitor */

    public class SingularizeVisitor : AWordVisitor
    {
        public override string VisitAdjective(Adjective adjective)  /* verified */  {
            /* adjectives are not declined */
            return (adjective.Word);
        }

        public override string VisitAdverb(Adverb adverb)  /* verified */  {
            /* adverbs are not declined */
            return (adverb.Word);
        }

        public override string VisitArticleDefinition(ArticleDefinition article)  /* verified */  {
            /* interpreting abstract forms; assuming no /an/'s needed */
            List<string> abstracts = new List<string> { "def", "indef" };
            List<string> concretes = new List<string> { "the", "a" };

            int index = abstracts.IndexOf(article.Word);
            string concrete = concretes[index];

            return (concrete);
        }

        public override string VisitNoun(Noun noun)  /* verified */  {
            /* singular nouns are not declined */
            return (noun.Word);
        }

        public override string VisitPronounDefinition(PronounDefinition pronoun)  /* verified */  {
            /* interpreting abstract forms of these; some are declined, others not; skipping /we/ */
            List<string> abstracts = new List<string> { "first", "second", "third m", "third f" };
            List<string> concretes = new List<string> { "I", "you", "he", "she" };

            int index = abstracts.IndexOf(pronoun.Word);
            string concrete = concretes[index];

            return (concrete);
        }

        public override string VisitVerb(Verb verb)  /* verified */  {
            /* singular verbs are declined; "weak" verbs only here: adding an /s/ */
            string declined = verb.Word + "s";
            return (declined);
        }
    }
}
