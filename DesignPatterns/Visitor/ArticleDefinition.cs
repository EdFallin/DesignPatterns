﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Visitor
{
    /* an abstract definition of an article, so the Visitor objects have more to do; 
     * can be "def" ( /the/ ) or "indef" ( /a/ or /an/, but only /a/ here ) */

    public class ArticleDefinition : AWord
    {
        /// <summary>
        /// Valid article definitions are /def/ and /indef/. 
        /// </summary>
        public ArticleDefinition(string word) : base(word) {
        }

        public override string Visit(AWordVisitor visitor) {
            return (visitor.VisitArticleDefinition(this));
        }
    }
}
