﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Visitor
{
    /* the abstract base for all Visitors in the Visitor pattern; defines the basic interface */

    public abstract class AWordVisitor
    {
        /* the abstract Visitor knows the concrete classes it may visit, and has a method 
         * for each one, because each is assumed to need a special implementation; 
         * this is why changes at the subject end, especially, can make a lot of work */

        public abstract string VisitNoun(Noun noun);

        public abstract string VisitPronounDefinition(PronounDefinition pronoun);

        public abstract string VisitVerb(Verb verb);

        public abstract string VisitArticleDefinition(ArticleDefinition article);

        public abstract string VisitAdjective(Adjective adjective);

        public abstract string VisitAdverb(Adverb adverb);
    }
}