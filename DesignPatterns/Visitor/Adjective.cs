﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Visitor
{
    public class Adjective : AWord
    {
        public Adjective(string word) : base(word) {
        }

        public override string Visit(AWordVisitor visitor) {
            return (visitor.VisitAdjective(this));
        }
    }
}
