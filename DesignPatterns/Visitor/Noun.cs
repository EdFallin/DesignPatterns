﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Visitor
{
    public class Noun : AWord
    {
        public Noun(string word) : base(word) {
        }

        public override string Visit(AWordVisitor visitor) {
            return (visitor.VisitNoun(this));
        }
    }
}
