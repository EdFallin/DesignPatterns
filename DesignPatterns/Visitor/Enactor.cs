﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Visitor
{
    /* Enactor is the part of the Visitor pattern that traverses the structure of node objects, 
     * providing a Visitor to each one; it may also retain that structure (as this one does) */

    public class Enactor
    {
        /* effectively a linked list */
        public AWord Sentence { get; set; }

        public Enactor(params AWord[] sentence)  /* verified */  {
            /* convert params into linked list, assign to property */
            AWord word = sentence[0];

            for (int i = 1; i < sentence.Length; i++) {
                word.Next = sentence[i];
                word = sentence[i];
            }

            this.Sentence = sentence[0];  // remainder linked to this now 
        }

        public string Enact(AWordVisitor visitor)  /* passed */  {
            StringBuilder gatherer = new StringBuilder();


            /* ugly, but always a first word, and no enumerability complications */
            AWord lexeme = this.Sentence;
            do {
                string word = lexeme.Visit(visitor);
                gatherer.Append(word);
                gatherer.Append(" ");

                /* traversing; leaves ~lexeme itself null for trailing conditional */
                lexeme = lexeme.Next;
            }
            while (lexeme != null);


            /* finishing grammar */
            gatherer[0] = (char)(gatherer[0] - 32);  // caps are 32 earlier in ASCII; easier in StringBuilder than string! 
            string sentence = gatherer.ToString();
            sentence = sentence.Trim();
            sentence += ".";

            return (sentence);
        }
    }
}
