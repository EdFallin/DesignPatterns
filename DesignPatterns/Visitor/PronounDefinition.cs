﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Visitor
{
    public class PronounDefinition : AWord
    {
        /// <summary>
        /// Valid pronoun definitions are /first/, /second/, /third m/, and /third f/ 
        /// </summary>
        public PronounDefinition(string word) : base(word) {
        }

        public override string Visit(AWordVisitor visitor) {
            return (visitor.VisitPronounDefinition(this));
        }
    }
}
