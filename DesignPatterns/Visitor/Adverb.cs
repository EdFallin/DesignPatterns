﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Visitor
{
    public class Adverb : AWord
    {
        public Adverb(string word) : base(word) {
        }

        public override string Visit(AWordVisitor visitor) {
            return (visitor.VisitAdverb(this));
        }
    }
}
