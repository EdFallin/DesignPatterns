﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Visitor
{
    /* my system is a series of words forming a sentence but without declensions; 
     * each Visitor object adds declensions for a particular sentence output; 
     * for simplicity, I'm keeping this to standard ("weak") verbs and so on */

    /* a node to be visited, and retained in a simple linked list rather than a tree */
    public abstract class AWord
    {
        public string Word { get; set; } = null;

        public AWord Next { get; set; } = null;

        /* forces all concrete subclasses to have equivalent ctor, or a ctor that invokes this one */
        public AWord(string word) {
            this.Word = word;
        }

        /* in Patternary, this is described as providing an implementation that knows about concrete subclasses; 
         * I'm assuming that's a mistake in my precis or else just a poor design choice; instead I implement on subclasses */
        public abstract string Visit(AWordVisitor visitor);
    }
}
