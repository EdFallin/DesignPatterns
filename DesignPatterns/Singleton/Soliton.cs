﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Singleton
{
    /* Soliton is the Singleton class in the Singleton pattern, which always returns the same instance; 
     * with instance-only syntax in C# (the indexed property) as one case for not static */

    public class Soliton
    {
        #region Static field, the instance

        private static Soliton _soliton;

        #endregion Static field, the instance


        #region Instance properties, including indexer

        public List<string> Contents { get; set; }

        public string this[int index]  /* verified */  {
            get {
                if (index < 0 || index > this.Contents.Top()) {
                    return (null);
                }

                return (this.Contents[index]);
            }

            set {
                if (index < 0 || index > this.Contents.Top()) {
                    this.Contents.Add(value);
                    return;
                }

                this.Contents[index] = value;
            }
        }

        #endregion Instance properties, including indexer


        #region Private constructor, prevents direct initing

        private Soliton()  /* verified */  {
            this.Contents = new List<string>();
        }

        #endregion Private constructor, prevents direct initing


        #region Public convenience method to get instance, always the same

        public static Soliton Instance()  /* verified */  {
            if (_soliton == null) {
                _soliton = new Soliton();
            }

            return (_soliton);
        }

        #endregion Public convenience method to get instance, always the same

    }
}
