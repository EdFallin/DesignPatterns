﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Builder
{
    public class PlainTextBuilder : ABuilder
    {
        private readonly string NL = Environment.NewLine;

        /* no StylingStartProduct() or StylingEndProduct(), since plain text has no styling; 
         * illustrates Builder pattern's virtual-empty option for unneeded concrete outputs */

        public override string TextProduct(Token token)  /* verified */  {
            return (token.Content);  // plain text only; _not_ assumed to be whole line 
        }

        public override string ImageProduct(Token token)  /* verified */  {
            string text = NL + $"Image found at \"{ token.Content }\"" + NL;
            return (text);  // should be faux URL / path 
        }

        public override string SectionStartProduct(Token token)  /* verified */  {
            /* if any .Content, section starts converted to vertically spaced title lines */

            string text = string.Empty;

            if (!string.IsNullOrWhiteSpace(token.Content)) {
                text = NL + token.Content + NL;
            }

            return (text);
        }

        public override string SectionEndProduct(Token token)  /* verified */  {
            return (NL);  // section ends converted to (extra) line breaks 
        }

    }
}
