﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Builder
{
    public class HtmlBuilder : ABuilder
    {
        public override string StylingStartProduct(Token token)  /* verified */  {
            string fauxTag = $"<style class=\"{ token.Content }\" >";  // I wish! 
            return (fauxTag);
        }

        public override string StylingEndProduct(Token token)  /* verified */  {
            string fauxTag = "</style>";
            return (fauxTag);
        }

        public override string TextProduct(Token token)  /* verified */  {
            return (token.Content);  // should contain only plain text 
        }

        public override string ImageProduct(Token token)  /* verified */  {
            string tag = $"<img src=\"{ token.Content }\" />";
            return (tag);
        }

        public override string SectionStartProduct(Token token)  /* verified */  {
            string tag = null;

            if (!string.IsNullOrWhiteSpace(token.Content)) {
                tag = $"<div id=\"{ token.Content }\" >";
            }
            else {
                tag = $"<div>";
            }

            return (tag);
        }

        public override string SectionEndProduct(Token token)  /* verified */  {
            string tag = "</div>";
            return (tag);
        }

    }
}
