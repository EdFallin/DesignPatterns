﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Builder
{
    /* in the Builder pattern, this is an element of the input that the Director passes to a Builder concrete class */

    public class Token
    {
        public TokenType Type { get; protected set; }
        public string Content { get; protected set; }

        public Token(TokenType type, string content) {
            this.Type = type;
            this.Content = content;
        }
    }

    public enum TokenType
    {
        None,
        Text,
        Image,
        StylingStart,
        StylingEnd,
        SectionStart,
        SectionEnd,
    }
}
