﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Builder
{
    /* in the Builder pattern, this class takes an instance of the abstract ABuilder class, which is actually of a concrete subclass; 
     * the Builder uses the abstract methods defined to assemble the correct output, not caring about the exact results */

    public class Director
    {
        #region Fields

        ABuilder _builder = null;

        TokenType[] _types;
        Producer[] _producers;
        Dictionary<TokenType, Producer> _producerByToken;

        #endregion Fields


        #region Constructors and dependencies

        public Director(ABuilder builder) {
            _builder = builder;
            this.InitDelegationDictionary();
        }

        private void InitDelegationDictionary() {
            /* setting up delegation; in a real use, this would be done on the class, or similar */
            _types = new TokenType[] { TokenType.Text, TokenType.Image,
                    TokenType.StylingStart, TokenType.StylingEnd,
                    TokenType.SectionStart, TokenType.SectionEnd, TokenType.None };

            _producers = new Producer[] { _builder.TextProduct, _builder.ImageProduct,
                    _builder.StylingStartProduct, _builder.StylingEndProduct,
                    _builder.SectionStartProduct, _builder.SectionEndProduct, null };

            _producerByToken = new Dictionary<TokenType, Producer>();
            _producerByToken.FromSets(_types, _producers);
        }

        #endregion Constructors and dependencies


        #region Building methods

        public string Build(Queue<Token> input)  /* passed */  {
            string output = string.Empty;  // more readable than StringBuilder for learnings code 

            while (input.Count > 0) {
                Token token = input.Dequeue();  // ? also decrements .Count 

                /* switch, yuck, but other approaches too heavyweight for learnings code */
                switch (token.Type) {
                    case TokenType.Text:
                        output += _builder.TextProduct(token);
                        break;
                    case TokenType.Image:
                        output += _builder.ImageProduct(token);
                        break;
                    case TokenType.StylingStart:
                        output += _builder.StylingStartProduct(token);
                        break;
                    case TokenType.StylingEnd:
                        output += _builder.StylingEndProduct(token);
                        break;
                    case TokenType.SectionStart:
                        output += _builder.SectionStartProduct(token);
                        break;
                    case TokenType.SectionEnd:
                        output += _builder.SectionEndProduct(token);
                        break;
                    case TokenType.None:
                    /* fall-through */
                    default:
                        /* no ops */
                        break;
                }
            }

            return (output);
        }

        /* DeleBuild() illustrates use of delegates for a cleaner design; 
         * for optimal structuring this way, delegation dictionary would be 
         * set up on Director instance, not in build method, and so on */

        public string DeleBuild(Queue<Token> input)  /* passed */  {
            /* implementing again in a clean, switch-free way, using delegation */

            string output = string.Empty;  // more readable than StringBuilder for learnings code 

            /* crux: the building loop */
            while (input.Count > 0) {
                Token token = input.Dequeue();  // ? also decrements .Count 
                Producer producer = _producerByToken[token.Type];
                output += producer?.Invoke(token);  // null inlined harmlessly if type == .None 
            }

            return (output);
        }

        #endregion Building methods

    }


    /* needed for delegation version of building, unless I use Func<Token, string> */
    public delegate string Producer(Token token);
}
