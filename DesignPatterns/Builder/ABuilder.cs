﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Builder
{
    /* in the Builder pattern, this is the abstract class that all Builder concrete classes inherit from, 
     * and which defines all of the different token-translation methods those will implement */

    public abstract class ABuilder
    {
        /* methods here are virtual, with null-return defaults, to allow subclasses to 
         * override only the methods for which they will return output */

        public virtual string StylingStartProduct(Token token) {
            return (null);
        }

        public virtual string StylingEndProduct(Token token) {
            return (null);
        }

        public virtual string TextProduct(Token token) {
            return (null);
        }

        public virtual string ImageProduct(Token token) {
            return (null);
        }

        public virtual string SectionStartProduct(Token token) {
            return (null);
        }

        public virtual string SectionEndProduct(Token token) {
            return (null);
        }
    }
}
