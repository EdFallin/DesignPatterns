﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Composite
{
    public class TrueBranchNode : AParseNode
    {
        readonly string EXIT_JUMP = "long jump to @this-line + @offset;";
        readonly string BRANCH_OPS = "when true ops ops ops;";

        public string JumpOffset { get; set; }  // jump over false-branch at end of ops 

        public override string ToAssembler() {
            /* a sort of inorder traversal, to give Composite-driven ops context */
            string content = null;
            content = content.AppendLine(BRANCH_OPS);

            content += base.ToAssembler();  // rcalls  

            string exitJump = EXIT_JUMP.Replace("@offset", this.JumpOffset);
            content = content.AppendLine(exitJump);

            return (content);
        }

        public TrueBranchNode(string jumpOffset) {
            this.Token = TokenType.TrueBranch;
            this.JumpOffset = jumpOffset;
        }
    }
}
