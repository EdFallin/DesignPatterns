﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Composite
{
    /* represents any branch, like an 'if' or a loop condition of any kind; 
     * this node must use _preorder_ recursion to inline the branches correctly; 
     * when true, ops continue from BNE line; when false, they follow the BNE jump */

    public class BranchNode : AParseNode
    {
        readonly string COMPARE = $"compare value at @address with register and save to register;";
        readonly string BREAK_NOT_EQUAL = "if register is all 000s jump to @jump-not-equal;";

        public override string ToAssembler() {
            /* ops at this level; preorder */
            string content = null;

            content = content.AppendLine(COMPARE);
            content = content.AppendLine(BREAK_NOT_EQUAL);

            content += base.ToAssembler();  // rcalls 

            return (content);
        }

        public BranchNode() {
            this.Token = TokenType.Branch;
        }
    }
}
