﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Composite
{
    public class IdentifierNode : AParseNode
    {
        readonly string OPS_ADDRESS = $"get @address for @symbol to register;";
        readonly string OPS_VALUE = "get value at @address to register;";

        public string Symbol { get; set; }  // variable name 

        public override string ToAssembler() {
            string content = null;

            content += base.ToAssembler();  // rcalls  

            string getAddress = OPS_ADDRESS.Replace("@symbol", this.Symbol);
            content = content.AppendLine(getAddress);
            content = content.AppendLine(OPS_VALUE);

            return (content);
        }

        public IdentifierNode(string symbol) {
            this.Token = TokenType.Identifier;
            this.Symbol = symbol;
        }
    }
}
