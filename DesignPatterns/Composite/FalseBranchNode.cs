﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Composite
{
    public class FalseBranchNode : AParseNode
    {
        readonly string BRANCH_FIRST_OPS = "when false first ops ops ops;";
        readonly string BRANCH_LAST_OPS = "when false last ops ops ops;";

        public override string ToAssembler() {
            /* a sort of inorder traversal so branch structure is easily visible */

            string content = null;
            content = content.AppendLine(BRANCH_FIRST_OPS);

            content += base.ToAssembler();  // rcalls  

            content = content.AppendLine(BRANCH_LAST_OPS);

            return (content);
        }

        public FalseBranchNode() {
            this.Token = TokenType.FalseBranch;
        }
    }
}
