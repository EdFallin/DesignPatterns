﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Composite
{
    /* this is the abstract class that defines the Composite -- the nodes that will all be treated the same; 
     * the model here is a parse tree, though only quarter-implemented, and I'm not aiming much for realism; 
     * ToAssembler() is the method shared among all Composite nodes; for half-realism, called in postorder recursion */

    public abstract class AParseNode : IEnumerable<AParseNode>
    {
        public TokenType Token { get; protected set; }
        List<AParseNode> _children = new List<AParseNode>();

        /* the recursor that acts over the Composite structure; this base version simply performs 
         * the rcalls, and must be invoked within any overrides before local ops are performed; 
         * if a preorder recursion or something else, the order in concrete subclasses could vary */
        public virtual string ToAssembler() {
            /* rcalls */
            string content = null;

            foreach (AParseNode child in _children) {
                content += child.ToAssembler();
            }

            return (content);
        }

        #region Indexer

        public AParseNode this[int index] {
            get {
                /* invalid arg; minimum exception handling, fast exit */
                if ((index > _children.Top()) || (index < 0)) {
                    return (null);
                }

                return (_children[index]);
            }
            set {
                /* invalid arg; minimum exception handling, fast exit */
                if ((index > _children.Top()) || (index < 0)) {
                    return;
                }

                _children[index] = value;
            }
        }

        #endregion Indexer


        #region Methods

        public void Add(AParseNode node) {
            _children.Add(node);
        }

        public void Add(params AParseNode[] nodes) {
            _children.AddRange(nodes);
        }

        #endregion Methods


        #region IEnumerable

        public IEnumerator<AParseNode> GetEnumerator() {
            for (int i = 0; i < _children.Count; i++) {
                yield return (this[i]);
            }
        }

        IEnumerator IEnumerable.GetEnumerator() {
            for (int i = 0; i < _children.Count; i++) {
                yield return (this[i]);
            }
        }

        #endregion IEnumerable
    }

    public enum TokenType {
        None,
        Identifier,
        Number,
        Branch,
        TrueBranch,
        FalseBranch,
        Operator,
    }
}
