﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Composite
{
    /* a StructuralNode does not generate code, but only gathers all of something's code in one place; 
     * top node of any Composite hierarchy; if realism, may also be in other parse-tree locations */

    public class StructuralNode : AParseNode
    {
        public override string ToAssembler() {
            /* only rcalls here; no local content to interpolate */
            string content = null;

            content += base.ToAssembler();  // rcalls 
            return (content);
        }

        public StructuralNode() {
            this.Token = TokenType.None;
        }
    }
}
