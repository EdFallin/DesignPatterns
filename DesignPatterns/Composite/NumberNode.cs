﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Composite
{
    public class NumberNode : AParseNode
    {
        readonly string OPS_NUMBER = $"@number to register;";

        public string Number { get; set; }  // number literal 

        public override string ToAssembler() {
            /* leaf node: no valid children, no rcalls */
            string content = null;

            string getAddress = OPS_NUMBER.Replace("@number", this.Number);
            content = content.AppendLine(getAddress);

            return (content);
        }

        public NumberNode(string number) {
            this.Token = TokenType.Number;
            this.Number = number;
        }
    }
}
