﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Composite
{
    public class OperatorNode : AParseNode
    {
        readonly string OP_CODE = $"@operation from register and register to register;";

        public string Operator { get; set; }  // pseudo-opcode 

        public override string ToAssembler() {
            string content = null;

            content += base.ToAssembler();  // rcalls  

            string getAddress = OP_CODE.Replace("@operation", this.Operator);
            content = content.AppendLine(getAddress);

            return (content);
        }

        public OperatorNode(string symbol) {
            this.Token = TokenType.Operator;
            this.Operator = symbol;
        }
    }
}
