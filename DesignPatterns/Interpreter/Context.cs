﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Interpreter
{
    /// <summary>
    /// The Context class in the Interpreter pattern.
    /// <para />
    /// Contains whatever is needed by AExpression subclasses 
    /// to perform their operations and return results, 
    /// which in this case are stored here as well.
    /// </summary>
    public class Context
    {
        public List<decimal> Inputs { get; set; } = new List<decimal>();
        public decimal Output { get; set; } = 0.0M;
    }
}
