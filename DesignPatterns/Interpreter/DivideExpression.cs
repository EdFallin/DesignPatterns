﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Interpreter
{
    public class DivideExpression : AExpression
    {
        public override List<AExpression> Children { get; } = new List<AExpression>();

        /// <summary>
        /// Returns all the children up to .Split.
        /// No setter, because the parser is expected to set .Children.
        /// </summary>
        public List<AExpression> Dividends  /* passed */  {
            get {
                // all children except the divisor are dividends 
                return Children
                    .Where((x, n) => n < Split)
                    .ToList();
            }
        }

        /// <summary>
        /// Returns the child at .Split, which should be the last child.
        /// No setter, because the parser is expected to set .Children.
        /// </summary>
        public AExpression Divisor  /* passed */  {
            get {
                // expected always to be at this index 
                return Children[Split];
            }
        }

        public override void Interpret(Context context)  /* passed */  {
            // recursion over dividend children, assuming only one; 
            // otherwise results of each would have to be gathered 
            foreach (AExpression child in Dividends) {
                child.Interpret(context);
            }

            // localizing results, which get replaced next 
            decimal[] dividends = context.Inputs.ToArray();

            // recursion over single divisor child 
            Divisor.Interpret(context);

            // dividing sums of dividends and divisor/s 
            decimal dividend = dividends.Sum();
            decimal divisor = context.Inputs.Sum();
            decimal result = dividend / divisor;

            // replacement of inputs with result for r-caller 
            context.Inputs.Clear();
            context.Inputs.Add(result);
        }
    }
}
