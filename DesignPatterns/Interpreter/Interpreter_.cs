﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Interpreter
{
    /// <summary>
    /// This class is the interpreter consumer in the Interpreter pattern.
    /// <para />
    /// The trailing underscore in the name reduces namespacing in usages.
    /// </summary>
    public class Interpreter_
    {
        /// <summary>
        /// Text in the target language in, results out.
        /// </summary>
        public decimal Interpret(string sentence)  /* passed */  {
            /* algorithm: 
             *     lex the input, parse the tokens, 
             *     call the interpret method on the root, 
             *     and return the result 
             */

            Lexer lexer = new Lexer();
            Parser parser = new Parser();

            string[] tokens = lexer.Tokenize(sentence);
            AExpression tree = parser.Parse(tokens);

            Context context = new Context();

            tree.Interpret(context);

            return context.Output;
        }
    }
}
