﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Interpreter
{
    public class StoreExpression : AExpression
    {
        /// <summary>
        /// Override of superclass property, but should generally not be used.
        /// <para/>
        /// Use .Child instead.
        /// </summary>
        public override List<AExpression> Children { get; } = new List<AExpression>();

        public override void Interpret(Context context) {
            /* essentially just causes its NumbersExpression child to store its numbers */

            // recursion; only one child 
            AExpression child = Children[0];
            child.Interpret(context);
        }
    }
}
