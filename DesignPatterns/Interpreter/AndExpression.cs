﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Interpreter
{
    public class AndExpression : AExpression
    {
        public override List<AExpression> Children { get; } = new List<AExpression>();

        public override void Interpret(Context context)  /* passed */  {
            List<decimal> results = new List<decimal>();

            // recursion over all children and local gathering of their values 
            foreach (AExpression child in Children) {
                child.Interpret(context);
                results.AddRange(context.Inputs);
            }

            // replacement of any (child's) inputs with all gathered for r-caller 
            context.Inputs.Clear();
            context.Inputs.AddRange(results);
        }
    }
}
