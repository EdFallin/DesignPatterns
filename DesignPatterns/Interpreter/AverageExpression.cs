﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Interpreter
{
    public class AverageExpression : AExpression
    {
        public override List<AExpression> Children { get; } = new List<AExpression>();

        public override void Interpret(Context context)  /* passed */  {
            // recursion over any number of children 
            foreach (AExpression child in Children) {
                child.Interpret(context);
            }

            // actually averaging all of children's results; 
            // could actually do calculation in one step 
            decimal sum = context.Inputs.Sum();
            decimal count = context.Inputs.Count;
            decimal average = sum / count;

            // replacing inputs with result for r-caller 
            context.Inputs.Clear();
            context.Inputs.Add(average);
        }
    }
}
