﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Interpreter
{
    public class MaxExpression : AExpression
    {
        public override List<AExpression> Children { get; } = new List<AExpression>();

        public override void Interpret(Context context)  /* passed */  {
            // recursion; only one child 
            AExpression child = Children[0];
            child.Interpret(context);

            decimal max = context.Inputs.Max();

            // replacement of existing inputs with results for r-caller 
            context.Inputs.Clear();
            context.Inputs.Add(max);
        }
    }
}
