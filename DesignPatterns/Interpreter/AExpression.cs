﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Interpreter
{
    /// <summary>
    /// Base class for all expressions in the target language.
    /// </summary>
    public abstract class AExpression
    {
        public const int NONE = 0;

        /// <summary>
        /// Allows use of AExpression subclasses without casting.
        /// <para/>
        /// Subclasses must equate to their own child handling.
        /// </summary>
        public abstract List<AExpression> Children { get; }

        /// <summary>
        /// If a subclass has distinct left and right child subtrees, 
        /// this is the index in .Children at which the right subtree starts.
        /// </summary>
        public int Split { get; set; } = NONE;

        /// <summary>
        /// The key method of the interpreter pattern.
        /// </summary>
        public abstract void Interpret(Context context);

    }
}
