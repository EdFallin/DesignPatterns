﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Interpreter
{
    /// <summary>
    /// A class to isolate throws I make during parsing rather than those thrown by the framework.
    /// </summary>
    public class ParseException : Exception
    {
        public ParseException(string message) : base(message)  /* ok */  {
            /* no operations */
        }

        public ParseException  /* passed */
            (Queue<string> tokens, int total, string actual, params string[] expecteds)
            : this(MessageFromArgs(tokens, total, actual, expecteds)) {
            /* no operations */
        }


        private static string MessageFromArgs(Queue<string> tokens, int total, string actual, params string[] expecteds)  /* verified */  {
            // wrapping each expected token in double quotes 
            for (int at = 0; at < expecteds.Length; at++) {
                expecteds[at] = $"\"{ expecteds[at] }\"";
            }

            // converting token array to a list with " or " between 
            string expected = string.Join(" or ", expecteds);

            // changing actual equivalently for consistency next 
            actual = $"\"{ actual }\"";

            // assembling whole message 
            string message
                = $"Syntax error at token { total - tokens.Count }:"
                + "  "
                + $"Expected { expected }, encountered { actual }.";

            return message;
        }

    }

}
