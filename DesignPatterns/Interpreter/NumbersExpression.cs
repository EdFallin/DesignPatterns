﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DesignPatterns.Interpreter
{
    public class NumbersExpression : AExpression
    {
        public const string NUMBER_EXPRESSION = @"(\d+)";

        public override List<AExpression> Children { get; } = new List<AExpression>();

        public string Numbers { get; set; }

        public override void Interpret(Context context)  /* passed */  {
            /* algorithm:  
             *     since NumbersExpression is a leaf, 
             *     simply convert its .Numbers to numbers 
             *     and retain them on the Context instance 
             */

            // get the number texts from the bracketed, comma-delimited list 
            MatchCollection texts = Regex.Matches(Numbers, NUMBER_EXPRESSION);

            List<decimal> numbers = new List<decimal>();

            // convert the number texts to numbers (initially integers) 
            foreach (Match text in texts) {
                int number = int.Parse(text.Value);
                numbers.Add(number);
            }

            // replacement of any existing inputs with current ones for r-caller 
            context.Inputs.Clear();
            context.Inputs.AddRange(numbers);
        }
    }
}
