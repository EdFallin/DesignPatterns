﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DesignPatterns;

namespace DesignPatterns.Interpreter
{
    public class Parser
    {
        #region Target grammar and algorithm

        /*  a recursive descent parser for this language:                                                                   
                "( store [1, 2] and store [3, 4] ) then average ) and ( store [5, 6] divide [7] ) ) then sum then output"   
                                                                                                                            
            BNF productions:                                                                                                
                Language => Action " then " " output " $$                                                                   
                Action => Pre-Action Post-Action                                                                            
                Pre-Action => Group | Provide                                                                               
                Group => "(" Action " " Grouper " " Action ")"                                                              
                Grouper => Sibling | Divide                                                                                 
                Sibling => "and"                                                                                            
                Divide => "divide"                                                                                          
                Provide => Store " " Numbers                                                                                
                Store => "store"                                                                                            
                Numbers => "[" one number, or several numbers with ", " between "]"                                         
                Post-Action => " then " "sum" | "max" | "min" | "count" | "average"                                         
                Post-Action => ø                                                                                            
                                                                                                                            
        */

        /* 
            Nullables:      
                Term                                                                    1       2       3   
                Language => Action " then " " output " $$                               n       n       n   
                Action => Pre-Action Post-Action                                        n       n       n   
                Pre-Action => Group | Provide                                           n       n       n   
                Group => "(" Action " " Grouper " " Action ")"                          n       n       n   
                Grouper => Sibling | Divide                                             n       n       n   
                Sibling => "and"                                                        n       n       n   
                Divide => "divide"                                                      n       n       n   
                Provide => Store " " Numbers                                            n       n       n   
                Store => "store"                                                        n       n       n   
                Numbers => "[" one number, or several numbers with ", " between "]"     n       n       n   
                Post-Action => " then " "sum" | "max" | "min" | "count" | "average"     n       n       n   
                Post-Action => ø                                                        Y       n       n   

            Firsts:         
                Term                                                                    1               2                       3   
                Language => Action " then " " output " $$                               ø               { "(", "store" }        =   
                Action => Pre-Action Post-Action                                        ø               { "(", "store" }        =   
                Pre-Action => Group | Provide                                           ø               { "(", "store" }        =   
                Group => "(" Action " " Grouper " " Action ")"                          "("             =                       =   
                Grouper => Sibling | Divide                                             ø               { "and", "divide" }     =   
                Sibling => "and"                                                        "and"           =                       =   
                Divide => "divide"                                                      "divide"        =                       =   
                Provide => Store " " Numbers                                            ø               "store"                 =   
                Store => "store"                                                        "store"         =                       =   
                Numbers => "[" one number, or several numbers with ", " between "]"     "[a, ... z]"    =                       =   
                Post-Action => " then " "sum" | "max" | "min" | "count" | "average"     { names }       =                       =   
                Post-Action => ø                                                        ø               =                       =   

            Follows:        
                Term                                                                    1        2                                      3   
                Language => Action " then " " output " $$                               ø        $$                                     =   
                Action => Pre-Action Post-Action                                        ø        { "and", "divide", ")", "then" }       =   
                Pre-Action => Group | Provide                                           ø        ø                                      =   
                Group => "(" Action " " Grouper " " Action ")"                          ø        ø                                      =   
                Grouper => Sibling | Divide                                             ø        ø                                      =   
                Sibling => "and"                                                        ø        ø                                      =   
                Divide => "divide"                                                      ø        ø                                      =   
                Provide => Store " " Numbers                                            ø        ø                                      =   
                Store => "store"                                                        ø        ø                                      =   
                Numbers => "[" one number, or several numbers with ", " between "]"     ø        ø                                      =   
                Post-Action => " then " "sum" | "max" | "min" | "count" | "average"     ø        ø                                      =   
                Post-Action => ø                                                        ø        ø                                      =   
        */

        #endregion Target grammar and algorithm


        #region Definitions

        // output and structural tokens 
        const string OUTPUT_TOKEN = @"output";
        const string START_TOKEN = @"(";
        const string AND_TOKEN = @"and";
        const string END_TOKEN = @")";
        const string THEN_TOKEN = @"then";
        const string AVERAGE_TOKEN = @"average";

        // storage and mathematical tokens 
        const string STORE_TOKEN = @"store";
        const string SUM_TOKEN = @"sum";
        const string COUNT_TOKEN = @"count";
        const string DIVIDE_TOKEN = @"divide";
        const string MAX_TOKEN = @"max";
        const string MIN_TOKEN = @"min";

        const string NUMBERS_TOKEN_REGEX = @"\[ *(\d+)(, *\d+)* *\]";

        const string OPEN = @"[";
        const string CLOSE = @"]";

        public delegate AExpression ParseMethod(Queue<string> tokens);

        #endregion Definitions


        #region Type-introspection static properties

        /* These properties make unit-test calls of private methods less unstable. */

        public static string NameOfParseAction { get; } = nameof(Parser.ParseAction);
        public static string NameOfParseGroup { get; } = nameof(Parser.ParseGroup);
        public static string NameOfParseGrouper { get; } = nameof(Parser.ParseGrouper);
        public static string NameOfParseLanguage { get; } = nameof(Parser.ParseLanguage);
        public static string NameOfParseNumbers { get; } = nameof(Parser.ParseNumbers);
        public static string NameOfParsePostAction { get; } = nameof(Parser.ParsePostAction);
        public static string NameOfParsePreAction { get; } = nameof(Parser.ParsePreAction);
        public static string NameOfParseStore { get; } = nameof(Parser.ParseStore);
        public static string NameOfParseDivide { get; } = nameof(Parser.ParseDivide);
        public static string NameOfParseAnd { get; } = nameof(Parser.ParseAnd);
        public static string NameOfIsANumbersToken { get; } = nameof(Parser.IsANumbersToken);
        public static string NameOfTotal { get; } = nameof(Parser._total);

        #endregion Type-introspection static properties


        #region Fields

        /// <summary>
        /// Original count of the tokens in the input to Parse().
        /// <para />
        /// Used to provide locations of syntax errors if they are found.
        /// </summary>
        private int _total;

        #endregion Fields


        #region ParseMethod components

        /* inverted dependencies, set in a dependency of the constructor, 
         * which can be replaced for unit tests of other parsing methods */

        private ParseMethod LanguageParser { get; set; }
        private ParseMethod ActionParser { get; set; }
        private ParseMethod PreActionParser { get; set; }
        private ParseMethod GroupParser { get; set; }
        private ParseMethod GrouperParser { get; set; }
        private ParseMethod AndParser { get; set; }
        private ParseMethod DivideParser { get; set; }
        private ParseMethod StoreParser { get; set; }
        private ParseMethod NumbersParser { get; set; }
        private ParseMethod PostActionParser { get; set; }

        #endregion ParseMethod components


        #region Constructors and dependencies

        /// <summary>
        /// Default constructor.
        /// <para />
        /// Sets default parsers used in cross-recursion started from Parse().
        /// </summary>
        public Parser() {
            SetParsers();
        }

        /// <summary>
        /// A dependency of the constructor.
        /// <para />
        /// Sets default dependencies used in cross-recursion.
        /// <para/>
        /// These dependencies can be changed in unit tests.
        /// </summary>
        private void SetParsers() {
            LanguageParser = ParseLanguage;
            ActionParser = ParseAction;
            PreActionParser = ParsePreAction;
            GroupParser = ParseGroup;
            GrouperParser = ParseGrouper;
            AndParser = ParseAnd;
            DivideParser = ParseDivide;
            StoreParser = ParseStore;
            NumbersParser = ParseNumbers;
            PostActionParser = ParsePostAction;
        }

        #endregion Constructors and dependencies


        #region Parse()

        /// <summary>
        /// Returns a parse tree based on the token sequence passed as arg.
        /// Uses a recursive descent parser algorithm.
        /// </summary>
        public AExpression Parse(string[] source) {
            // different each time 
            _total = source.Length;

            // making the tokens a queue so they can be tossed as they are consumed 
            Queue<string> tokens = new Queue<string>(source);

            // all cross-recursive parsing 
            AExpression tree = LanguageParser(tokens);

            // whole tree should have been set during cross-recursion 
            return tree;
        }

        #endregion Parse()


        #region Cross-recursive dependencies of Parse()

        private AExpression ParseLanguage(Queue<string> tokens)  /* passed */  {
            /* the parsed language starts with a vertical trunk from an OutputAction root; 
             * the remainder of the parse tree is a subtree from its sole child */

            // first token has to be an action 
            AExpression subtree = ActionParser(tokens);

            // the following tokens have to exist after all other tokens used 

            string token = tokens.Dequeue();

            // has to be next 
            if (token != THEN_TOKEN) {
                throw new ParseException(tokens, _total, token, THEN_TOKEN);
            }

            token = tokens.Dequeue();

            // has to be next 
            if (token != OUTPUT_TOKEN) {
                throw new ParseException(tokens, _total, token, OUTPUT_TOKEN);
            }

            // has to be true 
            if (tokens.Count != 0) {
                throw new ParseException(
                    "Tokens were encountered after the expected end of input."
                );
            }

            // assembling top of whole parse tree 
            AExpression root = new OutputExpression();
            root.Children.Add(subtree);

            /* now parsing is done; whole tree to public caller */
            return root;
        }

        private AExpression ParseAction(Queue<string> tokens)  /* passed */  {
            /* a parsed action forms a vertical trunk in a tree / subtree; 
             * any pre-action is the sole child of the post-action */

            // every action starts with a pre-action at the same token 
            AExpression preAction = PreActionParser(tokens);

            // next token must be one of a few that can come next in the syntax 
            string token = tokens.Peek();

            string[] canComeNext = { AND_TOKEN, DIVIDE_TOKEN, END_TOKEN, THEN_TOKEN };

            if (!canComeNext.Contains(token)) {
                throw new ParseException(tokens, _total, token, THEN_TOKEN);
            }

            // of the tokens that can come next, all end this subtree except "then" 
            if (token != THEN_TOKEN) {
                return preAction;
            }

            // the "then" is no longer needed 
            tokens.Dequeue();

            // every action ends with a post-action at token after "then" 
            AExpression postAction = PostActionParser(tokens);

            // subtree out to the caller for it to work into the parse tree 
            postAction.Children.Add(preAction);
            return postAction;
        }

        private AExpression ParsePreAction(Queue<string> tokens)  /* passed */  {
            // pre-actions start at the initial pre-action token 
            string token = tokens.Peek();

            if (token == START_TOKEN) {
                return GroupParser(tokens);
            }

            if (token == STORE_TOKEN) {
                return StoreParser(tokens);
            }

            // has to be the token for group-start or store 
            throw new ParseException(tokens, _total, token, START_TOKEN, STORE_TOKEN);
        }

        /// <summary>
        /// A group is the production with parens and either "and" or "divide" in the middle.
        /// </summary>
        private AExpression ParseGroup(Queue<string> tokens)  /* passed */  {
            // initial parenthesis can be dropped 
            tokens.Dequeue();

            // a group is an action, a grouper, and another action 
            AExpression leftChild = ActionParser(tokens);
            AExpression root = GrouperParser(tokens);
            AExpression rightChild = ActionParser(tokens);

            // ending parenthesis can be dropped 
            string token = tokens.Dequeue();

            // has to be this 
            if (token != END_TOKEN) {
                throw new ParseException(tokens, _total, token, END_TOKEN);
            }

            // post-order assembling the subtree to return 
            root.Children.Add(leftChild);
            root.Children.Add(rightChild);
            root.Split = 1;

            // output to caller 
            return root;
        }

        /// <summary>
        /// A grouper is either "and" or "divide", found in the middle of a group.
        /// </summary>
        private AExpression ParseGrouper(Queue<string> tokens)  /* passed */  {
            // groupers start at the initial token 
            string token = tokens.Peek();

            if (token == AND_TOKEN) {
                return AndParser(tokens);
            }

            if (token == DIVIDE_TOKEN) {
                return DivideParser(tokens);
            }

            // has to be the token for and or divide 
            throw new ParseException(tokens, _total, token, AND_TOKEN, DIVIDE_TOKEN);
        }

        private AExpression ParseAnd(Queue<string> tokens)  /* passed */  {
            // known to be an and token when this is called 
            string token = tokens.Dequeue();
            AndExpression node = new AndExpression();

            // out to the caller for it to work into the parse tree 
            return node;
        }

        private AExpression ParseDivide(Queue<string> tokens)  /* passed */  {
            // known to be a divide token when this is called 
            string token = tokens.Dequeue();
            DivideExpression node = new DivideExpression();

            // out to the caller for it to work into the parse tree 
            return node;
        }

        private AExpression ParseStore(Queue<string> tokens)  /* passed */  {
            // first token is "store" 
            string token = tokens.Dequeue();

            // second token should be a numbers token 
            token = tokens.Peek();

            // has to be a numbers token 
            if (!IsANumbersToken(token)) {
                throw new ParseException(tokens, _total, token, NUMBERS_TOKEN_REGEX);
            }

            // parsing the numbers token 
            AExpression numbers = NumbersParser(tokens);

            // post-order assembling the subtree to return 
            StoreExpression root = new StoreExpression();
            root.Children.Add(numbers);

            // output to return 
            return root;
        }

        #region Dependencies of ParseStore()

        private bool IsANumbersToken(string token)  /* passed */  {
            return Regex.IsMatch(token, NUMBERS_TOKEN_REGEX);
        }

        #endregion Dependencies of ParseStore()

        private AExpression ParseNumbers(Queue<string> tokens)  /* passed */  {
            // the token for the numbers 
            string token = tokens.Dequeue();

            NumbersExpression output = new NumbersExpression();

            // the token contains the actual numbers as a bracketed, comma'd list 
            output.Numbers = token;

            // output to caller 
            return output;
        }

        private AExpression ParsePostAction(Queue<string> tokens)  /* passed */  {
            // any preceding "then" has already been dequeued and tossed 
            string token = tokens.Dequeue();

            // for any throw message 
            string[] postActions = { SUM_TOKEN, MAX_TOKEN, MIN_TOKEN, COUNT_TOKEN, AVERAGE_TOKEN };

            // to be output 
            AExpression node = null;

            switch (token) {
                case SUM_TOKEN:
                    node = new SumExpression();
                    break;
                case MAX_TOKEN:
                    node = new MaxExpression();
                    break;
                case MIN_TOKEN:
                    node = new MinExpression();
                    break;
                case COUNT_TOKEN:
                    node = new CountExpression();
                    break;
                case AVERAGE_TOKEN:
                    node = new AverageExpression();
                    break;
                default:
                    throw new ParseException(tokens, _total, token, postActions);
            }

            // out to the caller for it to work into the parse tree 
            return node;
        }

        #endregion Cross-recursive dependencies of Parse()

    }
}
