﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Interpreter
{
    public class CountExpression : AExpression
    {
        public override List<AExpression> Children { get; } = new List<AExpression>();

        public override void Interpret(Context context)  /* passed */  {
            // recursion; only one child 
            AExpression child = Children[0];
            child.Interpret(context);

            // counting whatever the recursion produced 
            decimal[] countables = context.Inputs.ToArray();
            decimal count = countables.Length;

            // replacing the inputs with the count for r-caller
            context.Inputs.Clear();
            context.Inputs.Add(count);
        }
    }
}
