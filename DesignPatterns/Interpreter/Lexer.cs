﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Interpreter
{
    public class Lexer
    {
        #region Definitions

        private const char SPACE = ' ';
        private const char OPEN = '[';
        private const char CLOSE = ']';

        #endregion Definitions


        #region Tokenize() and dependencies

        /// <summary>
        /// Lexes a sentence in the grammar for parsing.
        /// All sentences are assumed to match the language.
        /// </summary>
        public string[] Tokenize(string text)  /* passed */  {
            /* algorithm: read each character; 
             * [ starts a token that ends with ]; 
             * otherwise, a space separates two tokens 
             */

            // readying for handling character-wise 
            char[] chars = text.ToCharArray();
            bool isInNumbersState = false;

            // output and local passer 
            List<string> tokens = new List<string>();
            StringBuilder token = new StringBuilder();

            // traversing char by char and building or saving tokens 
            for (int at = 0; at < chars.Length; at++) {
                // at each non-numbers space, the token has ended, 
                // so save the token and start a new one 
                if (chars[at] == SPACE && !isInNumbersState) {
                    tokens.Add(token.ToString());
                    token = new StringBuilder();
                    continue;
                }

                // building the current token 
                token.Append(chars[at]);

                // shifting in and out of numbers state, in which spaces do not separate tokens 
                isInNumbersState = SetNumbersState(isInNumbersState, chars[at]);

                // because final token has no trailing delimiter 
                if (at == chars.Length - 1) {
                    tokens.Add(token.ToString());
                }
            }

            return tokens.ToArray();
        }

        /// <summary>
        /// Dependency of Tokenize().
        /// <para />
        /// Changes numbers state if the char is a delimiter for numbers.
        /// Otherwise, leaves the numbers state unchanged.
        /// </summary>
        private bool SetNumbersState(bool isInNumbersState, char c)  /* verified */  {
            if (c == OPEN) {
                isInNumbersState = true;
            }

            if (c == CLOSE) {
                isInNumbersState = false;
            }

            return isInNumbersState;
        }

        #endregion Tokenize() and dependencies
    }
}
