﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    /* the Abstract Factory itself in the pattern; 
     * consumer classes request one of these and get abstract but usable objects from it;
     * in fact, though, they get a concrete factory providing concrete objects; 
     * the concrete factory and objects simply are never exposed in their concrete forms  */

    public class EllipticGeometryFactory : AGeometryFactory
    {
        public override APoint BuildPoint() {
            return (new EllipticPoint());
        }

        public override ALine BuildLine() {
            return (new EllipticLine());
        }

        public override APlane BuildPlane() {
            return (new EllipticPlane());
        }

        public override AShape BuildShape() {
            return (new EllipticShape());
        }

        public override AVolume BuildVolume() {
            return (new EllipticVolume());
        }
    }
}
