﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    public class EllipticPoint : APoint
    {
        public override string ToString() {
            string coordinates = base.ToString();
            return ($"Elliptic { coordinates }, hemi-ellipse: { this.HemiEllipse }");
        }
    }
}
