﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    public class EllipticShape : AShape
    {
        public override Curvature ShapeCurvature(Extent extent) {
            /* for my purposes, small / smallish shapes are close enough to flat, others convex */
            int asInt = (int)extent;

            if (asInt < 2) {
                return (Curvature.Flat);
            }

            return (Curvature.Convex);
        }
    }
}
