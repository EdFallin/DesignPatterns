﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    public class EllipticVolume : AVolume
    {
        public override string Nature(int faces) {
            return ($"Fat solid with { faces } faces.");
        }
    }
}
