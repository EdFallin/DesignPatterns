﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    /* the Abstract Factory itself in the pattern; 
     * consumer classes request one of these and get abstract but usable objects from it;
     * in fact, though, they get a concrete factory providing concrete objects; 
     * the concrete factory and objects simply are never exposed in their concrete forms  */

    public abstract class AGeometryFactory
    {
        public abstract APoint BuildPoint();

        public abstract ALine BuildLine();

        public abstract APlane BuildPlane();

        public abstract AShape BuildShape();

        public abstract AVolume BuildVolume();
    }
}
