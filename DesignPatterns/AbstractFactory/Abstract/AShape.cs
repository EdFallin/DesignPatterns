﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    public abstract class AShape
    {
        public abstract Curvature ShapeCurvature(Extent extent);
    }

    public enum Extent
    {
        Tiny,
        Small,
        Medium,
        Large,
        Huge,
    }

    public enum Curvature
    {
        Flat,       // Euclidean 
        Convex,     // Elliptic 
        Concave,    // Hyperbolic 
    }
}
