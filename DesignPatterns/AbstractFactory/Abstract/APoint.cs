﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    public abstract class APoint
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        /* only defined for elliptical-geometry points, in which at least some opposites 
         * can sort-of be considered the same, according to Wikipedia */
        public int HemiEllipse { get; set; }

        public override string ToString() {
            return ($"X:{ this.X }, Y:{ this.Y }, Z:{ this.Z }");
        }
    }
}
