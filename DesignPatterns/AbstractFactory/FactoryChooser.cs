﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    /* in the Abstract Factory design pattern, consumer code may never know 
     * anything about which concrete factory / products it's using; 
     * to imitate this at a small scale, I choose one pseudo-randomly here; 
     * although not exactly part of the pattern, it is parallel to 
     * real opaque code / choices that would be made, but in other ways */

    public static class FactoryChooser
    {
        #region Fields

        static AGeometryFactory[] _factories = {
                new EuclideanGeometryFactory(),
                new EllipticGeometryFactory(),
                new HyperbolicGeometryFactory() };

        static int _lastIndex = -1;
        static Random _random = new Random(Environment.TickCount / TaskScheduler.Current.Id);  // semi-random 

        #endregion Fields


        public static AGeometryFactory Factory()  /* passed */  {
            int index = _lastIndex;

            /* never the same factory twice in a row */
            while (index == _lastIndex) {
                int raw = _random.Next(1, 100);
                index = raw % _factories.Length;
            }

            _lastIndex = index;  // to coerce difference next time 

            return (_factories[index]);
        }
    }
}
