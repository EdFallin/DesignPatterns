﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    /* the Abstract Factory itself in the pattern; 
     * consumer classes request one of these and get abstract but usable objects from it;
     * in fact, though, they get a concrete factory providing concrete objects; 
     * the concrete factory and objects simply are never exposed in their concrete forms  */

    public class EllipticalGeometryFactory : AGeometryFactory
    {
        public override APoint BuildPoint() { return (null); }

        public override ALine BuildLine() { return (null); }

        public override APlane BuildPlane() { return (null); }

        public override AShape BuildShape() { return (null); }

        public override AVolume BuildVolume() { return (null); }
    }
}
