﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    public class EllipticalLine : ALine
    {
        public override string Description(string start, string end) {
            string description = $"Line from { start } to { end } on a convex surface";
            return (description);
        }
    }
}
