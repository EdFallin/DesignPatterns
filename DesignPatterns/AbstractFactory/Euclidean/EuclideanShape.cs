﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    public class EuclideanShape : AShape
    {
        public override Curvature ShapeCurvature(Extent extent) {
            /* all (planar) shapes in Euclidean geomegry are flat */
            return (Curvature.Flat);
        }
    }
}
