﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    public class EuclideanLine : ALine
    {
        public override string Description(string start, string end) {
            string description = $"Straight line from { start } to { end }";
            return (description);
        }
    }
}
