﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    public class EuclideanVolume : AVolume
    {
        public override string Nature(int faces) {
            return ($"Normal solid with { faces } faces.");
        }
    }
}
