﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    public class EuclideanPlane : APlane
    {
        public override int Dimensions {
            get {
                return (2);
            }
        }
    }
}
