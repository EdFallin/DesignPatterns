﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    /* the Factory itself in the pattern; 
     * consumer classes request one of these and get but usable objects from it;
     * in fact, though, they get a concrete factory providing concrete objects; 
     * the concrete factory and objects simply are never exposed in their concrete forms  */

    public class EuclideanGeometryFactory : AGeometryFactory
    {
        public override APoint BuildPoint() {
            return (new EuclideanPoint());
        }

        public override ALine BuildLine() {
            return (new EuclideanLine());
        }

        public override APlane BuildPlane() {
            return (new EuclideanPlane());
        }

        public override AShape BuildShape() {
            return (new EuclideanShape());
        }

        public override AVolume BuildVolume() {
            return (new EuclideanVolume());
        }
    }
}
