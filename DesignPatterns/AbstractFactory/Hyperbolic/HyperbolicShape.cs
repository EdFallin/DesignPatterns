﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    public class HyperbolicShape : AShape
    {
        public override Curvature ShapeCurvature(Extent extent) {
            /* really small shapes are effectively flat, others concave */
            if (extent == Extent.Tiny) {
                return (Curvature.Flat);
            }

            return (Curvature.Concave);
        }
    }
}
