﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    public class HyperbolicLine : ALine
    {
        public override string Description(string start, string end) {
            string description = $"Line on a complex concave surface from { start } to { end }";
            return (description);
        }
    }
}
