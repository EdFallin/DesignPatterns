﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    public class HyperbolicPlane : APlane
    {
        public override int Dimensions {
            get {
                return (4);  // not really; but useful for differentiating 
            }
        }
    }
}
