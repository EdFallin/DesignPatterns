﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory
{
    public class HyperbolicVolume : AVolume
    {
        public override string Nature(int faces) {
            return ($"Skinny solid with { faces } faces.");
        }
    }
}
