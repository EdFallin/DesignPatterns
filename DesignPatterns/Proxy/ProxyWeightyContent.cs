﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Proxy
{
    /* in the Proxy pattern, this is the proxy object itself; 
     * it retains a reference to proxied instance, inited only when needed */

    public class ProxyWeightyContent : AContent
    {
        /* in the Proxy pattern, the object being proxied, always retained as field or prop, but null or metadata till needed */
        public WeightyContent Proxied { get; set; } = null;

        public ProxyWeightyContent() {
            /* .Size represents things about proxied object that proxy can store or calc on its own; 
             * here as learnings, getting from static value on proxied class, also used by its instances */
            this.Size = WeightyContent.ContentSize;
        }

        /* Proxy pattern: this can only be returned by initing proxied object and invoking its equivalent */
        public override string AsString() {
            /* Proxy pattern: in cases where proxying heavy objects, 
             * the proxy instance typically inits its referred object 
             * only when the latter is definitely needed */
            if (this.Proxied == null) {
                this.Proxied = new WeightyContent();
            }

            /* Proxy pattern: once the proxied object is inited, 
             * requests to proxy object are forwarded to proxied */
            return (this.Proxied.AsString());
        }
    }
}
