﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Proxy
{
    /* in the Proxy pattern, this plays the role of the heavy, remote, or 
     * security-constrained object that should only be inited when needed; 
     * to make it suitably heavy, I use random numbers with a delay for uniqueness */

    public class WeightyContent : AContent
    {
        /* Proxy pattern, learnings: a value proxy object can use without initing proxied object */
        public static int ContentSize = 128;

        double[] _randoms = new double[ContentSize];

        public override int Size {
            get {
                return (_randoms.Length);  // same as ContentSize 
            }
        }

        public WeightyContent() {
            /* here's the learnings weighty part: random numbers, from an all-new Random each time */
            double next;
            double last;

            for (int i = 0; i < _randoms.Length; i++) {
                Random random = new Random();
                last = next = random.NextDouble();

                /* the delay; fraction of a second needed for seed to change so new first NextDouble() */
                while (last == next) {
                    random = new Random();
                    next = random.NextDouble();
                }

                /* here, ~next is different than previous; one effect is to skip 0th random double generated */
                _randoms[i] = next;
            }
        }

        public override string AsString() {
            string numbers = string.Join(", ", _randoms);
            string output = $"{{{ numbers }}}";  // output = "{ n0, n1, ..., n2047 }" 

            return (output);
        }
    }
}
