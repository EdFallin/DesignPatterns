﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Proxy
{
    /* this is the abstract root class for Proxy instances and their proxied objects in the Proxy pattern, as well as their siblings in a hosting context */

    public abstract class AContent
    {
        /* in the Proxy pattern, some values may be locally available from 
         * within the proxy, while others require initing the proxied object; 
         * here, proxy can store Size, while AsString() requires proxied object */

        public virtual int Size { get; protected set; }

        public abstract string AsString();

    }
}
