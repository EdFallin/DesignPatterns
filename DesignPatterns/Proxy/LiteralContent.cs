﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;

namespace DesignPatterns.Proxy
{
    /* in the Proxy pattern, more or less, this is a class at the same level of abstraction in the context as a Proxy instance */

    public class LiteralContent : AContent
    {
        string _literal;

        public override int Size {
            get {
                return (_literal?.Length ?? 0);
            }
        }

        public LiteralContent(string content) {
            _literal = content;
        }

        public override string AsString() {
            return (_literal);
        }
    }
}
