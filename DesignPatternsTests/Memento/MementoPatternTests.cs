﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Memento;
using System.Threading;

namespace DesignPatternsTests.Memento
{
    [TestClass]
    public class MementoPatternTests
    {
        #region Individual Change Methods

        [TestMethod()]
        public void Topic_Add__Knowns__Assert_Expected_Number_And_Memento()  /* working */  {
            //**  groundwork  **//
            Topic topic = new Topic();


            //**  exercising the code  **//
            int actual = topic.Add("Sixteen");


            //**  gathering data  **//
            /* Memento objects must be opaque externally, and are here, so PrivateObject used */
            TopicMemento memento = topic.Memento;
            PrivateObject p = new PrivateObject(memento);
            Material added = (Material)p.GetFieldOrProperty("Added");
            Material subtracted = (Material)p.GetFieldOrProperty("Subtracted");


            //**  testing  **//
            Assert.AreEqual(16, actual);
            Assert.IsInstanceOfType(memento, typeof(TopicMemento));
            Assert.AreEqual(Material.Sixteen, added);
            Assert.AreEqual(Material.Zero, subtracted);
        }

        [TestMethod()]
        public void Topic_Subtract__Knowns__Assert_Expected_Number_And_Memento()  /* working */  {
            //**  groundwork  **//
            Topic topic = new Topic();

            // total of 22 
            topic.Add("Sixteen");
            topic.Add("Four");
            topic.Add("Two");


            //**  exercising the code  **//
            int actual = topic.Subtract("Four");


            //**  gathering data  **//
            /* since Memento object correctly opaque, PrivateObject access needed */
            TopicMemento memento = topic.Memento;
            PrivateObject p = new PrivateObject(memento);
            Material added = (Material)p.GetFieldOrProperty("Added");
            Material subtracted = (Material)p.GetFieldOrProperty("Subtracted");


            //**  testing  **//
            Assert.AreEqual(18, actual);  // 16 +4 +2 -4 
            Assert.IsInstanceOfType(memento, typeof(TopicMemento));
            Assert.AreEqual(Material.Zero, added);
            Assert.AreEqual(Material.Four, subtracted);
        }

        #endregion Individual Change Methods


        #region Pattern

        [TestMethod()]
        public void Memento_Pattern__Known_Adds__Assert_Correct_Restoration()  /* working */  {
            /* with a series of changes and their Mementos, reverting to a previous state */

            //**  groundwork  **//
            Topic topic = new Topic();

            List<TopicMemento> mementos = new List<TopicMemento>();

            Material[] changes = {
                Material.Four, Material.Eight, Material.SixtyFour,
                Material.Two, Material.One, Material.ThirtyTwo };

            int mostChanged = 0;


            // changes before the target time 
            for (int i = 0; i < 3; i++) {
                // making change 
                string asString = Enum.GetName(typeof(Material), changes[i]);
                mostChanged = topic.Add(asString);

                // saving state change 
                mementos.Add(topic.Memento);

                // delaying for time comparability 
                Thread.Sleep(3);
            }

            // the target time, and a delay for time comparability 
            DateTime between = DateTime.Now;
            Thread.Sleep(3);

            for (int i = 3; i < 6; i++) {
                // making change 
                string asString = Enum.GetName(typeof(Material), changes[i]);
                mostChanged = topic.Add(asString);

                // saving state change 
                mementos.Add(topic.Memento);

                // delaying for time comparability 
                Thread.Sleep(3);
            }

            //**  pre-testing  **//
            Assert.AreEqual(111, mostChanged);  // 4 +8 +64 +2 +1 +32 


            //**  exercising the code  **//
            int actual = topic.Restore(between, mementos);


            //**  testing  **//
            Assert.AreEqual(76, actual);  // 111 -32 -1 -2 
        }

        [TestMethod()]
        public void Memento_Pattern__Known_Adds_And_Subtracts__Assert_Correct_Restoration()  /* working */  {
            /* with a series of changes and their Mementos, reverting to a previous state */

            //**  groundwork  **//
            Topic topic = new Topic();

            List<TopicMemento> mementos = new List<TopicMemento>();

            Material[] adds = {
                Material.Four, Material.Eight, Material.SixtyFour,
                Material.Two, Material.One, Material.ThirtyTwo };

            Material[] subtracts = {
                Material.Two, Material.One, Material.SixtyFour,
                Material.Four, Material.Eight, Material.ThirtyTwo };

            int mostChanged = 0;
            int targetedState = -1;  // anti-default 

            DateTime between = DateTime.MaxValue;  // anti-default 


            // changes before the target time 
            for (int i = 0; i < adds.Length; i++) {
                // making change 
                string asString = Enum.GetName(typeof(Material), adds[i]);
                mostChanged = topic.Add(asString);

                // saving state change 
                mementos.Add(topic.Memento);

                // delaying for time comparability 
                Thread.Sleep(3);
            }

            // the target time, and a delay for time comparability 
            Thread.Sleep(3);

            for (int i = 0; i < subtracts.Length; i++) {
                // making change 
                string asString = Enum.GetName(typeof(Material), subtracts[i]);
                mostChanged = topic.Subtract(asString);

                // saving state change 
                mementos.Add(topic.Memento);

                // getting reverse-to point, 3 back from last change 
                if (i == 2) {
                    between = DateTime.Now;
                    targetedState = mostChanged;  // present value 
                }

                // delaying for time comparability 
                Thread.Sleep(3);
            }

            //**  pre-testing  **//
            Assert.AreEqual(0, mostChanged);  // everything added, then subtracted 
            Assert.AreEqual(44, targetedState);  // 3 before the end 


            //**  exercising the code  **//
            /* last 3 subtractions reversed, in effect added to 0 */
            int actual = topic.Restore(between, mementos);


            //**  testing  **//
            Assert.AreEqual(targetedState, actual);  // 111 -64 -2 -1 
        }

        #endregion Pattern

    }
}
