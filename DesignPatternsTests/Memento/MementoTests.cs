﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Memento;

namespace DesignPatternsTests.Memento
{
    [TestClass]
    public class MementoTests
    {
        #region Point, equatability

        #region Equal and not equal

        [TestMethod()]
        public void OperatorEquals_Test__Equal__Assert_True()  /* working */  {
            //**  groundwork  **//
            Point self = new Point(3, 5, 9);
            Point other = new Point(3, 5, 9);


            //**  exercising the code  **//
            bool actual = (self == other);


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        [TestMethod()]
        public void OperatorNotEqual_Test__Equal__Assert_False()  /* working */  {
            //**  groundwork  **//
            Point self = new Point(3, 5, 9);
            Point other = new Point(3, 5, 9);


            //**  exercising the code  **//
            bool actual = (self != other);


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void OperatorEquals_Test__Not_Equal__Assert_False()  /* working */  {
            //**  groundwork  **//
            Point self = new Point(4, 5, 7);
            Point other = new Point(3, 5, 9);


            //**  exercising the code  **//
            bool actual = (self == other);

            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void OperatorNotEqual_Test__Not_Equal__Assert_True()  /* working */  {
            //**  groundwork  **//
            Point self = new Point(3, 5, 9);
            Point other = new Point(4, 5, 8);


            //**  exercising the code  **//
            bool actual = (self != other);


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        #endregion Equal and not equal


        #region Self

        [TestMethod()]
        public void OperatorEquals_Test__Same_Object__Assert_True()  /* working */  {
            //**  groundwork  **//
            Point self = new Point(3, 5, 9);
            Point other = self;  // aliasing 


            //**  exercising the code  **//
            bool actual = (self == other);


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        [TestMethod()]
        public void OperatorNotEqual_Test__Same_Object__Assert_False()  /* working */  {
            //**  groundwork  **//
            Point self = new Point(3, 5, 9);
            Point other = self;


            //**  exercising the code  **//
            bool actual = (self != other);


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        #endregion Self


        #region Null args

        [TestMethod()]
        public void OperatorEquals_Test__One_Null__Assert_False()  /* working */  {
            //**  groundwork  **//
            Point self = new Point(3, 5, 9);
            object other = null;


            //**  exercising the code  **//
            bool actual = (self == other);


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void OperatorNotEqual_Test__One_Null__Assert_True()  /* working */  {
            //**  groundwork  **//
            Point self = new Point(3, 5, 9);
            Point other = null;


            //**  exercising the code  **//
            bool actual = (self != other);


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        [TestMethod()]
        public void OperatorEquals_Test__Both_Null__Assert_True()  /* working */  {
            //**  groundwork  **//
            Point self = null;
            Point other = null;


            //**  exercising the code  **//
            bool actual = (self == other);


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        [TestMethod()]
        public void OperatorNotEqual_Test__Both_Null__Assert_False()  /* working */  {
            //**  groundwork  **//
            Point self = null;
            Point other = null;


            //**  exercising the code  **//
            bool actual = (self != other);


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        #endregion Null args


        #region Hash code

        [TestMethod()]
        public void GetHashCode_Test__Same_Values__Assert_Same()  /* working */  {
            //**  groundwork  **//
            Point first = new Point(8, 6, 8);
            Point second = new Point(8, 6, 8);


            //**  exercising the code  **//
            int firstHash = first.GetHashCode();
            int secondHash = second.GetHashCode();


            //**  testing  **//
            Assert.AreEqual(firstHash, secondHash);
        }

        [TestMethod()]
        public void GetHashCode_Test__Reordered_Values__Assert_Different()  /* working */  {
            //**  groundwork  **//
            Point first = new Point(8, 6, 8);
            Point second = new Point(8, 8, 6);


            //**  exercising the code  **//
            int firstHash = first.GetHashCode();
            int secondHash = second.GetHashCode();


            //**  testing  **//
            Assert.AreNotEqual(firstHash, secondHash);
        }

        [TestMethod()]
        public void GetHashCode_Test__Different_Values__Assert_Different()  /* working */  {
            //**  groundwork  **//
            Point first = new Point(8, 6, 8);
            Point second = new Point(4, 3, 5);


            //**  exercising the code  **//
            int firstHash = first.GetHashCode();
            int secondHash = second.GetHashCode();


            //**  testing  **//
            Assert.AreNotEqual(firstHash, secondHash);
        }


        #endregion Hash code

        #endregion Point, equatability


        #region PolyLine, drawing & redrawing

        [TestMethod()]
        public void Draw_Test__Knowns__Assert_Same()  /* working */  {
            //**  groundwork  **//
            PolyLine poly = new PolyLine();

            Point[] args = {
                new Point(0, 1, 2), new Point(3, 8, 5),
                new Point(2, 4, 2), new Point(8, 6, 6),
                new Point(-5, -9, -4), new Point(2, -1, 10) };


            //**  exercising the code  **//
            poly.Draw(args[0]);
            poly.Draw(args[1]);
            poly.Draw(args[2]);
            poly.Draw(args[3]);
            poly.Draw(args[4]);
            poly.Draw(args[5]);


            //**  testing  **//
            for (int i = 0; i < args.Length; i++) {
                Assert.AreEqual(args[i], poly[i]);
            }
        }



        /* !!: reesume here with real values in Assert */

        [TestMethod()]
        public void Draw_Test__Knowns__Assert_Start_And_End()  /* working */  {
            //**  groundwork  **//
            PolyLine poly = new PolyLine();

            Point[] args = {
                new Point(0, 1, 2), new Point(3, 8, 5),
                new Point(2, 4, 2), new Point(8, 6, 6),
                new Point(-5, -9, -4), new Point(2, -1, 10) };


            //**  exercising the code  **//
            poly.Draw(args[0]);
            poly.Draw(args[1]);
            poly.Draw(args[2]);
            poly.Draw(args[3]);
            poly.Draw(args[4]);
            poly.Draw(args[5]);


            //**  testing  **//
            Assert.AreEqual(Point.Empty, poly.Start);
            Assert.AreEqual(Point.Empty, poly.End);
        }



        #endregion PolyLine, drawing & redrawing


    }
}
