﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Iterator;

namespace DesignPatternsTests.Iterator
{
    [TestClass]
    public class IteratorPatternTests
    {
        /* two tests with the same basis values, one array, one linked list; iterator itself always abstract */

        [TestMethod()]
        public void Pattern__Arrays__Assert_Correct()  /* working */  {
            //**  groundwork  **//
            IterableArray<char> iterable = new IterableArray<char>(8);

            char[] chars = { 'a', 'b', 'e', 'g', 'k', 'n', 'q', 'v' };

            for (int i = 0; i < chars.Length; i++) {
                iterable[i] = chars[i];
            }

            List<char> expecteds = chars.ToList();
            List<char> actualsFor = new List<char>();    // for better test-testing 
            List<char> actualsWhile = new List<char>();  // for better test-testing 


            //**  exercising the code  **//
            AIterator<char> iterator = iterable.GetIterator();  // concrete subclass as abstract superclass 

            /* iteration as 'for' loop; can't use Current() as first statement in loop, 
             * because that is for initialization only, and is only called once ever  */
            for (/* no initing clause */; iterator.HasMore(); iterator.MoveToNext()) {
                char c = iterator.Current();
                actualsFor.Add(c);
            }

            // since state not maintained by environment; could also get brand-new iterator 
            iterator.Reset();

            // iteration as 'while' loop 
            while (iterator.HasMore()) {
                actualsWhile.Add(iterator.Current());
                iterator.MoveToNext();
            }


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actualsFor);
            CollectionAssert.AreEqual(expecteds, actualsWhile);
        }

        [TestMethod()]
        public void Pattern__LinkedLists__Assert_Correct()  /* working */  {
            //**  groundwork  **//
            IterableLinkedList<char> iterable = new IterableLinkedList<char>('a');

            char[] chars = { 'a', 'n', 's', 'f', 'k', 'b', 'q', 'p' };

            // adding 2nd and later elements 
            for (int i = 1; i < chars.Length; i++) {
                iterable.Add(chars[i]);
            }

            List<char> expecteds = chars.ToList();
            List<char> actualsFor = new List<char>();    // for better test-testing 
            List<char> actualsWhile = new List<char>();  // for better test-testing 


            //**  exercising the code  **//
            AIterator<char> iteratorFor = iterable.GetIterator();  // concrete subclass as abstract superclass 

            for (/* */; iteratorFor.HasMore(); iteratorFor.MoveToNext()) {
                char c = iteratorFor.Current();
                actualsFor.Add(c);
            }

            // always as abstract here; new object with its own iteration cycle, instead of Reset() 
            AIterator<char> iteratorWhile = iterable.GetIterator();

            while (iteratorWhile.HasMore()) {
                char c = iteratorWhile.Current();
                actualsWhile.Add(c);

                /* in this simple implemn, failing to include this will cause crash on out-of-memory; 
                 * in real implementation without syntactic sugar, a fail-safe would be needed */
                iteratorWhile.MoveToNext();
            }


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actualsFor);
            CollectionAssert.AreEqual(expecteds, actualsWhile);
        }

    }
}
