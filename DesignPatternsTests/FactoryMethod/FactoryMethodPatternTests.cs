﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.FactoryMethod;

namespace DesignPatternsTests.FactoryMethod
{
    [TestClass]
    public class FactoryMethodPatternTests
    {
        [TestMethod()]
        public void Pattern__Swapping_Concretes__With_Uses__Assert_Expected_Product_Values()  /* working */  {
            /* reusing context and product symbols to demonstrate interchangeability yet concreteness */

            //**  groundwork  **//
            string textData = "data data data";
            byte[] data = textData.ToBytes();  // extension 

            AContext context;
            AProduct product;
            object display;

            AContext imageContext;

            #region Expecteds

            string expectedAt0 = "[[start-image]]data data data[[end-image]]";
            string expectedAt1 = "[[start-text]]blah blah blah and more and more[[end-text]]";
            object expectedAt2 = "[[start-formatted-text]]ring-a-ding-ding[[end-formatted-text]]";
            object expectedAt3 = "[[start-formatted-text]]ring-a-ding-ding [start-bold]plus[end-bold][[end-formatted-text]]";
            object expectedAt4 = "[[start-image]]\0at\0 d\0ta\0da\0a[[end-image]]";

            #endregion Expecteds

            #region Actuals

            string actualAt0;
            string actualAt1;
            object actualAt2;
            string actualAt3;
            string actualAt4;

            #endregion Actuals

            //**  exercising the code  **//
            /* reusing context and product symbols, invoking factory method on different 
             * concretes, getting different results from same symbols  */
            #region Image context and product

            context = imageContext = new ImageContext(data);  // ~imageContext for reuse later 
            product = context.MakeProduct();

            display = product.Display();
            actualAt0 = display.AsString();  // extension 

            #endregion Image context and product

            #region Text context and product

            context = new TextContext("blah blah blah");

            product = context.MakeProduct();  // same symbol, new object 

            /* as a related object, AProduct ~product should include these changes  */
            context.Change();
            context.Change();

            display = product.Display();
            actualAt1 = display.AsString();

            #endregion Text context and product

            #region Formatted-text context and product

            context = new FormattedTextContext("ring-a-ding-ding");
            product = context.MakeProduct();  // again, same symbol, new object 

            display = product.Display();
            actualAt2 = display.AsString();

            context.Change();

            display = product.Display();
            actualAt3 = display.AsString();

            #endregion Formatted-text context and product

            #region Image context and product again

            /* ~context as original image, and (new) related product */
            context = imageContext;
            context.Change();  // every 3rd char to null, in effect 
            product = context.MakeProduct();

            display = product.Display();
            actualAt4 = display.AsString();

            #endregion Image context and product again


            //**  testing  **//
            Assert.AreEqual(expectedAt0, actualAt0);
            Assert.AreEqual(expectedAt1, actualAt1);
            Assert.AreEqual(expectedAt2, actualAt2);
            Assert.AreEqual(expectedAt3, actualAt3);
            Assert.AreEqual(expectedAt4, actualAt4);
        }

    }
}
