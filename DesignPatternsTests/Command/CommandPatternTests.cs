﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Command;

namespace DesignPatternsTests.Command
{
    [TestClass]
    public class CommandPatternTests
    {
        public List<ACommand> Commands { get; set; } = new List<ACommand>();

        public CommandPatternTests()  /* verified */  {
            ACommand[] commands = {
                new AddTextCommand(),
                new CharToNumberCommand(),
                new SpaceOutCommand(),
                new UndoCommand(),
                new RedoCommand()
            };

            this.Commands.AddRange(commands);
        }


        #region Menu items

        [TestMethod()]
        public void Command__Known_Commands__Assert_Expected_Menu_Items()  /* working */  {
            //**  groundwork  **//
            Menu menu = new Menu();
            menu.AssignCommands(this.Commands);

            string[] expected = {
                "Add text", "Chars to numbers", "Space out text",
                "Undo", "Redo"
            };


            //**  exercising the code  **//
            string[] actual = menu.MenuItems;


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion Menu items


        #region Single commands

        [TestMethod()]
        public void Command__Text__Assert_Expected_Text()  /* working */  {
            //**  groundwork  **//
            StringBuilder subject = new StringBuilder("Subject ...");
            Menu menu = new Menu();
            menu.AssignCommands(this.Commands);


            //**  exercising the code  **//
            menu.InvokeItem("Add text", subject, "ends here.");


            //**  testing  **//
            Assert.AreEqual("Subject ... ends here.", subject.ToString());
        }

        [TestMethod()]
        public void Command__Chars_To_Numbers__Assert_Text()  /* working */  {
            //**  groundwork  **//
            StringBuilder subject = new StringBuilder("Learn your abc's on ABC.");
            Menu menu = new Menu();
            menu.AssignCommands(this.Commands);


            //**  exercising the code  **//
            menu.InvokeItem("Chars to numbers", subject);


            //**  testing  **//
            Assert.AreEqual("L51rn your 123's on ABC.", subject.ToString());
        }

        [TestMethod()]
        public void Command__Space__Assert_Expected_Text()  /* working */  {
            //**  groundwork  **//
            StringBuilder subject = new StringBuilder("Sample text");
            Menu menu = new Menu();
            menu.AssignCommands(this.Commands);


            //**  exercising the code  **//
            menu.InvokeItem("Space out text", subject);


            //**  testing  **//
            Assert.AreEqual("S a m p l e   t e x t ", subject.ToString());
        }

        #endregion Single commands


        #region Single reversals

        [TestMethod()]
        public void Command__Text__Reverse__Assert_Correct_Text()  /* working */  {
            //**  groundwork  **//
            StringBuilder subject = new StringBuilder("Subject ...");
            Menu menu = new Menu();
            menu.AssignCommands(this.Commands);

            menu.InvokeItem("Add text", subject, "ends here.");


            //**  exercising the code  **//
            menu.InvokeItem("Undo", subject);  // calls .Reverse() on original command 


            //**  testing  **//
            Assert.AreEqual("Subject ...", subject.ToString());
        }

        [TestMethod()]
        public void Command__Chars_To_Numbers__Reverse__Assert_Correct_Text()  /* working */  {
            //**  groundwork  **//
            StringBuilder subject = new StringBuilder("Learn your abc's on ABC.");
            Menu menu = new Menu();
            menu.AssignCommands(this.Commands);

            menu.InvokeItem("Chars to numbers", subject);


            //**  exercising the code  **//
            menu.InvokeItem("Undo", subject);  // calls .Reverse() on original command 
            //subject.Clear();
            //subject.Append("fail");


            //**  testing  **//
            Assert.AreEqual("Learn your abc's on ABC.", subject.ToString());
        }

        [TestMethod()]
        public void Command__Space_Out__Reverse__Assert_Correct_Text()  /* working */  {
            //**  groundwork  **//
            StringBuilder subject = new StringBuilder("Sample text");
            Menu menu = new Menu();
            menu.AssignCommands(this.Commands);

            menu.InvokeItem("Space out text", subject);


            //**  exercising the code  **//
            menu.InvokeItem("Undo", subject);  // calls .Reverse() on original command 
            //subject.Clear();
            //subject.Append("fail");


            //**  testing  **//
            Assert.AreEqual("Sample text", subject.ToString());
        }

        #endregion Single reversals


        #region Command series, with undo and redo

        [TestMethod()]
        public void Command__Known_Command_Series__Assert_Expected_Contents()  /* working */  {
            //**  groundwork  **//
            StringBuilder subject = new StringBuilder("The text");
            Menu menu = new Menu();
            menu.AssignCommands(this.Commands);

            string expectedInvokes = "T 8 5   t 5 x t   9 s   j u s t   1   6 5 w   w o r 4 s . ";
            string expectedUndoes = "The text is just a few words.";
            string expectedRedoes = "The text is just a few words.  And then some!  And some more!";
            string expectedLast = "T85 t5xt 9s just 1 65w wor4s.  An4 t85n som5!";

            //**  exercising the code and testing  **//
            menu.InvokeItem("Add text", subject, "is just a few words.");
            menu.InvokeItem("Chars to numbers", subject);
            menu.InvokeItem("Space out text", subject);

            Assert.AreEqual(expectedInvokes, subject.ToString());

            menu.InvokeItem("Undo", subject);  // no extra spaces, again 
            menu.InvokeItem("Undo", subject);  // now no digits, again 
            Assert.AreEqual(expectedUndoes, subject.ToString());

            menu.InvokeItem("Add text", subject, " And then some!");
            menu.InvokeItem("Add text", subject, " And some more!");
            Assert.AreEqual(expectedRedoes, subject.ToString());

            menu.InvokeItem("Undo", subject);  // dropping the last text 
            menu.InvokeItem("Chars to numbers", subject);
            Assert.AreEqual(expectedLast, subject.ToString());
        }

        #endregion Command series, with undo and redo

    }
}
