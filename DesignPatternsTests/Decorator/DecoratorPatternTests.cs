﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Decorator;

namespace DesignPatternsTests.Decorator
{
    [TestClass]
    public class DecoratorPatternTests
    {
        [TestMethod()]
        public void Implicit_Casting__Knowns__Assert_Functional_And_Correct()  /* working */  {
            //**  no groundwork  **//


            //**  exercising the code  **//
            Number eleventy = 111;
            int number = eleventy;


            //**  testing  **//
            Assert.AreEqual(111, eleventy.Value);
            Assert.AreEqual(111, number);
        }

        [TestMethod()]
        public void Pattern__Known_Concretes_And_Decorators__Assert_Expected_Functionalities()  /* working */  {
            //**  groundwork  **//
            ANumber numberOne = new Number(87898);
            ANumber numberTwo = new Number(87898);
            ANumber numberThree = new Number(87898);
            ANumber numberFour = new Number(87898);

            // decorating 4 different ways (1st 3 are adding to same chain) 
            ANumber low = new EmbiggeningNumber(numberOne);
            ANumber med = new EvenOddNumber(new EmbiggeningNumber(numberTwo));
            ANumber high = new EnumaNumber(new EvenOddNumber(new EmbiggeningNumber(numberThree)));
            ANumber alt = new EvenOddNumber(new EnumaNumber(numberFour));


            //**  exercising the code  **//
            // first values just to get mangling started; discarded 
            int actualLow = low.CalculateValue();    // +=7 
            int actualMed = med.CalculateValue();    // +=7 
            int actualHigh = high.CalculateValue();  // +=7 
            int actualAlt = alt.CalculateValue();

            actualLow = low.CalculateValue();    // 87898, +7, +7 == 87912 
            actualMed = med.CalculateValue();    // 87898, +7, +7, (odd) +1 == 87913 
            actualHigh = high.CalculateValue();  // 87898, +7, +7, (odd) +1, (@1) == 7

            /* an extra alt calculation to get a more interesting value */
            actualAlt = alt.CalculateValue();    // 87898, (@1 L to R) 7, (odd) +0 == 7 
            actualAlt = alt.CalculateValue();    // 87898, (@2 L to R) 8, (even) +0 == 8 

            //**  testing  **//
            Assert.AreEqual(87912, actualLow);
            Assert.AreEqual(87913, actualMed);
            Assert.AreEqual(7, actualHigh);
            Assert.AreEqual(8, actualAlt);
        }

    }
}
