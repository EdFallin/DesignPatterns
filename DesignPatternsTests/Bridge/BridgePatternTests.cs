﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Windows;
using System.Windows.Controls;

using DesignPatterns.Bridge;

namespace DesignPatternsTests.Bridge
{
    [TestClass]
    public class BridgePatternTests
    {
        /*  4 permutations possible; all 4 tested here  */

        [TestMethod()]
        public void Bridge_Label_To_Numeric__Knowns__Assert_Expecteds() {
            /*  the text is passed across the bridge, 
             *  where the square root of a last digit in the text 
             *  is calculated and converted to a string, 
             *  then sent back across the bridge, where it is placed in a Label  */

            //**  groundwork **//
            /*  internal bridge, external bridge  */
            InternalAnchorLabel nearEnd = new InternalAnchorLabel();
            nearEnd.FarEnd = new ExternalAnchorNumeric();

            string expected = Math.Sqrt(5.0).ToString();


            //**  exercising the code **//
            Label actual = nearEnd.LabelFromString("the square is 5");


            //**  testing **//
            Assert.AreEqual(expected, actual.Content);
        }

        [TestMethod()]
        public void Bridge_TextBox_To_Textual__Knowns__Assert_Expecteds() {
            //**  groundwork **//
            InternalAnchorTextBox nearEnd = new InternalAnchorTextBox();
            nearEnd.FarEnd = new ExternalAnchorTextual();

            int cAsInt = 'c';  // ! 

            string expected = new string('c', cAsInt);


            //**  exercising the code **//
            TextBox actual = nearEnd.TextBoxFromString("cde");


            //**  testing **//
            Assert.AreEqual(expected, actual.Text);
        }

        [TestMethod()]
        public void Bridge_Label_To_Textual__Knowns__Assert_Expecteds() {
            //**  groundwork **//

            InternalAnchorLabel nearEnd = new InternalAnchorLabel();
            nearEnd.FarEnd = new ExternalAnchorTextual();

            string expected = new string('p', 'p');  // in second arg, 'p' treated as int ! 


            //**  exercising the code **//
            Label actual = nearEnd.LabelFromString("proximity");


            //**  testing **//
            Assert.AreEqual(expected, actual.Content);
        }

        [TestMethod()]
        public void Bridge_TextBox_To_Numeric__Knowns__Assert_Expecteds() {
            //**  groundwork **//
            InternalAnchorTextBox nearEnd = new InternalAnchorTextBox();
            nearEnd.FarEnd = new ExternalAnchorNumeric();

            string expected = Math.Sqrt(3).ToString();


            //**  exercising the code **//
            TextBox actual = nearEnd.TextBoxFromString("proximity to 3");


            //**  testing **//
            Assert.AreEqual(expected, actual.Text);
        }

    }
}
