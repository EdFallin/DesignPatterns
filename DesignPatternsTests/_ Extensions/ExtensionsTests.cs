﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using System.Linq;

namespace DesignPatternsTests.__Extensions
{
    [TestClass]
    public class ExtensionsTests
    {
        [TestMethod()]
        public void FromSets__Knowns__Assert_Correct_Added_And_Count()  /* working */  {
            //**  groundwork  **//
            Dictionary<string, int> subject = new Dictionary<string, int>();

            string[] keys = { "one", "two", "three", "five" };
            List<int> values = new List<int> { 1, 2, 3, 5 };


            //**  exercising the code  **//
            subject.FromSets(keys, values);


            //**  testing  **//
            Assert.AreEqual(4, subject.Count);  // metadata 

            Assert.AreEqual(subject["two"], 2);
            Assert.AreEqual(subject["three"], 3);
            Assert.AreEqual(subject["one"], 1);
            Assert.AreEqual(subject["five"], 5);
        }

        [TestMethod()]
        public void FromSets__Repeats__Assert_Correct_Added_And_Count()  /* working */  {
            //**  groundwork  **//
            Dictionary<string, int> subject = new Dictionary<string, int>();

            /* repeated keys should be ignored */
            string[] keys = { "one", "two", "two", "three", "five", "five" };
            List<int> values = new List<int> { 1, 2, 7, 3, 5, 6 };


            //**  exercising the code  **//
            subject.FromSets(keys, values);


            //**  testing  **//
            Assert.AreEqual(4, subject.Count);  // metadata 

            Assert.AreEqual(subject["two"], 2);
            Assert.AreEqual(subject["three"], 3);
            Assert.AreEqual(subject["one"], 1);
            Assert.AreEqual(subject["five"], 5);
        }

        [TestMethod()]
        public void FromTuples__Known_Tuples__Assert_Correct_Dictionary_Contents()  /* working */  {
            //**  groundwork  **//
            (string, int)[] tuples = { ("first", 0), ("second", 2), ("last", 8) };

            Dictionary<string, int> expected = new Dictionary<string, int>();
            expected.Add("first", 0);
            expected.Add("second", 2);
            expected.Add("last", 8);


            //**  exercising the code  **//
            Dictionary<string, int> actual = new Dictionary<string, int>();
            actual = actual.FromTuples(tuples);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ToDictionary__Known_Tuples__Assert_Correct_Dictionary_Contents()  /* working */  {
            //**  groundwork  **//
            (string, int)[] tuples = { ("first", 0), ("second", 2), ("last", 8) };

            Dictionary<string, int> expected = new Dictionary<string, int>();
            expected.Add("first", 0);
            expected.Add("second", 2);
            expected.Add("last", 8);


            //**  exercising the code  **//
            Dictionary<string, int> actual = tuples.ToDictionary();


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

    }
}
