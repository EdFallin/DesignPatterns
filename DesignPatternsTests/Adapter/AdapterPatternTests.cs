﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Windows;
using System.Windows.Controls;

using DesignPatterns.Adapter;

namespace DesignPatternsTests.Adapter
{
    [TestClass]
    public class AdapterPatternTests
    {
        /*  in each of these tests, methods of an internal adapter class (AdapterShapes) are called, 
         *  which do some internal work and then pass derived values to an "external" class (ExternalInteger) 
         *  for it to do the key operations; its results are converted as needed and returned; 
         *  this is the Adapter pattern (value conversion / internal work in adapter is common but not defining)  */

        [TestMethod()]
        public void Circle_Area__Knowns__Assert_Expecteds() {
            //**  groundwork **//
            string radiusText = "7";

            AdapterShapes adapter = new AdapterShapes();


            //**  exercising the code **//
            string area = adapter.CircleArea(radiusText);


            //**  testing **//
            Assert.AreEqual("147", area);
        }

        [TestMethod()]
        public void Triangle_Area__Knowns__Assert_Expecteds() {
            //**  groundwork **//
            string baseText = "8";
            string heightText = "5";

            AdapterShapes adapter = new AdapterShapes();


            //**  exercising the code **//
            string area = adapter.TriangleArea(baseText, heightText);


            //**  testing **//
            Assert.AreEqual("20", area);
        }

        [TestMethod()]
        public void Square_Perimeter__Knowns__Assert_Expecteds() {
            //**  groundwork **//
            string sideText = "15";

            AdapterShapes adapter = new AdapterShapes();


            //**  exercising the code **//
            string perimiter = adapter.SquarePerimeter(sideText);


            //**  testing **//
            Assert.AreEqual("60", perimiter);
        }

        [TestMethod()]
        public void Circumference__Knowns__Assert_Expecteds() {
            //**  groundwork **//
            string radiusText = "9";

            AdapterShapes adapter = new AdapterShapes();


            //**  exercising the code **//
            string circumference = adapter.Circumference(radiusText);


            //**  testing **//
            Assert.AreEqual("54", circumference);
        }

        [TestMethod()]
        public void RingArea__Knowns__Assert_Expecteds() {
            //**  groundwork **//
            string outerRadiusText = "14";
            string innerRadiusText = "11";

            AdapterShapes adapter = new AdapterShapes();


            //**  exercising the code **//
            string area = adapter.RingArea(outerRadiusText, innerRadiusText);


            //**  testing **//
            Assert.AreEqual("225", area);
        }

    }
}
