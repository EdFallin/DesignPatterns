﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Singleton;

namespace DesignPatternsTests.Singleton
{
    [TestClass]
    public class SingletonPatternTests
    {
        [TestMethod()]
        public void SingletonPattern__Repeated_Symbols__Assert_Same_Identity()  /* working */  {
            //**  groundwork  **//
            Soliton first = Soliton.Instance();
            Soliton second = Soliton.Instance();
            Soliton third = Soliton.Instance();
            Soliton fourth = Soliton.Instance();
            Soliton fifth = Soliton.Instance();

            bool allSame = false;

            //**  exercising the code  **//
            /* Soliton not IEquatable, so address equality */
            allSame = (first == second);
            allSame &= (second == third);
            allSame &= (third == fourth);
            allSame &= (fourth == fifth);


            //**  testing  **//
            Assert.AreEqual(true, allSame);
        }

        [TestMethod()]
        public void SingletonPattern__Repeated_Symbols__Assert_Reusable_Content()  /* working */  {
            /* all the same, so defined on one symbol is defined on any other */

            //**  groundwork  **//
            Soliton first = Soliton.Instance();
            Soliton second = Soliton.Instance();
            Soliton third = Soliton.Instance();
            Soliton fourth = Soliton.Instance();
            Soliton fifth = Soliton.Instance();

            string[] expecteds 
                = { "first", "change two", "third", "change one", "fifth" };


            //**  exercising the code  **//
            /* adding to the end of the List<> on same Soliton */
            first[0] = "first";
            second[1] = "second";
            third[2] = "third";
            fourth[3] = "fourth";
            fifth[4] = "fifth";

            second[3] = "change one";
            fourth[1] = "change two";


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, fifth.Contents);
        }

    }
}
