﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Proxy;

namespace DesignPatternsTests.Proxy
{
    [TestClass]
    public class ProxyPatternTests
    {
        [TestMethod()]
        public void Proxy_Pattern__Knowns__Assert_Expecteds()  /* working */  {
            //**  groundwork  **//
            List<AContent> topic = new List<AContent>();

            AContent content_1 = new LiteralContent("First:");
            AContent content_2 = new LiteralContent(" ");
            AContent content_3 = new ProxyWeightyContent();  // Proxy pattern 
            AContent content_4 = new LiteralContent(" ");
            AContent content_5 = new LiteralContent("Second:");
            AContent content_6 = new LiteralContent(" ");
            AContent content_7 = new ProxyWeightyContent();  // Proxy pattern 

            topic.AddRange(
                content_1, content_2, content_3, content_4,
                content_5, content_6, content_7);

            // ~expectedSize not an actual string length, because doubles + punctuation aren't 1 char each 
            int expectedSize = 6 + 1 + 128 + 1 + 7 + 1 + 128;
            int actualSize = 0;

            string notExpected = "First: { } Second: { }";
            string actual = null;

            DateTime last;
            TimeSpan actualFirst;
            TimeSpan actualSecond;


            //**  exercising the code  **//

            // two timed traversals of the content: 
            //    first should be fast, since data is something proxies can return without initing slow real objects; 
            //    second should be slow, since data is something proxies must init slow real objects to get and return 

            // first, the fast, no-real-objects traversal 
            last = DateTime.Now;
            topic
                .ForEach(
                    x => { actualSize += x.Size; }  // weird Linq-style accumulator 
                    );
            actualFirst = DateTime.Now - last;

            // second, the slow, initing-real-objects traversal 
            last = DateTime.Now;
            topic
                .ForEach(
                    x => { actual += x.AsString(); }  // again the weird Linq accumulator, this time with strings 
                    );
            actualSecond = DateTime.Now - last;


            //**  testing  **//
            // first, the objects generated by the traversals 
            Assert.AreEqual(expectedSize, actualSize);
            Assert.AreNotEqual(notExpected, actual);

            // second, the timing of the traversals; second should be _much_ slower 
            Assert.IsTrue(
                actualSecond.Ticks > (10 * actualFirst.Ticks), 
                "Real-objects traversal was not as much slower than proxy-only traversal as expected.");
        }

    }
}
