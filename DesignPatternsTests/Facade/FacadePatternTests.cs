﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Facade;

namespace DesignPatternsTests.Facade
{
    [TestClass]
    public class FacadePatternTests
    {
        /* These tests verify each of the methods on the Facade class that each use multiple objects internally.
         * A couple of them also use some of those otherwise-internal objects directly, as Facade supports. */

        [TestMethod()]
        public void TotalCountAndAverage__Known_Numbers__Assert_Correct_Tuple_Values()  /* working */  {
            //**  groundwork  **//
            PolyOperationFacade topic = new PolyOperationFacade();

            decimal[] numbers = {
                17.6m, 43.9m, 18.0m, 26.4m, 77.3m, 91.6m
            };

            // expecteds 
            (decimal, decimal, decimal) expected
                = (numbers.Sum(), numbers.Length, (numbers.Sum() / numbers.Length));


            //**  exercising the code  **//
            (decimal, decimal, decimal) actual
                = topic.TotalCountAndAverage(numbers);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void LengthTotalAndProductSplits__Known_Numbers__Assert_Correct_Splits()  /* working */  {
            //**  groundwork  **//
            PolyOperationFacade topic = new PolyOperationFacade();

            List<decimal> left = new List<decimal> { 2.0m, 4.2m, 6.4m, 8.6m };
            decimal[] right = { 3.1m, 5.3m, 7.5m };

            // Facade pattern: direct use of an object the Facade's code employs 
            MultiplyOperation times = new MultiplyOperation();

            // expecteds 
            (decimal expL, decimal expT, decimal expP) = (
                left.Count - right.Length,
                left.Sum() - right.Sum(),
                times.Calculate(left) - times.Calculate(right)
            );


            //**  exercising the code  **//
            (decimal acL, decimal acT, decimal acP) = topic.LengthTotalAndProductSplits(left, right);


            //**  testing  **//
            Assert.AreEqual(expL, acL);
            Assert.AreEqual(expT, acT);
            Assert.AreEqual(expP, acP);
        }

        [TestMethod()]
        public void AverageOverGroups__No_Groups__Assert_Duple_Is_Both_Zeroes()  /* working */  {
            //**  groundwork  **//
            PolyOperationFacade topic = new PolyOperationFacade();

            (decimal total, decimal average) expected = (default, default);


            //**  exercising the code  **//
            (decimal total, decimal average) actual = topic.AverageOverGroups();


            //**  testing  **//
            Assert.AreEqual(expected.total, actual.total);
            Assert.AreEqual(expected.average, actual.average);
        }

        [TestMethod()]
        public void AverageOverGroups__Known_Groups__Assert_Correct_Duple_Values()  /* working */  {
            //**  groundwork  **//
            PolyOperationFacade topic = new PolyOperationFacade();

            // inputs 
            List<decimal> one = new List<decimal> { 1.0m, 2.0m, 3.0m, 4.1m };
            decimal[] two = { 5.1m, 6.1m, 7.1m };
            decimal[] three = { 8.2m, 9.2m };

            // again directly using an object that would otherwise by hidden behind the Facade 
            SumOperation sum = new SumOperation();

            // mixing syntaxes, only using the new object for the first group 
            decimal total = sum.Calculate(one) + two.Sum() + three.Sum();

            // expecteds 
            (decimal total, decimal average) expected = (3.0m, total / 3.0m);


            //**  exercising the code  **//
            (decimal total, decimal average) actual = topic.AverageOverGroups(one, two, three);


            //**  testing  **//
            Assert.AreEqual(expected.total, actual.total);
            Assert.AreEqual(expected.average, actual.average);
        }

    }
}
