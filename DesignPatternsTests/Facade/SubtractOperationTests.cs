﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Facade;

namespace DesignPatternsTests.Facade
{
    [TestClass]
    public class SubtractOperationTests
    {
        [TestMethod()]
        public void Calculate__Known_Numbers__Assert_Correct_Difference()  /* working */  {
            //**  groundwork  **//
            SubtractOperation topic = new SubtractOperation();

            decimal minuend = 99.208m;
            decimal subtrahend = 73.645m;

            decimal expected = minuend - subtrahend;


            //**  exercising the code  **//
            decimal actual = topic.Calculate(minuend, subtrahend);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

    }
}
