﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Facade;

namespace DesignPatternsTests.Facade
{
    [TestClass]
    public class DivideOperationTests
    {
        [TestMethod()]
        public void Calculate__Known_Numbers__Assert_Correct_Quotient()  /* working */  {
            //**  groundwork  **//
            DivideOperation topic = new DivideOperation();

            decimal dividend = 18.0m;
            decimal divisor = 7.3m;


            decimal expected = dividend / divisor;


            //**  exercising the code  **//
            decimal actual = topic.Calculate(dividend, divisor);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

    }
}
