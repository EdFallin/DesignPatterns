﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Facade;

namespace DesignPatternsTests.Facade
{
    [TestClass]
    public class CountOperationTests
    {
        [TestMethod()]
        public void Calculate__Known_Numbers__Assert_Correct_Count()  /* working */  {
            //**  groundwork  **//
            CountOperation topic = new CountOperation();
            List<decimal> numbers = new List<decimal> {
                6.7m, 2.9m, 8.84m, 10.05m, 37.11m, 3.76m
            };

            decimal expected = numbers.Count;


            //**  exercising the code  **//
            decimal actual = topic.Calculate(numbers);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

    }
}
