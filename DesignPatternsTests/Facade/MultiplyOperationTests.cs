﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Facade;

namespace DesignPatternsTests.Facade
{
    [TestClass]
    public class MultiplyOperationTests
    {
        [TestMethod()]
        public void Calculate__Known_Numbers__Assert_Correct_Product()  /* working */  {
            //**  groundwork  **//
            MultiplyOperation topic = new MultiplyOperation();

            // exactly six numbers 
            List<decimal> numbers = new List<decimal> {
                6.7m, 3.9m, 7.74m, 10.05m, 37.12m, 3.76m
            };

            decimal expected 
                = numbers[0] * numbers[1] * numbers[2]
                * numbers[3] * numbers[4] * numbers[5];


            //**  exercising the code  **//
            decimal actual = topic.Calculate(numbers);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

    }
}
