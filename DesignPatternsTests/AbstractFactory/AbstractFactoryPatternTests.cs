﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.AbstractFactory;

namespace DesignPatternsTests.AbstractFactory
{
    [TestClass]
    public class AbstractFactoryPatternTests
    {
        [TestMethod()]
        public void FactoryChooser__Random_Factory__Assert_Different_Consecutives()  /* working */  {
            //**  groundwork  **//
            AGeometryFactory[] actuals = new AGeometryFactory[5];


            //**  exercising the code  **//
            for (int i = 0; i < actuals.Length; i++) {
                actuals[i] = FactoryChooser.Factory();
            }


            //**  testing  **//
            for (int i = 0; i < actuals.Length; i++) {
                if (i > 0) {
                    Type current = actuals[i].GetType();
                    Type previous = actuals[i - 1].GetType();

                    Assert.AreNotEqual(previous, current, $"at index { i }.");
                }
            }
        }

        [TestMethod()]
        public void Pattern__Two_Different_Factories_And_Product_Sets__Assert_Different_Outputs()  /* working */  {
            //**  groundwork  **//
            /* getting two equivalent concrete factories as their abstract */
            AGeometryFactory one = FactoryChooser.Factory();
            AGeometryFactory two = FactoryChooser.Factory();


            //**  exercising the code  **//
            /* getting pairs of equivalent concrete products as their abstracts */
            ALine lineOne = one.BuildLine();
            ALine lineTwo = two.BuildLine();

            APlane planeOne = one.BuildPlane();
            APlane planeTwo = two.BuildPlane();

            APoint pointOne = one.BuildPoint();
            APoint pointTwo = two.BuildPoint();

            // defining point contents 
            pointOne.X = pointTwo.X = 9;
            pointOne.Y = pointTwo.Y = 10;
            pointOne.Z = pointTwo.Z = 11;
            pointOne.HemiEllipse = pointTwo.HemiEllipse = 2;  // only used by some points 

            AShape shapeOne = one.BuildShape();
            AShape shapeTwo = two.BuildShape();

            AVolume volumeOne = one.BuildVolume();
            AVolume volumeTwo = two.BuildVolume();


            //**  testing  **//
            /* each pair should return different values for their method / property */
            Assert.AreNotEqual(lineOne.Description("5,5,7", "8,8,3"), lineTwo.Description("5,5,7", "8,8,3"));
            Assert.AreNotEqual(planeOne.Dimensions, planeTwo.Dimensions);
            Assert.AreNotEqual(pointOne.ToString(), pointTwo.ToString());  // .ToString() overridden 
            Assert.AreNotEqual(shapeOne.ShapeCurvature(Extent.Medium), shapeTwo.ShapeCurvature(Extent.Medium));
            Assert.AreNotEqual(volumeOne.Nature(7), volumeTwo.Nature(7));
        }

        [TestMethod()]
        public void Pattern__Repeated_Tests__Assert_Always_Passing() {
            /* repeating the test a lot, to ensure differentiation behind abstraction */

            //**  no groundwork  **//

            //**  no exercising code  **//


            //**  testing  **//
            for (int i = 0; i < 10; i++) {
                this.Pattern__Two_Different_Factories_And_Product_Sets__Assert_Different_Outputs();
            }
        }

    }
}
