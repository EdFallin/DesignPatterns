﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Flyweight;

namespace DesignPatternsTests.Flyweight
{
    [TestClass]
    public class FlyweightPatternTests
    {
        [TestMethod()]
        public void Pattern__Knowns__Assert_Correct_Output()  /* working */  {
            //**  groundwork  **//
            HtmlQueueStack topic = new HtmlQueueStack();

            #region Queue + stack of content / state objects, plus tag / state Flyweights

            topic.Add(new HtmlStartContext("id=\"Headline\" class=\"megabig\""));
            topic.Add(DivOpenTag.Instance());  // a Flyweight 

            topic.Add(new HtmlContent("Welcome to my website!"));
            topic.Add(new HtmlContent("<br />"));
            topic.Add(new HtmlContent("You'll just love how "));

            topic.Add(new HtmlStartContext("cool"));
            topic.Add(SpanOpenTag.Instance());  // Flyweight 
            topic.Add(new HtmlContent("tubular"));
            topic.Add(SpanCloseTag.Instance());  // Flyweight 
            topic.Add(HtmlEndContext.Instance());  // also a Flyweight 

            topic.Add(new HtmlContent(" this tubular website is!"));
            topic.Add(DivCloseTag.Instance());  // Flyweight 

            topic.Add(new HtmlStartContext("id=\"Details\""));
            topic.Add(DivOpenTag.Instance());  // Flyweight 

            topic.Add(new HtmlStartContext("clean"));
            topic.Add(SpanOpenTag.Instance());  // Flyweight 
            topic.Add(new HtmlContent("Keeping things clean because that's my scene!"));
            topic.Add(HtmlEndContext.Instance());  // also Flyweight 
            topic.Add(HtmlEndContext.Instance());
            topic.Add(SpanCloseTag.Instance());  // Flyweight 
            topic.Add(DivCloseTag.Instance());  // Flyweight 

            #endregion Queue + stack of content / state objects, plus tag / state Flyweights

            string expected
                = "<div id=\"Headline\" class=\"megabig\">Welcome to my website!<br />"
                + "You'll just love how <span class=\"cool\">tubular</span> this tubular website is!</div>"
                + "<div id=\"Details\"><span class=\"clean\">Keeping things clean because that's my scene!</span></div>";


            //**  exercising the code  **//
            string actual = null;

            foreach (string element in topic) {
                /* contents of ~topic are not strings, but are output as strings */
                actual += element;
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

    }
}
