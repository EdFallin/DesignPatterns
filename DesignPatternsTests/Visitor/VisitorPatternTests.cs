﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Visitor;

namespace DesignPatternsTests.Visitor
{
    [TestClass]
    public class VisitorPatternTests
    {
        [TestMethod()]
        public void Pattern_Singularizing__Knowns_Male__Assert_Correct_Sentence()  /* working */  {
            //**  groundwork  **//
            PronounDefinition pronoun = new PronounDefinition("third m");
            Verb verb = new Verb("drive");
            ArticleDefinition article = new ArticleDefinition("indef");
            Adjective adjective = new Adjective("blue");
            Noun noun = new Noun("car");
            Adverb adverb = new Adverb("every day");

            Enactor enactor = new Enactor(pronoun, verb, article, adjective, noun, adverb);

            string expected = "He drives a blue car every day.";


            //**  exercising the code  **//
            string actual = enactor.Enact(new SingularizeVisitor());


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Pattern_Singularizing__Knowns_Female__Assert_Correct_Sentence()  /* working */  {
            //**  groundwork  **//
            PronounDefinition pronoun = new PronounDefinition("third f");
            Verb verb = new Verb("drive");
            ArticleDefinition article = new ArticleDefinition("indef");
            Adjective adjective = new Adjective("blue");
            Noun noun = new Noun("car");
            Adverb adverb = new Adverb("every day");

            Enactor enactor = new Enactor(pronoun, verb, article, adjective, noun, adverb);

            string expected = "She drives a blue car every day.";


            //**  exercising the code  **//
            string actual = enactor.Enact(new SingularizeVisitor());


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Pattern_Pluralizing__Knowns__Assert_Correct_Sentence()  /* working */  {
            //**  groundwork  **//
            PronounDefinition pronoun = new PronounDefinition("third m");
            Verb verb = new Verb("drive");
            ArticleDefinition article = new ArticleDefinition("indef");
            Adjective adjective = new Adjective("blue");
            Noun noun = new Noun("car");
            Adverb adverb = new Adverb("every day");

            Enactor enactor = new Enactor(pronoun, verb, article, adjective, noun, adverb);

            string expected = "They drive some blue cars every day.";  // not quite good English 


            //**  exercising the code  **//
            string actual = enactor.Enact(new PluralizeVisitor());


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

    }
}
