﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.TemplateMethod;

namespace DesignPatternsTests.TemplateMethod
{
    [TestClass]
    public class TemplateMethodPatternTests
    {
        [TestMethod()]
        public void Pattern__Knowns__Assert_Expecteds()  /* working */  {
            /* a very simple pseudo-Html document using my Template Method -pattern objects / methods */

            //**  groundwork  **//
            AHtmlElement suggestion = new SpanHtmlElement("suggestion", "Enter your name...", new string[] { "font:extra-big", "border:wide", "align:center" });
            AHtmlElement input = new InputTextHtmlElement("namebox", "username", "inputs");
            AHtmlElement more = new LinkTextHtmlElement("infolink", "For more information, click here.", @"\\internet\some\resource.html");
            AHtmlElement thanks = new SpanHtmlElement("thanks", "Thanks for playing!", new string[] { "font:italic" });

            string actual = null;

            string expected
                = @"<span id=""suggestion"" style=""font:extra-big; border:wide; align:center"">Enter your name...</span>"
                + @"<input id=""namebox"" type=""text"" name=""username"" class=""inputs"" />"
                + @"<a id=""infolink"" href=""\\internet\some\resource.html"">For more information, click here.</a>"
                + @"<span id=""thanks"" style=""font:italic"">Thanks for playing!</span>";


            //**  exercising the code  **//
            // a series of calls to the template method itself, which here is MakeElement() 
            actual += suggestion.MakeElement();
            actual += input.MakeElement();
            actual += more.MakeElement();
            actual += thanks.MakeElement();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

    }
}
