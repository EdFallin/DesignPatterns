﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DesignPatterns.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.State.Tests
{
    [TestClass()]
    public class StatePatternTests
    {

        #region State pattern : successful

        [TestMethod()]
        public void State_Pattern__Assert_Expected_Texts_And_Progression()  /* working */  {
            /*  each AState reports its state via ~topic's state storage 
             *  at topic.GetStringForSystemState(), then moves to 
             *  the next state at topic.StepForward()  */

            //**  groundwork **//
            AState state = new StartState();
            SystemCapsule topic = new SystemCapsule(state);

            string[] expectedTexts = {
                "starting state",
                "early-middle state",
                "late-middle state",
                "ending state" };

            string[] actualTexts = new string[4];


            //**  exercising the code **//
            /*  4 states, 3 steps from first to last  */
            for (int i = 0; i < 4; i++) {
                actualTexts[i] = topic.GetStringForSystemState();
                topic.StepForward();
            }


            //**  testing **//
            CollectionAssert.AreEqual(expectedTexts, actualTexts);
        }

        #endregion State pattern : successful


    }
}