﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DesignPatterns.Interpreter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternTests.Interpreter
{
    [TestClass()]
    public class AExpressionSubclassTests
    {
        #region Friendlies

        /// <summary>
        /// A class spoofing a child or subtree for recursive calls by Interpret(), 
        /// allowing you to set whatever values Interpret() uses in your test's case.
        /// </summary>
        private class SourceExpression_ : AExpression
        {
            public decimal[] Numbers { get; set; } = new decimal[0];

            public override List<AExpression> Children { get; } = new List<AExpression>();

            public override void Interpret(Context context)  /* verified */  {
                context.Inputs.Clear();
                context.Inputs.AddRange(Numbers);
            }
        }

        #endregion Friendlies


        #region Tests

        #region AndExpression

        [TestMethod()]
        public void AndExpression_Interpret__Known_Inputs__Assert_Correct_Context()  /* working */  {
            /* an AndExpression gathers multiple versions of 
             * Context.Inputs and merges them into one */

            //**  groundwork  **//
            AndExpression topic = new AndExpression();

            // child objects whose Context.Inputs results are merged 
            SourceExpression_ lNumbers = new SourceExpression_();
            SourceExpression_ rNumbers = new SourceExpression_();
            topic.Children.Add(lNumbers);
            topic.Children.Add(rNumbers);

            // sources for the inputs to be merged 
            lNumbers.Numbers = new decimal[] { 2M, 4M, 14M, 7M };
            rNumbers.Numbers = new decimal[] { 7M, 6M, 3M, 12M, 10M, 18M, 4M };

            Context context = new Context();

            // should be replaced by Interpret() 
            decimal[] existing = { 6M, 8M, 12M, 16M };
            context.Inputs.AddRange(existing);

            // array to ensure there's only one value 
            decimal[] expecteds = { 2M, 4M, 14M, 7M, 7M, 6M, 3M, 12M, 10M, 18M, 4M };


            //**  exercising the code  **//
            topic.Interpret(context);


            //**  gathering results  **//
            decimal[] actuals = context.Inputs.ToArray();


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        #endregion AndExpression


        #region AverageExpression

        [TestMethod()]
        public void AverageExpression_Interpret__Known_Inputs__Assert_Correct_Context()  /* working */  {
            //**  groundwork  **//
            AverageExpression topic = new AverageExpression();
            SourceExpression_ numbers = new SourceExpression_();
            topic.Children.Add(numbers);

            numbers.Numbers = new decimal[] { 4M, 2M, 4M, 6M, 5M, 14M, 7M, 12M };

            Context context = new Context();

            // should be replaced by Interpret() 
            decimal[] existing = { 6, 8, 12, 16 };
            context.Inputs.AddRange(existing);

            // as an array to ensure there's only one value 
            decimal expected = numbers.Numbers.Average();
            decimal[] expecteds = { expected };


            //**  exercising the code  **//
            topic.Interpret(context);


            //**  gathering results  **//
            decimal[] actuals = context.Inputs.ToArray();


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        #endregion AverageExpression


        #region CountExpression

        [TestMethod()]
        public void CountExpression_Interpret__Known_Inputs__Assert_Correct_Context()  /* working */  {
            //**  groundwork  **//
            CountExpression topic = new CountExpression();
            SourceExpression_ numbers = new SourceExpression_();
            topic.Children.Add(numbers);

            numbers.Numbers = new decimal[] { 2M, 4M, 14M, 7M, 3M, 11M };

            Context context = new Context();

            // should be replaced by Interpret() 
            decimal[] existing = { 6M, 8M, 12M, 16M };
            context.Inputs.AddRange(existing);

            // as an array to ensure there's only one value 
            int expected = numbers.Numbers.Length;
            decimal[] expecteds = { expected };


            //**  exercising the code  **//
            topic.Interpret(context);


            //**  gathering results  **//
            decimal[] actuals = context.Inputs.ToArray();


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        #endregion CountExpression


        #region DivideExpression

        [TestMethod()]
        public void Dividends_Get__Known_Children_And_Split__Assert_Correct_Children()  /* working */  {
            //**  groundwork  **//
            DivideExpression divider = new DivideExpression();
            int split = 7;
            int top = split + 1;

            List<AExpression> expecteds = new List<AExpression>();

            // adding Dividends and Divisor, and building expecteds 
            for (int of = 0; of < top; of++) {
                AExpression child = new SourceExpression_();
                divider.Children.Add(child);

                // last element (at split) should not be a .Dividend 
                if (of < split) {
                    expecteds.Add(child);
                }
            }

            divider.Split = split;


            //**  exercising the code  **//
            List<AExpression> actuals = divider.Dividends;


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void Divisor_Get__Known_Children_And_Split__Assert_Correct_Child()  /* working */  {
            //**  groundwork  **//
            DivideExpression divider = new DivideExpression();
            int split = 7;
            int top = split + 1;

            AExpression expected = null;

            // adding Dividends and Divisor, and building expecteds 
            for (int of = 0; of < top; of++) {
                AExpression child = new SourceExpression_();
                divider.Children.Add(child);

                // last element (at split) should be .Divisor 
                if (of == split) {
                    expected = child;
                }
            }

            divider.Split = split;


            //**  exercising the code  **//
            AExpression actual = divider.Divisor;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void DivideExpression_Interpret__Known_Inputs__Assert_Correct_Context()  /* passed */  {
            //**  groundwork  **//
            DivideExpression topic = new DivideExpression();
            topic.Split = 1;

            // sources of numbers to divide 
            SourceExpression_ dividend = new SourceExpression_();
            SourceExpression_ divisor = new SourceExpression_();
            topic.Children.Add(dividend);
            topic.Children.Add(divisor);

            dividend.Numbers = new decimal[] { 3M, 4M, 14M, 12M };
            divisor.Numbers = new decimal[] { 6M, 10M };

            Context context = new Context();

            // should be replaced by Interpret() 
            decimal[] existing = { 6M, 8M, 12M, 16M };
            context.Inputs.AddRange(existing);

            // as an array to ensure there's only one value 
            decimal expected = (3M + 4M + 14M + 12M) / (6M + 10M);
            decimal[] expecteds = { expected };


            //**  exercising the code  **//
            topic.Interpret(context);


            //**  gathering results  **//
            decimal[] actuals = context.Inputs.ToArray();


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        #endregion DivideExpression


        #region MaxExpression

        [TestMethod()]
        public void MaxExpression_Interpret__Known_Inputs__Assert_Correct_Context()  /* working */  {
            //**  groundwork  **//
            MaxExpression topic = new MaxExpression();
            SourceExpression_ numbers = new SourceExpression_();
            topic.Children.Add(numbers);

            numbers.Numbers = new decimal[] { 2M, 4M, 14M, 7M };

            Context context = new Context();

            // should be replaced by Interpret() 
            decimal[] existing = { 6M, 8M, 12M, 16M };
            context.Inputs.AddRange(existing);

            // array to ensure there's only one value 
            decimal[] expecteds = { 14M };


            //**  exercising the code  **//
            topic.Interpret(context);


            //**  gathering results  **//
            decimal[] actuals = context.Inputs.ToArray();


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        #endregion MaxExpression


        #region MinExpression

        [TestMethod()]
        public void MinExpression_Interpret__Known_Inputs__Assert_Correct_Context()  /* working */  {
            //**  groundwork  **//
            MinExpression topic = new MinExpression();
            SourceExpression_ numbers = new SourceExpression_();
            topic.Children.Add(numbers);

            numbers.Numbers = new decimal[] { 3M, 2M, 14M, 7M };

            Context context = new Context();

            // should be replaced by Interpret() 
            decimal[] existing = { 6M, 8M, 12M, 16M };
            context.Inputs.AddRange(existing);

            // array to ensure there's only one value 
            decimal[] expecteds = { 2M };


            //**  exercising the code  **//
            topic.Interpret(context);


            //**  gathering results  **//
            decimal[] actuals = context.Inputs.ToArray();


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        #endregion MinExpression


        #region NumbersExpression

        [TestMethod()]
        public void NumbersExpression_Interpret__Known_Numbers_Text__Assert_Correct_Context()  /* working */  {
            //**  groundwork  **//
            NumbersExpression topic = new NumbersExpression();
            string text = "[2, 8, 11]";
            topic.Numbers = text;

            Context context = new Context();

            // these should be replaced by Interpret() 
            decimal[] existing = { 3M, 6M, 7M, 12M };
            context.Inputs.AddRange(existing);

            decimal[] expecteds = { 2M, 8M, 11M };


            //**  exercising the code  **//
            topic.Interpret(context);


            //**  gathering results  **//
            decimal[] actuals = context.Inputs.ToArray();


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        #endregion NumbersExpression


        #region OutputExpression

        [TestMethod()]
        public void OutputExpression_Interpret__Known_Inputs__Assert_Correct_Context()  /* working */  {
            //**  groundwork  **//
            OutputExpression topic = new OutputExpression();
            SourceExpression_ source = new SourceExpression_();
            topic.Children.Add(source);

            Context context = new Context();

            decimal[] numbers = { 228M };
            source.Numbers = numbers;

            decimal expected = numbers[0];


            //**  exercising the code  **//
            topic.Interpret(context);


            //**  gathering results  **//
            // after r-calls to the object's child / subtree, 
            // this Interpret() uniquely sets Context.Output 
            decimal actual = context.Output;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion OutputExpression


        #region StoreExpression

        [TestMethod()]
        public void StoreExpression_Interpret__Known_Inputs__Assert_Correct_Context()  /* working */  {
            /* a StoreExpression just calls Interpret() on its sole child, 
             * so its results are the same as its child's results */

            //**  groundwork  **//
            StoreExpression topic = new StoreExpression();
            SourceExpression_ numbers = new SourceExpression_();
            topic.Children.Add(numbers);

            numbers.Numbers = new decimal[] { 2M, 8M, 11M };

            Context context = new Context();

            // these should be replaced by Interpret() 
            decimal[] existing = { 3M, 6M, 7M, 12M };
            context.Inputs.AddRange(existing);

            decimal[] expecteds = numbers.Numbers.ToArray();


            //**  exercising the code  **//
            topic.Interpret(context);


            //**  gathering results  **//
            decimal[] actuals = context.Inputs.ToArray();


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        #endregion StoreExpression


        #region SumExpression

        [TestMethod()]
        public void SumExpression_Interpret__Known_Inputs__Assert_Correct_Context()  /* working */  {
            //**  groundwork  **//
            SumExpression topic = new SumExpression();
            SourceExpression_ numbers = new SourceExpression_();
            topic.Children.Add(numbers);

            numbers.Numbers = new decimal[] { 9M, 17M, 6M, 3M, 8M, 15M, 6M, 11M, 12M };

            Context context = new Context();

            // should be replaced by Interpret() 
            decimal[] existing = { 6, 8, 12, 16 };
            context.Inputs.AddRange(existing);

            // as an array to ensure there's only one value 
            decimal expected = numbers.Numbers.Sum();
            decimal[] expecteds = { expected };


            //**  exercising the code  **//
            topic.Interpret(context);


            //**  gathering results  **//
            decimal[] actuals = context.Inputs.ToArray();


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        #endregion SumExpression

        #endregion Tests

    }
}