﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DesignPatterns.Interpreter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Interpreter.Tests
{
    [TestClass()]
    public class SyntaxErrorExceptionTests
    {
        [TestMethod()]
        public void Constructor_Many_Args__Known_Args__Assert_Correct_Message()  /* working */  {
            //**  groundwork  **//
            int total = 6;

            Queue<string> tokens = new Queue<string>();

            for (int at = 0; at < 3; at++) {
                tokens.Enqueue($"{ at }");
            }

            string argExpected = "up";
            string argActual = "down";

            string expected
                = $"Syntax error at token 3:  Expected \"up\", encountered \"down\".";


            //**  exercising the code  **//
            ParseException acThrown = new ParseException(tokens, total, argActual, argExpected);


            //**  gathering results  **//
            string actual = acThrown.Message;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Constructor_Many_Args__Many_Expecteds__Assert_Correct_Message()  /* working */  {
            //**  groundwork  **//
            int total = 6;

            Queue<string> tokens = new Queue<string>();

            for (int at = 0; at < 3; at++) {
                tokens.Enqueue($"{ at }");
            }

            string argExpUp = "up";
            string argExpOut = "out";
            string argExpAround = "around";

            string argActual = "down";

            string expected
                = $"Syntax error at token 3:  Expected \"up\" or \"out\" or \"around\", encountered \"down\".";


            //**  exercising the code  **//
            ParseException acThrown = new ParseException(tokens, total, argActual, argExpUp, argExpOut, argExpAround);


            //**  gathering results  **//
            string actual = acThrown.Message;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

    }
}