﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DesignPatterns.Interpreter;

namespace DesignPatternTests.Interpreter
{
    [TestClass()]
    public class ParserTests
    {
        #region Definitions

        private string PARSER_FROM = @"Parse(\w+)";
        private string PARSER_TO = @"$1Parser";

        #endregion Definitions


        #region Friendlies and their tests

        #region Friendly types

        /// <summary>
        /// This fixture class simplifies comparison of parse-tree contents en masse.
        /// </summary>
        private class NodeFacts
        {
            public static int NONE = 0;

            public Type NodeType { get; set; }
            public int Branches { get; set; } = NONE;
            public int Split { get; set; } = NONE;
            public string Text { get; set; }

            public override bool Equals(object obj)  /* verified */  {
                if (obj is NodeFacts other) {
                    bool areEqual
                        = this.NodeType == other.NodeType
                        && this.Branches == other.Branches
                        && this.Split == other.Split
                        && this.Text == other.Text;

                    return areEqual;
                }

                return false;
            }

            public override int GetHashCode()  /* unused */  {
                int code =
                    NodeType.GetHashCode()
                    * Branches
                    * Split
                    * Text.GetHashCode();

                return code;
            }
        }

        #endregion Friendly types


        #region Friendly methods

        /// <summary>
        /// Converts a parsing method's name to its corresponding ParseMethod 
        /// property name if it uses Parser's internal naming convention.
        /// </summary>
        private string PropertyFromMethod(string method)  /* verified */  {
            string result = Regex.Replace(method, PARSER_FROM, PARSER_TO);
            return result;
        }

        /// <summary>
        /// Dequeues repeatedly. 
        /// <para/> 
        /// Use this to simplify spoofs of parse methods.
        /// </summary>
        private void Dequeue(Queue<string> queue, int count)  /* verified */  {
            for (int at = 0; at < count; at++) {
                queue.Dequeue();
            }
        }

        /// <summary>
        /// This method recursively traverses a parse tree to allow 
        /// comparison of its contents against expecteds en masse.
        /// </summary>
        private List<NodeFacts> VisitNodeAndChildren(AExpression node, List<NodeFacts> outputOrInitialNull)  /* passed */  {
            // simplifying initial call by consumer 
            List<NodeFacts> output = outputOrInitialNull ?? new List<NodeFacts>();


            /* depth-first, preorder, pyramidal recursion */

            #region this level: storing facts for output

            NodeFacts facts = new NodeFacts {
                NodeType = node.GetType(),
                Branches = node.Children.Count,
                Split = node.Split
            };

            if (node is NumbersExpression numbers) {
                facts.Text = numbers.Numbers;
            }

            output.Add(facts);

            #endregion this level: storing facts for output

            #region next level: the actual recursion

            foreach (AExpression child in node.Children) {
                VisitNodeAndChildren(child, output);
            }

            #endregion next level: the actual recursion


            // final output: used only by consumer 
            return output;
        }

        /// <summary>
        /// This friendly simplifies some tree-assembly syntax.
        /// </summary>
        private AExpression ReturnWithNullChildren(AExpression node, int nullChildrenToAdd)  /* verified */  {
            for (int of = 0; of < nullChildrenToAdd; of++) {
                node.Children.Add(null);
            }

            return node;
        }

        #endregion Friendly methods


        #region Tests of friendlies

        [TestMethod()]
        public void VisitNodeAndChildren__Known_Tree__Assert_Correct_List_Data()  /* working */  {
            //**  groundwork  **//
            // root and unbranched part of tree 
            AExpression root = ReturnWithNullChildren(new OutputExpression(), 1);
            AExpression sum = root.Children[0] = ReturnWithNullChildren(new SumExpression(), 1);
            AExpression and = sum.Children[0] = ReturnWithNullChildren(new AndExpression(), 2);
            (and as AndExpression).Split = 1;

            // L side of "and", L and R 
            AExpression lDivide = and.Children[0] = ReturnWithNullChildren(new DivideExpression(), 2);
            AExpression llStore = lDivide.Children[0] = ReturnWithNullChildren(new StoreExpression(), 1);
            AExpression llNumbers = llStore.Children[0] = new NumbersExpression();

            AExpression lrStore = lDivide.Children[1] = ReturnWithNullChildren(new StoreExpression(), 1);
            AExpression lrNumbers = lrStore.Children[0] = new NumbersExpression();

            // characteristic data of L side, L and R 
            string expLlNumbersText = "[2, 6, 7]";
            string expLrNumbersText = "[3, 5, 8]";

            (lDivide as DivideExpression).Split = 1;
            (llNumbers as NumbersExpression).Numbers = expLlNumbersText;
            (lrNumbers as NumbersExpression).Numbers = expLrNumbersText;

            // R side of "and" 
            AExpression rStore = and.Children[1] = ReturnWithNullChildren(new StoreExpression(), 1);
            AExpression rNumbers = rStore.Children[0] = new NumbersExpression();

            // characteristic data of R side 
            string expRNumbersText = "[10, 12, 14]";
            (rNumbers as NumbersExpression).Numbers = expRNumbersText;

            // expecteds: the result of preorder depth-first pyramidal recursion 
            List<NodeFacts> expecteds = new List<NodeFacts> {
                new NodeFacts { NodeType = typeof(OutputExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(SumExpression), Branches = 1 },

                new NodeFacts { NodeType = typeof(AndExpression), Branches = 2, Split = 1 },
                new NodeFacts { NodeType = typeof(DivideExpression), Branches = 2, Split = 1 },
                new NodeFacts { NodeType = typeof(StoreExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(NumbersExpression), Branches = 0, Text = expLlNumbersText },
                new NodeFacts { NodeType = typeof(StoreExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(NumbersExpression), Branches = 0, Text = expLrNumbersText },

                new NodeFacts { NodeType = typeof(StoreExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(NumbersExpression), Branches = 0, Text = expRNumbersText }
            };

            //List<NodeFacts> actuals = new List<NodeFacts>();


            //**  exercising the code  **//
            List<NodeFacts> actuals = VisitNodeAndChildren(root, null);


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        #endregion Tests of friendlies

        #endregion Friendlies and their tests


        #region Parse()

        [TestMethod()]
        public void Parse__Known_Simple_Sentence_Sum__Assert_Correct_Parse_Tree()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            string[] tokens = { "store", "[2, 5, 6, 3]", "then", "sum", "then", "output" };

            // nodes in the tree 
            OutputExpression expRoot = new OutputExpression();
            SumExpression expSum = new SumExpression();
            StoreExpression expStore = new StoreExpression();
            NumbersExpression expNumbers = new NumbersExpression();
            expNumbers.Numbers = tokens[1];

            // the tree, which is actually just one path 
            expRoot.Children.Add(expSum);
            expSum.Children.Add(expStore);
            expStore.Children.Add(expNumbers);


            //**  exercising the code  **//
            AExpression acRoot = parser.Parse(tokens);


            //**  testing  **//
            // root, the output 
            Assert.AreEqual(expRoot.GetType(), acRoot?.GetType());

            // child of root, the sum 
            AExpression acChildSum = acRoot.Children[0];
            Assert.AreEqual(expSum.GetType(), acChildSum.GetType());

            // grandchild, the storer 
            AExpression acGrandchildStore = acChildSum.Children[0];
            Assert.AreEqual(expStore.GetType(), acGrandchildStore.GetType());

            // great-grandchild, the numbers 
            AExpression acGreatGrandchildNumbers = acGrandchildStore.Children[0];
            Assert.AreEqual(expNumbers.GetType(), acGreatGrandchildNumbers.GetType());
            Assert.AreEqual(expNumbers.Numbers, ((NumbersExpression)acGreatGrandchildNumbers).Numbers);
        }

        [TestMethod()]
        public void Parse__Known_Simple_Sentence_Count__Assert_Correct_Parse_Tree()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            string[] tokens = { "store", "[25, 54, 63, 37]", "then", "count", "then", "output" };

            // nodes in the tree 
            OutputExpression expRoot = new OutputExpression();
            CountExpression expCount = new CountExpression();
            StoreExpression expStore = new StoreExpression();
            NumbersExpression expNumbers = new NumbersExpression();
            expNumbers.Numbers = tokens[1];

            // the tree, which is actually just one path 
            expRoot.Children.Add(expCount);
            expCount.Children.Add(expStore);
            expStore.Children.Add(expNumbers);


            //**  exercising the code  **//
            AExpression acRoot = parser.Parse(tokens);


            //**  testing  **//
            // root, the output 
            Assert.AreEqual(expRoot.GetType(), acRoot?.GetType());

            // child of root, the sum 
            AExpression acChildSum = acRoot.Children[0];
            Assert.AreEqual(expCount.GetType(), acChildSum.GetType());

            // grandchild, the storer 
            AExpression acGrandchildStore = acChildSum.Children[0];
            Assert.AreEqual(expStore.GetType(), acGrandchildStore.GetType());

            // great-grandchild, the numbers 
            AExpression acGreatGrandchildNumbers = acGrandchildStore.Children[0];
            Assert.AreEqual(expNumbers.GetType(), acGreatGrandchildNumbers.GetType());
            Assert.AreEqual(expNumbers.Numbers, ((NumbersExpression)acGreatGrandchildNumbers).Numbers);
        }

        [TestMethod()]
        public void Parse__Known_Simple_Sentence_Max__Assert_Correct_Parse_Tree()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            string[] tokens = { "store", "[25, 54, 63, 37]", "then", "max", "then", "output" };

            // nodes in the tree 
            OutputExpression expRoot = new OutputExpression();
            MaxExpression expCount = new MaxExpression();
            StoreExpression expStore = new StoreExpression();
            NumbersExpression expNumbers = new NumbersExpression();
            expNumbers.Numbers = tokens[1];

            // the tree, which is actually just one path 
            expRoot.Children.Add(expCount);
            expCount.Children.Add(expStore);
            expStore.Children.Add(expNumbers);


            //**  exercising the code  **//
            AExpression acRoot = parser.Parse(tokens);


            //**  testing  **//
            // root, the output 
            Assert.AreEqual(expRoot.GetType(), acRoot?.GetType());

            // child of root, the sum 
            AExpression acChildSum = acRoot.Children[0];
            Assert.AreEqual(expCount.GetType(), acChildSum.GetType());

            // grandchild, the storer 
            AExpression acGrandchildStore = acChildSum.Children[0];
            Assert.AreEqual(expStore.GetType(), acGrandchildStore.GetType());

            // great-grandchild, the numbers 
            AExpression acGreatGrandchildNumbers = acGrandchildStore.Children[0];
            Assert.AreEqual(expNumbers.GetType(), acGreatGrandchildNumbers.GetType());
            Assert.AreEqual(expNumbers.Numbers, ((NumbersExpression)acGreatGrandchildNumbers).Numbers);
        }

        [TestMethod()]
        public void Parse__Known_Simple_Sentence_Min__Assert_Correct_Parse_Tree()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            string[] tokens = { "store", "[25, 54, 63, 37]", "then", "min", "then", "output" };

            // nodes in the tree 
            OutputExpression expRoot = new OutputExpression();
            MinExpression expCount = new MinExpression();
            StoreExpression expStore = new StoreExpression();
            NumbersExpression expNumbers = new NumbersExpression();
            expNumbers.Numbers = tokens[1];

            // the tree, which is actually just one path 
            expRoot.Children.Add(expCount);
            expCount.Children.Add(expStore);
            expStore.Children.Add(expNumbers);


            //**  exercising the code  **//
            AExpression acRoot = parser.Parse(tokens);


            //**  testing  **//
            // root, the output 
            Assert.AreEqual(expRoot.GetType(), acRoot?.GetType());

            // child of root, the sum 
            AExpression acChildSum = acRoot.Children[0];
            Assert.AreEqual(expCount.GetType(), acChildSum.GetType());

            // grandchild, the storer 
            AExpression acGrandchildStore = acChildSum.Children[0];
            Assert.AreEqual(expStore.GetType(), acGrandchildStore.GetType());

            // great-grandchild, the numbers 
            AExpression acGreatGrandchildNumbers = acGrandchildStore.Children[0];
            Assert.AreEqual(expNumbers.GetType(), acGreatGrandchildNumbers.GetType());
            Assert.AreEqual(expNumbers.Numbers, ((NumbersExpression)acGreatGrandchildNumbers).Numbers);
        }

        [TestMethod()]
        public void Parse__Known_Complex_Sentence_And__Assert_Correct_Parse_Tree()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            string[] tokens = {
                "(", "store", "[24, 57]", "and", "store", "[35, 68]", ")",
                "then", "average", "then", "output"
            };

            #region the tree

            OutputExpression expected = new OutputExpression();
            AverageExpression expAverager = new AverageExpression();
            expected.Children.Add(expAverager);

            AndExpression expSubroot = new AndExpression();
            expAverager.Children.Add(expSubroot);

            StoreExpression expLeftStorer = new StoreExpression();
            NumbersExpression expLeftNumbers = new NumbersExpression();
            expLeftNumbers.Numbers = tokens[2];
            expLeftStorer.Children.Add(expLeftNumbers);

            StoreExpression expRightStorer = new StoreExpression();
            NumbersExpression expRightNumbers = new NumbersExpression();
            expRightNumbers.Numbers = tokens[5];
            expRightStorer.Children.Add(expRightNumbers);

            expSubroot.Children.Add(expLeftStorer);
            expSubroot.Children.Add(expRightStorer);

            #endregion the tree


            //**  exercising the code  **//
            AExpression actual = parser.Parse(tokens);


            //**  testing  **//
            #region root and its sole child 

            Assert.AreEqual(expected.GetType(), actual.GetType());

            AExpression acAverager = actual.Children[0];
            Assert.AreEqual(expAverager.GetType(), acAverager.GetType());

            #endregion root and its sole child 

            #region subtree root

            AExpression acSubroot = acAverager.Children[0];
            Assert.AreEqual(expSubroot.GetType(), acSubroot.GetType());

            // subtree branches from here, two child branches 
            Assert.AreEqual(expSubroot.Children.Count, acSubroot.Children.Count);

            #endregion subtree root

            #region left branch of subtree

            AExpression acLeftStorer = acSubroot.Children[0];
            Assert.AreEqual(expLeftStorer.GetType(), acLeftStorer.GetType());

            AExpression acLeftNumbers = acLeftStorer.Children[0];
            Assert.AreEqual(expLeftNumbers.GetType(), acLeftNumbers.GetType());
            Assert.AreEqual(expLeftNumbers.Numbers, ((NumbersExpression)acLeftNumbers).Numbers);

            #endregion left branch of subtree

            #region right branch of subtree

            AExpression acRightStorer = acSubroot.Children[1];
            Assert.AreEqual(expRightStorer.GetType(), acRightStorer.GetType());

            AExpression acRightNumbers = acRightStorer.Children[0];
            Assert.AreEqual(expRightNumbers.GetType(), acRightNumbers.GetType());
            Assert.AreEqual(expRightNumbers.Numbers, ((NumbersExpression)acRightNumbers).Numbers);

            #endregion right branch of subtree
        }

        [TestMethod()]
        public void Parse__Known_Complex_Sentence_Divide__Assert_Correct_Parse_Tree()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            string[] tokens = {
                "(", "store", "[24, 57]", "divide", "store", "[35]", ")",
                "then", "sum", "then", "output"
            };

            // the results of a post-order depth-first pyramidal recursion 
            // of a correct parse tree of the input sentence's tokens 
            List<NodeFacts> expecteds = new List<NodeFacts> {
                // root and top of tree 
                new NodeFacts { NodeType = typeof(OutputExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(SumExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(DivideExpression), Branches = 2, Split = 1 },

                // left branch of subtree 
                new NodeFacts { NodeType = typeof(StoreExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(NumbersExpression), Text = tokens[2] },

                // right branch of subtree 
                new NodeFacts { NodeType = typeof(StoreExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(NumbersExpression), Text = tokens[5] }
                };


            //**  exercising the code  **//
            AExpression actual = parser.Parse(tokens);


            //**  gathering results  **//
            List<NodeFacts> actuals = VisitNodeAndChildren(actual, null);


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void Parse__Known_Polycomplex_Sentence__Assert_Correct_Parse_Tree()  /* working */  {
            /* the sentence here is polycomplex because it parses to have nested subtrees */

            #region parse tree diagram

            /*                                      
                       expected tree:               
                                                    
                                        output      
                                          |         
                                         sum        
                                          |         
                                         and        
                                          |         
                         ------------------------------------        
                         |                                  |        
                        and                               divide     
                         |                                  |        
                 -----------------                  -----------------        
                 |               |                  |               |        
                 store         store                store         store      
                 |               |                  |               |        
               [24, 57]      [16, 28]             [10, 12]         [8]       
                                                                             
            */

            #endregion parse tree diagram

            //**  groundwork  **//
            Parser parser = new Parser();
            string[] tokens = {
                "(", "(", "store", "[24, 57]", "and", "store", "[16, 28]", ")",
                "and",
                "(", "store", "[10, 12]", "divide", "store", "[8]", ")", ")",
                "then", "sum", "then", "output"
            };

            // the node facts from a preorder recursion (parent, then children) 
            List<NodeFacts> expecteds = new List<NodeFacts> {
                // root and top of tree 
                new NodeFacts { NodeType = typeof(OutputExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(SumExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(AndExpression), Branches = 2, Split = 1 },

                // left subtree 
                new NodeFacts { NodeType = typeof(AndExpression), Branches = 2, Split = 1 },

                // left subtree, left branch 
                new NodeFacts { NodeType = typeof(StoreExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(NumbersExpression), Text = "[24, 57]" },

                // left subtree, right branch 
                new NodeFacts { NodeType = typeof(StoreExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(NumbersExpression), Text = "[16, 28]" },

                // right subtree 
                new NodeFacts { NodeType = typeof(DivideExpression), Branches = 2, Split = 1 },
                
                // right subtree, left branch 
                new NodeFacts { NodeType = typeof(StoreExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(NumbersExpression), Text = "[10, 12]" },
                
                // right subtree, right branch 
                new NodeFacts { NodeType = typeof(StoreExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(NumbersExpression), Text = "[8]" },
            };

            List<NodeFacts> actuals = new List<NodeFacts>();


            //**  exercising the code  **//
            AExpression actual = parser.Parse(tokens);


            //**  gathering results  **//
            VisitNodeAndChildren(actual, actuals);

            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        #endregion Parse()


        #region Cross-recursive dependencies of Parse()

        #region ParseLanguage()

        [TestMethod()]
        public void ParseLanguage__Valid_Input__Assert_No_Throw()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            string[] source = { "store", "[2, 5, 8]", "then", "sum", "then", "output" };
            Queue<string> tokens = new Queue<string>(source);

            // model code 
            string method = Parser.NameOfParseLanguage;
            string name = PropertyFromMethod(Parser.NameOfParseAction);

            // spoof code 
            Parser.ParseMethod called = (Queue<string> arg) => { Dequeue(arg, 4); return new OutputExpression(); };

            // dependencies of model code 
            p.SetProperty(name, called);
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expected = null;
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParseLanguage__Valid_Input__Assert_Correct_Calls()  /* working */  {
            /* ParseLanguage() has one external call, of ActionParser() */

            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            string[] source = { "store", "[2, 5, 8]", "then", "sum", "then", "output" };
            Queue<string> tokens = new Queue<string>(source);

            // model code 
            string method = Parser.NameOfParseLanguage;
            string name = PropertyFromMethod(Parser.NameOfParseAction);

            bool expected = true;
            bool actual = false;

            // spoof code 
            Parser.ParseMethod shouldBeCalled = (Queue<string> args) => {
                Dequeue(args, 4); actual = true; return new OutputExpression();
            };


            // dependencies of model code 
            p.SetProperty(name, shouldBeCalled);
            p.SetField(Parser.NameOfTotal, source.Length);


            //**  exercising the code  **//
            p.Invoke(method, tokens);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParseLanguage__Valid_Input__Assert_Correct_Subtree()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // realistic correct syntax 
            string[] source = { "store", "[2, 5, 8]", "then", "sum", "then", "output" };
            Queue<string> tokens = new Queue<string>(source);

            // model code 
            string method = Parser.NameOfParseLanguage;
            string name = PropertyFromMethod(Parser.NameOfParseAction);

            // spoof code 
            Parser.ParseMethod called = (Queue<string> arg) => {
                Dequeue(arg, 4);
                return new SumExpression();
            };

            // dependencies of model code 
            p.SetProperty(name, called);
            p.SetField(Parser.NameOfTotal, source.Length);

            // expecteds 
            List<NodeFacts> expecteds = new List<NodeFacts> {
                new NodeFacts { NodeType = typeof(OutputExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(SumExpression) }
            };


            //**  exercising the code  **//
            AExpression actual = (AExpression)p.Invoke(method, tokens);


            //**  gathering results  **//
            List<NodeFacts> actuals = VisitNodeAndChildren(actual, null);


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void ParseLanguage__Invalid_Input_No_Then__Assert_Throw()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // in valid syntax, "then" appears between "sum" and "output" 
            string[] source = { "store", "[2, 5, 8]", "then", "sum", "output" };
            Queue<string> tokens = new Queue<string>(source);

            // model code 
            string method = Parser.NameOfParseLanguage;
            string name = PropertyFromMethod(Parser.NameOfParseAction);

            // spoof code 
            Parser.ParseMethod called = (Queue<string> arg) => { Dequeue(arg, 4); return new OutputExpression(); };

            // dependencies of model code 
            p.SetProperty(name, called);
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expected = typeof(ParseException);
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParseLanguage__Invalid_Input_No_Output__Assert_Throw()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // in valid syntax, the last token should be "output" 
            string[] source = { "store", "[2, 5, 8]", "then", "sum", "then", "sum" };
            Queue<string> tokens = new Queue<string>(source);

            // model code 
            string method = Parser.NameOfParseLanguage;
            string name = PropertyFromMethod(Parser.NameOfParseAction);

            // spoof code 
            Parser.ParseMethod called = (Queue<string> arg) => { Dequeue(arg, 4); return new OutputExpression(); };

            // dependencies of model code 
            p.SetProperty(name, called);
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expected = typeof(ParseException);
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParseLanguage__Invalid_Input_Extra_Tokens__Assert_Throw()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // in valid input, "output" should be the final token 
            string[] source = { "store", "[2, 5, 8]", "then", "sum", "then", "output", "then" };
            Queue<string> tokens = new Queue<string>(source);

            // model code 
            string method = Parser.NameOfParseLanguage;
            string name = PropertyFromMethod(Parser.NameOfParseAction);

            // spoof code 
            Parser.ParseMethod called = (Queue<string> arg) => { Dequeue(arg, 4); return new OutputExpression(); };

            // dependencies of model code 
            p.SetProperty(name, called);
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expected = typeof(ParseException);
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion ParseLanguage()


        #region ParseAction()

        [TestMethod()]
        public void ParseAction__Valid_Input_Each_Token__Assert_No_Throw()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // each of these is a valid syntax; in the first three, the token after 
            // the numbers ends the whole action after the pre-action; 
            // in the fourth, the action ends after a pre-action and a post-action 
            string[][] sources = {
                new string[] { "store", "[2, 5, 8]", "and" },
                new string[] { "store", "[2, 5, 8]", "divide" },
                new string[] { "store", "[2, 5, 8]", ")" },
                new string[] { "store", "[2, 5, 8]", "then", "sum" }
            };

            // model code 
            string method = Parser.NameOfParseAction;
            string nameOfFirst = PropertyFromMethod(Parser.NameOfParsePreAction);
            string nameOfSecond = PropertyFromMethod(Parser.NameOfParsePostAction);

            // spoof code 
            Parser.ParseMethod shouldBeFirst = (Queue<string> arg) => { Dequeue(arg, 2); return new StoreExpression(); };
            Parser.ParseMethod shouldBeSecond = (Queue<string> arg) => { arg.Dequeue(); return new SumExpression(); };

            // dependencies of model code 
            p.SetProperty(nameOfFirst, shouldBeFirst);
            p.SetProperty(nameOfSecond, shouldBeSecond);
            p.SetField(Parser.NameOfTotal, sources.Length);

            // expecteds and actuals 
            Type[] expecteds = { null, null, null, null };
            Type[] actuals = new Type[4];


            //**  exercising the code  **//
            for (int at = 0; at < sources.Length; at++) {
                // getting the current tokens from the array of arrays 
                Queue<string> tokens = new Queue<string>(sources[at]);

                // default that is replaced if call throws 
                Type actual = null;

                // actually exercising the code 
                try {
                    p.Invoke(method, tokens);
                }
                catch (Exception x) {
                    actual = x.GetType();
                }

                // all results are traversed at the end 
                actuals[at] = actual;
            }


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void ParseAction__Valid_Input_Then__Assert_Correct_Calls()  /* working */  {
            /* the "then" token after the numbers produces 
             * a post-action with a single pre-action child */

            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // pre-action + "then" + post-action is a valid action 
            string[] source = { "store", "[2, 5, 8]", "then", "sum" };
            Queue<string> tokens = new Queue<string>(source);

            // model code 
            string method = Parser.NameOfParseAction;
            string nameOfFirst = PropertyFromMethod(Parser.NameOfParsePreAction);
            string nameOfSecond = PropertyFromMethod(Parser.NameOfParsePostAction);

            List<string> expecteds = new List<string> { "first", "second" };
            List<string> actuals = new List<string>();

            // spoof code 
            Parser.ParseMethod shouldBeFirst = (Queue<string> arg) => {
                Dequeue(arg, 2); actuals.Add("first"); return new StoreExpression();
            };
            Parser.ParseMethod shouldBeSecond = (Queue<string> arg) => {
                arg.Dequeue(); actuals.Add("second"); return new SumExpression();
            };

            // dependencies of model code 
            p.SetProperty(nameOfFirst, shouldBeFirst);
            p.SetProperty(nameOfSecond, shouldBeSecond);
            p.SetField(Parser.NameOfTotal, source.Length);


            //**  exercising the code  **//
            p.Invoke(method, tokens);


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void ParseAction__Valid_Input_Each_PreAction_Only_Token__Assert_Correct_Subtrees_Returned()  /* working */  {
            /* each of the tokens after the numbers here 
             * ends the whole action after the pre-action */

            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // each of these is a valid syntax ending the whole action after the pre-action, 
            // since a post-action and its intro token "then" don't always follow the pre-action 
            string[][] sources = {
                new string[] { "store", "[2, 5, 8]", "and" },
                new string[] { "store", "[2, 5, 8]", "divide" },
                new string[] { "store", "[2, 5, 8]", ")" },
            };

            // model code 
            string method = Parser.NameOfParseAction;
            string nameOfFirst = PropertyFromMethod(Parser.NameOfParsePreAction);
            string nameOfSecond = PropertyFromMethod(Parser.NameOfParsePostAction);

            // spoof code 
            Parser.ParseMethod shouldBeFirst = (Queue<string> arg) => { Dequeue(arg, 2); return new StoreExpression(); };
            Parser.ParseMethod shouldBeSecond = (Queue<string> arg) => { arg.Dequeue(); return new SumExpression(); };

            // dependencies of model code 
            p.SetProperty(nameOfFirst, shouldBeFirst);
            p.SetProperty(nameOfSecond, shouldBeSecond);
            p.SetField(Parser.NameOfTotal, sources.Length);

            // expecteds and actuals; the SumExpression part of 
            // the action should be skipped for all of these tokens
            Type expected = typeof(StoreExpression);
            Type[] actuals = new Type[sources.Length];


            //**  exercising the code  **//
            for (int at = 0; at < sources.Length; at++) {
                // getting the current tokens from the array of arrays 
                Queue<string> tokens = new Queue<string>(sources[at]);

                // actually exercising the code 
                AExpression actual = (AExpression)p.Invoke(method, tokens);

                // all results are traversed at the end 
                actuals[at] = actual.GetType();
            }


            //**  testing  **//
            for (int at = 0; at < actuals.Length; at++) {
                // same comparison for each token's result 
                Assert.AreEqual(expected, actuals[at]);
            }
        }

        [TestMethod()]
        public void ParseAction__Valid_Input_Then__Assert_Correct_Subtree_Returned()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // realistic syntax; "store" pre-action + "then" + post-action is a valid action 
            string[] source = { "store", "[2, 5, 8]", "then", "sum" };
            Queue<string> tokens = new Queue<string>(source);

            // model code 
            string method = Parser.NameOfParseAction;
            string nameOfFirst = PropertyFromMethod(Parser.NameOfParsePreAction);
            string nameOfSecond = PropertyFromMethod(Parser.NameOfParsePostAction);

            // spoof code; these imitate the results of correct parsing by 
            // cross-recursively called methods, each u-tested separately 
            Parser.ParseMethod shouldBeFirst = (Queue<string> arg) => {
                Dequeue(arg, 2); return new StoreExpression();
            };
            Parser.ParseMethod shouldBeSecond = (Queue<string> arg) => {
                arg.Dequeue(); return new SumExpression();
            };

            // dependencies of model code 
            p.SetProperty(nameOfFirst, shouldBeFirst);
            p.SetProperty(nameOfSecond, shouldBeSecond);
            p.SetField(Parser.NameOfTotal, source.Length);

            List<NodeFacts> expecteds = new List<NodeFacts> {
                new NodeFacts { NodeType= typeof(SumExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(StoreExpression) }
            };


            //**  exercising the code  **//
            AExpression actual = (AExpression)p.Invoke(method, tokens);


            //**  gathering results  **//
            List<NodeFacts> actuals = VisitNodeAndChildren(actual, null);


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void ParseAction__Valid_Input_Open_Parens_Group_Then__Assert_Correct_Subtree()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // realistic syntax; "(" ... "and" ... ")" + "then" + post-action are a valid action 
            string[] source = { "(", "store", "[2, 5, 8]", "and", "store", "[3, 6, 8]", ")", "then", "count" };
            Queue<string> tokens = new Queue<string>(source);

            // model code 
            string method = Parser.NameOfParseAction;
            string nameOfFirst = PropertyFromMethod(Parser.NameOfParsePreAction);
            string nameOfSecond = PropertyFromMethod(Parser.NameOfParsePostAction);

            // spoof code; these imitate the results of correct parsing by 
            // cross-recursively called methods, each u-tested separately 
            Parser.ParseMethod shouldBeFirst = (Queue<string> arg) => {
                Dequeue(arg, 7); return new AndExpression();
            };
            Parser.ParseMethod shouldBeSecond = (Queue<string> arg) => {
                arg.Dequeue(); return new CountExpression();
            };

            // dependencies of model code 
            p.SetProperty(nameOfFirst, shouldBeFirst);
            p.SetProperty(nameOfSecond, shouldBeSecond);
            p.SetField(Parser.NameOfTotal, source.Length);

            List<NodeFacts> expecteds = new List<NodeFacts> {
                new NodeFacts { NodeType= typeof(CountExpression), Branches = 1 },
                new NodeFacts { NodeType = typeof(AndExpression) }
            };


            //**  exercising the code  **//
            AExpression actual = (AExpression)p.Invoke(method, tokens);


            //**  gathering results  **//
            List<NodeFacts> actuals = VisitNodeAndChildren(actual, null);


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void ParseAction__Invalid_Input_Bad_Following_PreAction__Assert_Throw()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // there should be a "then" before the final post-action token 
            string[] source = { "store", "[2, 5, 8]", "sum" };
            Queue<string> tokens = new Queue<string>(source);

            // model code 
            string method = Parser.NameOfParseAction;
            string nameOfFirst = PropertyFromMethod(Parser.NameOfParsePreAction);
            string nameOfSecond = PropertyFromMethod(Parser.NameOfParsePostAction);

            // spoof code 
            Parser.ParseMethod shouldBeFirst = (Queue<string> arg) => { Dequeue(arg, 2); return null; };
            Parser.ParseMethod shouldBeSecond = (Queue<string> arg) => { arg.Dequeue(); return null; };

            // dependencies of model code 
            p.SetProperty(nameOfFirst, shouldBeFirst);
            p.SetProperty(nameOfSecond, shouldBeSecond);
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expected = typeof(ParseException);
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion ParseAction()


        #region ParsePreAction()

        [TestMethod()]
        public void ParsePreAction__Valid_Input_And__Assert_No_Throw()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // this is a group pre-action, made up of two store pre-actions 
            string[] source = { "(", "store", "[2, 5, 8]", "and", "store", "[3, 6, 10]", ")" };
            Queue<string> tokens = new Queue<string>(source);

            // model code 
            string method = Parser.NameOfParsePreAction;
            string nameOfGroup = PropertyFromMethod(Parser.NameOfParseGroup);
            string nameOfStore = PropertyFromMethod(Parser.NameOfParseStore);

            // spoof code 
            Parser.ParseMethod calledIfGroup = (Queue<string> arg) => { Dequeue(arg, 7); return null; };
            Parser.ParseMethod calledIfStore = (Queue<string> arg) => { Dequeue(arg, 2); return null; };

            // dependencies of model code 
            p.SetProperty(nameOfGroup, calledIfGroup);
            p.SetProperty(nameOfStore, calledIfStore);
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expected = null;
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParsePreAction__Valid_Input_Store__Assert_No_Throw()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // this is a store pre-action 
            string[] source = { "store", "[2, 5, 8]" };
            Queue<string> tokens = new Queue<string>(source);

            // model code 
            string method = Parser.NameOfParsePreAction;
            string nameOfGroup = PropertyFromMethod(Parser.NameOfParseGroup);
            string nameOfStore = PropertyFromMethod(Parser.NameOfParseStore);

            // spoof code 
            Parser.ParseMethod calledIfGroup = (Queue<string> arg) => { Dequeue(arg, 7); return null; };
            Parser.ParseMethod calledIfStore = (Queue<string> arg) => { Dequeue(arg, 2); return null; };

            // dependencies of model code 
            p.SetProperty(nameOfGroup, calledIfGroup);
            p.SetProperty(nameOfStore, calledIfStore);
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expected = null;
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParsePreAction__Valid_Input_And__Assert_Correct_Subtree_Call()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // this is a group pre-action, made up of two store pre-actions 
            string[] source = { "(", "store", "[2, 5, 8]", "and", "store", "[3, 6, 10]", ")" };
            Queue<string> tokens = new Queue<string>(source);

            // model code 
            string method = Parser.NameOfParsePreAction;
            string nameOfGroup = PropertyFromMethod(Parser.NameOfParseGroup);
            string nameOfStore = PropertyFromMethod(Parser.NameOfParseStore);

            Type expected = typeof(AndExpression);
            Type actual = null;

            bool expGroupCalled = true;
            bool acGroupCalled = false;

            bool expStoreCalled = false;
            bool acStoreCalled = false;

            // spoof code 
            Parser.ParseMethod calledIfGroup = (Queue<string> arg) => {
                Dequeue(arg, 7); acGroupCalled = true; return new AndExpression();
            };
            Parser.ParseMethod calledIfStore = (Queue<string> arg) => {
                Dequeue(arg, 2); acStoreCalled = true; return new StoreExpression();
            };

            // dependencies of model code 
            p.SetProperty(nameOfGroup, calledIfGroup);
            p.SetProperty(nameOfStore, calledIfStore);
            p.SetField(Parser.NameOfTotal, source.Length);


            //**  exercising the code  **//
            AExpression acPasser = (AExpression)p.Invoke(method, tokens);


            //**  gathering results  **//
            actual = acPasser.GetType();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expGroupCalled, acGroupCalled);
            Assert.AreEqual(expStoreCalled, acStoreCalled);
        }

        [TestMethod()]
        public void ParsePreAction__Valid_Input_Store__Assert_Correct_Subtree_Call()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // this is a store pre-action 
            string[] source = { "store", "[3, 6, 10]" };
            Queue<string> tokens = new Queue<string>(source);

            // model code 
            string method = Parser.NameOfParsePreAction;
            string nameOfGroup = PropertyFromMethod(Parser.NameOfParseGroup);
            string nameOfStore = PropertyFromMethod(Parser.NameOfParseStore);

            Type expected = typeof(StoreExpression);
            Type actual = null;

            bool expGroupCalled = false;
            bool acGroupCalled = false;

            bool expStoreCalled = true;
            bool acStoreCalled = false;

            // spoof code 
            Parser.ParseMethod calledIfGroup = (Queue<string> arg) => {
                Dequeue(arg, 7); acGroupCalled = true; return new AndExpression();
            };
            Parser.ParseMethod calledIfStore = (Queue<string> arg) => {
                Dequeue(arg, 2); acStoreCalled = true; return new StoreExpression();
            };


            // dependencies of model code 
            p.SetProperty(nameOfGroup, calledIfGroup);
            p.SetProperty(nameOfStore, calledIfStore);
            p.SetField(Parser.NameOfTotal, source.Length);


            //**  exercising the code  **//
            AExpression acPasser = (AExpression)p.Invoke(method, tokens);


            //**  gathering results  **//
            actual = acPasser.GetType();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expGroupCalled, acGroupCalled);
            Assert.AreEqual(expStoreCalled, acStoreCalled);
        }

        [TestMethod()]
        public void ParsePreAction__Invalid_Input__Assert_Throw()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // any first token besides "(" or "store" is invalid 
            string[] source = { "sum", "[3, 6, 10]" };
            Queue<string> tokens = new Queue<string>(source);

            // model code 
            string method = Parser.NameOfParsePreAction;
            string nameOfGroup = PropertyFromMethod(Parser.NameOfParseGroup);
            string nameOfStore = PropertyFromMethod(Parser.NameOfParseStore);

            // spoof code 
            Parser.ParseMethod calledIfGroup = (Queue<string> arg) => { Dequeue(arg, 7); return null; };
            Parser.ParseMethod calledIfStore = (Queue<string> arg) => { Dequeue(arg, 2); return null; };

            // dependencies of model code 
            p.SetProperty(nameOfGroup, calledIfGroup);
            p.SetProperty(nameOfStore, calledIfStore);
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expected = typeof(ParseException);
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion ParsePreAction()


        #region ParseGroup()

        [TestMethod()]
        public void ParseGroup__Valid_Input__Assert_No_Throws()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // a group is two actions, with "and" or "divide" between them, between "(" and ")" 
            string[] source = { "(", "store", "[2, 6, 7, 8]", "and", "store", "[1, 6, 8, 9]", ")" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseGroup;

            #region spoofed methods and other Parser members

            string actionName = PropertyFromMethod(Parser.NameOfParseAction);
            string grouperName = PropertyFromMethod(Parser.NameOfParseGrouper);

            // realistic spoofed dependencies 
            Parser.ParseMethod actionParser = (Queue<string> arg) => { Dequeue(arg, 2); return new StoreExpression(); };
            Parser.ParseMethod grouperParser = (Queue<string> arg) => { arg.Dequeue(); return new AndExpression(); };

            p.SetProperty(actionName, actionParser);
            p.SetProperty(grouperName, grouperParser);
            p.SetField(Parser.NameOfTotal, source.Length);

            #endregion spoofed methods and other Parser members

            Type expected = null;
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParseGroup__Valid_Input__Assert_Correct_Calls()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // a group is two actions, with "and" or "divide" between them, between "(" and ")" 
            string[] source = { "(", "store", "[2, 6, 7, 8]", "divide", "store", "[1, 6, 8, 9]", ")" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseGroup;

            #region spoofed methods and other Parser members

            string actionName = PropertyFromMethod(Parser.NameOfParseAction);
            string grouperName = PropertyFromMethod(Parser.NameOfParseGrouper);

            List<string> expecteds = new List<string> { "action 1", "grouper 2", "action 3" };
            List<string> actuals = new List<string>();
            int of = 0;

            // realistic spoofed dependencies 
            Parser.ParseMethod actionParser = (Queue<string> arg) => {
                Dequeue(arg, 2); actuals.Add($"action { ++of }"); return new StoreExpression();
            };
            Parser.ParseMethod grouperParser = (Queue<string> arg) => {
                arg.Dequeue(); actuals.Add($"grouper { ++of }"); return new DivideExpression();
            };

            p.SetProperty(actionName, actionParser);
            p.SetProperty(grouperName, grouperParser);
            p.SetField(Parser.NameOfTotal, source.Length);

            #endregion spoofed methods and other Parser members


            //**  exercising the code  **//
            p.Invoke(method, tokens);


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void ParseGroup__Valid_Input__Assert_Correct_Subtree_Returned()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // a group is two actions, with "and" or "divide" between them, between "(" and ")" 
            string[] source = { "(", "store", "[2, 6, 7, 8]", "and", "store", "[1, 6, 8, 9]", ")" };
            Queue<string> tokens = new Queue<string>(source);

            // spoofed subtree and branches as a preorder traversal, 
            // but with a real group subtree root type (an "and") 
            List<NodeFacts> expecteds = new List<NodeFacts> {
                new NodeFacts { NodeType = typeof(AndExpression), Branches = 2, Split = 1 },
                new NodeFacts { NodeType = typeof(OutputExpression) },
                new NodeFacts { NodeType = typeof(OutputExpression) }
            };

            string method = Parser.NameOfParseGroup;

            #region spoofed methods and other Parser members

            string actionName = PropertyFromMethod(Parser.NameOfParseAction);
            string grouperName = PropertyFromMethod(Parser.NameOfParseGrouper);

            Parser.ParseMethod actionParser = (Queue<string> arg) => {
                Dequeue(arg, 2); return new OutputExpression();
            };
            Parser.ParseMethod grouperParser = (Queue<string> arg) => {
                arg.Dequeue(); return new AndExpression();
            };

            p.SetProperty(actionName, actionParser);
            p.SetProperty(grouperName, grouperParser);
            p.SetField(Parser.NameOfTotal, source.Length);

            #endregion spoofed methods and other Parser members


            //**  exercising the code  **//
            AExpression actual = (AExpression)p.Invoke(method, tokens);


            //**  gathering results  **//
            List<NodeFacts> actuals = VisitNodeAndChildren(actual, null);


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void ParseGroup__Invalid_Input__Assert_Throw()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // the token after the second action in a group must be a ")" 
            string[] source = { "(", "store", "[2, 6, 7, 8]", "and", "store", "[1, 6, 8, 9]", "then", "sum" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseGroup;

            #region spoofed methods and other Parser members

            string actionName = PropertyFromMethod(Parser.NameOfParseAction);
            string grouperName = PropertyFromMethod(Parser.NameOfParseGrouper);

            // realistic spoofed methods 
            Parser.ParseMethod actionParser = (Queue<string> arg) => {
                Dequeue(arg, 2); return new StoreExpression();
            };
            Parser.ParseMethod grouperParser = (Queue<string> arg) => {
                arg.Dequeue(); return new AndExpression();
            };

            p.SetProperty(actionName, actionParser);
            p.SetProperty(grouperName, grouperParser);
            p.SetField(Parser.NameOfTotal, source.Length);

            #endregion spoofed methods and other Parser members

            Type expected = typeof(ParseException);
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion ParseGroup()


        #region ParseGrouper()

        [TestMethod()]
        public void ParseGrouper__Valid_Input_And__Assert_No_Throws()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // a grouper is either "and" or "divide" 
            string[] source = { "and" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseGrouper;
            string andName = PropertyFromMethod(Parser.NameOfParseAnd);
            string divideName = PropertyFromMethod(Parser.NameOfParseDivide);

            Parser.ParseMethod andParser = (Queue<string> arg) => { arg.Dequeue(); return null; };
            Parser.ParseMethod divideParser = (Queue<string> arg) => { arg.Dequeue(); return null; };

            p.SetProperty(andName, andParser);
            p.SetProperty(divideName, divideParser);
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expected = null;
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParseGrouper__Valid_Input_Divide__Assert_No_Throws()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // a grouper is either "and" or "divide" 
            string[] source = { "divide" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseGrouper;
            string andName = PropertyFromMethod(Parser.NameOfParseAnd);
            string divideName = PropertyFromMethod(Parser.NameOfParseDivide);

            Parser.ParseMethod andParser = (Queue<string> arg) => { arg.Dequeue(); return null; };
            Parser.ParseMethod divideParser = (Queue<string> arg) => { arg.Dequeue(); return null; };

            p.SetProperty(andName, andParser);
            p.SetProperty(divideName, divideParser);
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expected = null;
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParseGrouper__Valid_Input_And__Assert_Correct_Call()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // a grouper is either "and" or "divide" 
            string[] source = { "and" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseGrouper;
            string andName = PropertyFromMethod(Parser.NameOfParseAnd);
            string divideName = PropertyFromMethod(Parser.NameOfParseDivide);

            bool expAndWasCalled = true;
            bool acAndWasCalled = false;

            bool expDivideWasCalled = false;
            bool acDivideWasCalled = false;

            Parser.ParseMethod andParser = (Queue<string> arg) => { arg.Dequeue(); acAndWasCalled = true; return null; };
            Parser.ParseMethod divideParser = (Queue<string> arg) => { arg.Dequeue(); acDivideWasCalled = true; return null; };

            p.SetProperty(andName, andParser);
            p.SetProperty(divideName, divideParser);
            p.SetField(Parser.NameOfTotal, source.Length);


            //**  exercising the code  **//
            p.Invoke(method, tokens);


            //**  testing  **//
            Assert.AreEqual(expAndWasCalled, acAndWasCalled);
            Assert.AreEqual(expDivideWasCalled, acDivideWasCalled);
        }

        [TestMethod()]
        public void ParseGrouper__Valid_Input_Divide__Assert_Correct_Call()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // a grouper is either "and" or "divide" 
            string[] source = { "divide" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseGrouper;
            string andName = PropertyFromMethod(Parser.NameOfParseAnd);
            string divideName = PropertyFromMethod(Parser.NameOfParseDivide);

            bool expAndWasCalled = false;
            bool acAndWasCalled = false;

            bool expDivideWasCalled = true;
            bool acDivideWasCalled = false;

            Parser.ParseMethod andParser = (Queue<string> arg) => { arg.Dequeue(); acAndWasCalled = true; return null; };
            Parser.ParseMethod divideParser = (Queue<string> arg) => { arg.Dequeue(); acDivideWasCalled = true; return null; };

            p.SetProperty(andName, andParser);
            p.SetProperty(divideName, divideParser);
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expected = null;
            Type actual = null;


            //**  exercising the code  **//
            p.Invoke(method, tokens);


            //**  testing  **//
            Assert.AreEqual(expAndWasCalled, acAndWasCalled);
            Assert.AreEqual(expDivideWasCalled, acDivideWasCalled);
        }

        [TestMethod()]
        public void ParseGrouper__Valid_Input_And__Assert_AndExpression_Returned()  /* working */  {
            /* ParseGrouper determines which grouper to return, so that is tested here */

            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            string[] source = { "and" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseGrouper;
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expType = typeof(AndExpression);


            //**  exercising the code  **//
            AExpression actual = (AExpression)p.Invoke(method, tokens);


            //**  gathering results  **//
            Type acType = actual.GetType();


            //**  testing  **//
            Assert.AreEqual(expType, acType);
        }

        [TestMethod()]
        public void ParseGrouper__Valid_Input_Divide__Assert_DivideExpression_Returned()  /* working */  {
            /* ParseGrouper determines which grouper to return, so that is tested here */

            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            string[] source = { "divide" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseGrouper;
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expType = typeof(DivideExpression);


            //**  exercising the code  **//
            AExpression actual = (AExpression)p.Invoke(method, tokens);


            //**  gathering results  **//
            Type acType = actual.GetType();


            //**  testing  **//
            Assert.AreEqual(expType, acType);
        }

        [TestMethod()]
        public void ParseGrouper__Invalid_Input__Assert_Throw()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // a grouper is either "and" or "divide" 
            string[] source = { "other" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseGrouper;
            string andName = PropertyFromMethod(Parser.NameOfParseAnd);
            string divideName = PropertyFromMethod(Parser.NameOfParseDivide);

            Parser.ParseMethod andParser = (Queue<string> arg) => { arg.Dequeue(); return null; };
            Parser.ParseMethod divideParser = (Queue<string> arg) => { arg.Dequeue(); return null; };

            p.SetProperty(andName, andParser);
            p.SetProperty(divideName, divideParser);
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expected = typeof(ParseException);
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion ParseGrouper()


        #region ParseAnd()

        [TestMethod()]
        public void ParseAnd__Valid_Input__Assert_No_Throws()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            string[] source = { "and" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseAnd;

            Type expected = null;
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParseAnd__Valid_Input__Assert_One_Token_Consumed()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // realistic syntax; the first token is the and 
            string[] source = { "and", "store" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseAnd;
            p.SetField(Parser.NameOfTotal, source.Length);

            // the first token is consumed 
            int expected = tokens.Count - 1;


            //**  exercising the code  **//
            p.Invoke(method, tokens);


            //**  gathering results  **//
            int actual = tokens.Count;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParseAnd__Valid_Input__Assert_AndExpression_Returned()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // realistic syntax; the first token is the and 
            string[] source = { "and", "store" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseAnd;
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expType = typeof(AndExpression);


            //**  exercising the code  **//
            AExpression actual = (AExpression)p.Invoke(method, tokens);


            //**  gathering results  **//
            Type acType = actual.GetType();


            //**  testing  **//
            Assert.AreEqual(expType, acType);
        }

        #endregion ParseAnd()


        #region ParseDivide()

        [TestMethod()]
        public void ParseDivide__Valid_Input__Assert_No_Throws()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            string[] source = { "divide" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseDivide;

            Type expected = null;
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParseDivide__Valid_Input__Assert_One_Token_Consumed()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // realistic syntax; the first token is the divide 
            string[] source = { "divide", "store" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseDivide;
            p.SetField(Parser.NameOfTotal, source.Length);

            // the first token is consumed 
            int expected = tokens.Count - 1;


            //**  exercising the code  **//
            p.Invoke(method, tokens);


            //**  gathering results  **//
            int actual = tokens.Count;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParseDivide__Valid_Input__Assert_DivideExpression_Returned()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // realistic syntax; the first token is the divide 
            string[] source = { "divide", "store" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseDivide;
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expType = typeof(DivideExpression);


            //**  exercising the code  **//
            AExpression actual = (AExpression)p.Invoke(method, tokens);


            //**  gathering results  **//
            Type acType = actual.GetType();


            //**  testing  **//
            Assert.AreEqual(expType, acType);
        }

        #endregion ParseDivide()


        #region ParseStore()

        [TestMethod()]
        public void ParseStore__Valid_Input__Assert_No_Throws()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // the "store" and "[...]" together are the whole store 
            string[] source = { "store", "[12, 10, 16, 8]" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseStore;
            string nameOfNumberParser = PropertyFromMethod(Parser.NameOfParseNumbers);
            string nameOfStoreParser = PropertyFromMethod(Parser.NameOfParseStore);
            string nameOfTotal = Parser.NameOfTotal;

            // spoofed dependencies 
            Parser.ParseMethod numberParser = (Queue<string> arg) => { arg.Dequeue(); return new NumbersExpression(); };

            p.SetProperty(nameOfNumberParser, numberParser);
            p.SetField(nameOfTotal, source.Length);

            Type expected = null;
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParseStore__Valid_Input__Assert_Correct_Calls()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // the "store" and "[...]" are the store; "then" and "sum" are a following post-action 
            string[] source = { "store", "[12, 10, 16, 8]" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseStore;
            string nameOfNumberParser = PropertyFromMethod(Parser.NameOfParseNumbers);
            string nameOfTotal = Parser.NameOfTotal;

            List<string> expecteds = new List<string> { "numbers" };
            List<string> actuals = new List<string>();

            // spoofed dependencies 
            Parser.ParseMethod numberParser = (Queue<string> arg) => {
                arg.Dequeue(); actuals.Add("numbers"); return new NumbersExpression();
            };

            p.SetProperty(nameOfNumberParser, numberParser);
            p.SetField(nameOfTotal, source.Length);


            //**  exercising the code  **//
            p.Invoke(method, tokens);


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void ParseStore__Valid_Input__Assert_Correct_Subtree_Returned()  /* working */  {
            /* when a post action follows providing, the post action 
             * is returned, with providing as its child subtree */

            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // the "store" and "[...]" together are the whole store 
            string[] source = { "store", "[12, 10, 16, 8]" };
            Queue<string> tokens = new Queue<string>(source);

            #region names of Parser members, including target method

            string method = Parser.NameOfParseStore;
            string nameOfNumberParser = PropertyFromMethod(Parser.NameOfParseNumbers);
            string nameOfTotal = Parser.NameOfTotal;

            #endregion names of Parser members, including target method

            #region expected tree as data list

            AExpression expected = ReturnWithNullChildren(new StoreExpression(), 1);
            AExpression numbers = expected.Children[0] = new NumbersExpression();

            List<NodeFacts> expecteds = VisitNodeAndChildren(expected, null);


            #endregion expected tree as data list

            #region spoofed dependencies and other Parser members

            Parser.ParseMethod numberParser = (Queue<string> arg) => {
                arg.Dequeue(); return new NumbersExpression();
            };

            p.SetProperty(nameOfNumberParser, numberParser);
            p.SetField(nameOfTotal, source.Length);

            #endregion spoofed dependencies and other Parser members


            //**  exercising the code  **//
            AExpression actual = (AExpression)p.Invoke(method, tokens);


            //**  gathering results  **//
            List<NodeFacts> actuals = VisitNodeAndChildren(actual, null);


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void ParseStore__Invalid_Input_Not_Numbers__Assert_Throw()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // the "and" is not numbers, which have to come next after "store" 
            string[] source = { "store", "and" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseStore;
            string nameOfNumberParser = PropertyFromMethod(Parser.NameOfParseNumbers);
            string nameOfTotal = Parser.NameOfTotal;

            // spoofed dependencies 
            Parser.ParseMethod numberParser = (Queue<string> arg) => { arg.Dequeue(); return null; };

            p.SetProperty(nameOfNumberParser, numberParser);
            p.SetField(nameOfTotal, source.Length);

            Type expected = typeof(ParseException);
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion ParseStore()


        #region ParseNumbers()

        [TestMethod()]
        public void ParseNumbers__Valid_Input__Assert_No_Throws()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            string[] source = { "[8, 11, 22, 66]" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseNumbers;
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expected = null;
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParseNumbers__Valid_Input__Assert_One_Token_Consumed()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // realistic syntax; the first token is the numbers 
            string[] source = { "[8, 11, 22, 66]", "then", "sum" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseNumbers;
            p.SetField(Parser.NameOfTotal, source.Length);

            // the first token is consumed 
            int expected = tokens.Count - 1;


            //**  exercising the code  **//
            p.Invoke(method, tokens);


            //**  gathering results  **//
            int actual = tokens.Count;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ParseNumbers__Valid_Input__Assert_NumbersExpression_Returned()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            string[] source = { "[8, 11, 22, 77]" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParseNumbers;
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expType = typeof(NumbersExpression);
            string expText = source[0];


            //**  exercising the code  **//
            AExpression actual = (AExpression)p.Invoke(method, tokens);


            //**  gathering results  **//
            Type acType = actual.GetType();
            string acText = (actual as NumbersExpression)?.Numbers;


            //**  testing  **//
            Assert.AreEqual(expType, acType);
            Assert.AreEqual(expText, acText);
        }

        #endregion ParseNumbers()


        #region ParsePostAction()

        [TestMethod()]
        public void ParsePostAction__Valid_Input_Each_Token__Assert_No_Throws()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            string[] source = { "sum", "max", "min", "count", "average" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParsePostAction;
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expected = null;
            Type actual = null;

            Type[] expecteds = new Type[source.Length];
            Type[] actuals = new Type[source.Length];


            //**  exercising the code  **//
            // each token is tried by itself 
            for (int at = 0; at < source.Length; at++) {
                // to a single-element queue 
                string[] passer = { source[at] };
                tokens = new Queue<string>(passer);

                // actually exercising the code 
                try {
                    p.Invoke(method, tokens);
                }
                catch (Exception x) {
                    actual = x.GetType();
                }

                // same Type expected for all 
                expecteds[at] = expected;

                // current Type, thrown or default 
                actuals[at] = actual;
            }


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void ParsePostAction__Valid_Input_Each_Token__Assert_Token_Removed()  /* working */  {
            /* ParsePostAction() removes one token from the queue */

            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // one word is used for each iteration; tokens is reused 
            string[] source = { "sum", "max", "min", "count", "average" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParsePostAction;
            int countOfTokensEachTime = 2;
            p.SetField(Parser.NameOfTotal, countOfTokensEachTime);

            // all queues should have a single element after code is exercised 
            Queue<string> nextTokenOnly = new Queue<string>();
            nextTokenOnly.Enqueue("next");

            // arrays of queues; each iteration has its own queue 
            Queue<string>[] expecteds = new Queue<string>[source.Length];
            Queue<string>[] actuals = new Queue<string>[source.Length];


            //**  exercising the code  **//
            // each token is tried by itself 
            for (int at = 0; at < source.Length; at++) {
                // to a two-element queue 
                string[] passer = { source[at], "next" };
                tokens = new Queue<string>(passer);

                // actually exercising the code 
                p.Invoke(method, tokens);

                // same expected queue for all 
                expecteds[at] = nextTokenOnly;

                // current queue 
                actuals[at] = tokens;
            }


            //**  testing  **//
            // collections of collections can't be compared directly 
            for (int at = 0; at < expecteds.Length; at++) {
                Queue<string> expected = expecteds[at];
                Queue<string> actual = actuals[at];

                // comparing individual queues 
                CollectionAssert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void ParsePostAction__Valid_Input_Each_Token__Assert_Correct_Actions_Returned()  /* working */  {
            /* a new post action (in its more general form of "action") 
             * is always added at the root of the current subtree */

            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            // one word is used for each iteration; tokens is reused 
            string[] source = { "sum", "max", "min", "count", "average" };

            // the expression class of the tree's root is compared 
            List<Type> expecteds = new List<Type> {
                typeof(SumExpression), typeof(MaxExpression),
                typeof(MinExpression), typeof(CountExpression),
                typeof(AverageExpression)
            };

            List<Type> actuals = new List<Type>();

            string method = Parser.NameOfParsePostAction;

            int totalTokensQueued = 1;
            p.SetField(Parser.NameOfTotal, totalTokensQueued);


            //**  exercising the code  **//
            // each token is tried by itself 
            for (int at = 0; at < source.Length; at++) {
                // to a single-element queue 
                string[] passer = { source[at] };
                Queue<string> tokens = new Queue<string>(passer);

                // actually exercising the code 
                AExpression actual = (AExpression)p.Invoke(method, tokens);

                // to the actuals result 
                actuals.Add(actual.GetType());
            }


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void ParsePostAction__Invalid_Input_Unexpected_Token__Assert_Throw()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            string[] source = { "then" };
            Queue<string> tokens = new Queue<string>(source);

            string method = Parser.NameOfParsePostAction;
            p.SetField(Parser.NameOfTotal, source.Length);

            Type expected = typeof(ParseException);
            Type actual = null;


            //**  exercising the code  **//
            try {
                p.Invoke(method, tokens);
            }
            catch (Exception x) {
                actual = x.GetType();
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion ParsePostAction()

        #endregion Cross-recursive dependencies of Parse()


        #region IsANumbersToken(), a dependency of ParseStore()

        [TestMethod()]
        public void IsANumbersToken__Is_Numbers__Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            string token = "[2, 4, 6, 8]";
            string method = Parser.NameOfIsANumbersToken;

            bool expected = true;


            //**  exercising the code  **//
            bool actual = (bool)p.Invoke(method, token);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void IsANumbersToken__Not_Numbers__Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            Parser parser = new Parser();
            PrivateObject p = new PrivateObject(parser);

            string token = "divide";
            string method = Parser.NameOfIsANumbersToken;

            bool expected = false;


            //**  exercising the code  **//
            bool actual = (bool)p.Invoke(method, token);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion IsANumbersToken(), a dependency of ParseStore()

    }
}