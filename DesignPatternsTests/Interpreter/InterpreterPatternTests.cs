﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DesignPatterns.Interpreter;

namespace DesignPatternsTests.Interpreter
{
    [TestClass()]
    public class InterpreterPatternTests
    {
        #region Simple sentences

        [TestMethod()]
        public void Interpret__Known_Simple_Sentence_Sum__Assert_Correct_Results()  /* working */  {
            //**  groundwork  **//
            Interpreter_ interpreter = new Interpreter_();

            string sentence = "store [2, 3, 4, 5, 6] then sum then output";

            decimal expected = 2M + 3M + 4M + 5M + 6M;

            //**  exercising the code  **//
            decimal actual = interpreter.Interpret(sentence);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Interpret__Known_Simple_Sentence_Max__Assert_Correct_Results()  /* working */  {
            //**  groundwork  **//
            Interpreter_ interpreter = new Interpreter_();

            string sentence = "store [2, 3, 4, 5, 6] then max then output";

            decimal expected = 6M;

            //**  exercising the code  **//
            decimal actual = interpreter.Interpret(sentence);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Interpret__Known_Simple_Sentence_Min__Assert_Correct_Results()  /* working */  {
            //**  groundwork  **//
            Interpreter_ interpreter = new Interpreter_();

            string sentence = "store [2, 3, 4, 5, 6] then min then output";

            decimal expected = 2M;

            //**  exercising the code  **//
            decimal actual = interpreter.Interpret(sentence);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Interpret__Known_Simple_Sentence_Count__Assert_Correct_Results()  /* working */  {
            //**  groundwork  **//
            Interpreter_ interpreter = new Interpreter_();

            string sentence = "store [2, 3, 4, 5, 6] then count then output";

            decimal expected = 5M;

            //**  exercising the code  **//
            decimal actual = interpreter.Interpret(sentence);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Interpret__Known_Simple_Sentence_Average__Assert_Correct_Results()  /* working */  {
            //**  groundwork  **//
            Interpreter_ interpreter = new Interpreter_();

            string sentence = "store [2, 3, 4, 5, 6] then average then output";

            decimal expected = (2M + 3M + 4M + 5M + 6M) / 5M;

            //**  exercising the code  **//
            decimal actual = interpreter.Interpret(sentence);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Simple sentences


        #region Complex sentences

        [TestMethod()]
        public void Interpret__Known_Complex_Sentence_And__Assert_Correct_Results()  /* working */  {
            //**  groundwork  **//
            Interpreter_ interpreter = new Interpreter_();
            string sentence = "( store [24, 57] and store [35, 68] ) then average then output";

            decimal[] numbers = { 24M, 57M, 35M, 68M };
            decimal expected = numbers.Average();


            //**  exercising the code  **//
            decimal actual = interpreter.Interpret(sentence);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Interpret__Known_Complex_Sentence_Divide__Assert_Correct_Results()  /* working */  {
            //**  groundwork  **//
            Interpreter_ interpreter = new Interpreter_();
            string sentence = "( store [24, 57] divide store [35] ) then sum then output";

            decimal expected = (24M + 57M) / 35M;


            //**  exercising the code  **//
            decimal actual = interpreter.Interpret(sentence);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Complex sentences


        #region Polycomplex sentences

        [TestMethod()]
        public void Interpret__Known_Polycomplex_Sentence__Assert_Correct_Results()  /* working */  {
            /* the sentence here is polycomplex because it parses to nested subtrees */

            //**  groundwork  **//
            Interpreter_ interpreter = new Interpreter_();
            string sentence = "( ( store [24, 57] and store [16, 28] ) and ( store [10, 12] divide store [8] ) ) then sum then output";

            decimal[] lSubtreeNumbers = { 24M, 57M, 16M, 28M };
            decimal lSubtreeSum = lSubtreeNumbers.Sum();
            decimal rSubtreeResult = (10M + 12M) / 8M;

            decimal expected = lSubtreeSum + rSubtreeResult;


            //**  exercising the code  **//
            decimal actual = interpreter.Interpret(sentence);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Polycomplex sentences

    }
}