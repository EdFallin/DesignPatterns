﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DesignPatterns.Interpreter;

namespace DesignPatternsTests.Interpreter
{
    [TestClass()]
    public class LexerTests
    {
        [TestMethod()]
        public void Tokenize__Known_Simple_Sentence__Assert_Correct_Tokens()  /* working */  {
            //**  groundwork  **//
            Lexer lexer = new Lexer();

            string text = "average then sum then output";

            string[] expected = { "average", "then", "sum", "then", "output" };


            //**  exercising the code  **//
            string[] actual = lexer.Tokenize(text);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Tokenize__Numbers_In_Middle__Assert_Correct_Tokens()  /* working */  {
            //**  groundwork  **//
            Lexer lexer = new Lexer();

            string text = "store [2, 11, 10, 8, 9] then sum";

            string[] expected = { "store", "[2, 11, 10, 8, 9]", "then", "sum" };


            //**  exercising the code  **//
            string[] actual = lexer.Tokenize(text);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Tokenize__Single_Element_Numbers__Assert_Correct_Tokens()  /* working */  {
            //**  groundwork  **//
            Lexer lexer = new Lexer();

            string text = "store [6] then sum";

            string[] expected = { "store", "[6]", "then", "sum" };


            //**  exercising the code  **//
            string[] actual = lexer.Tokenize(text);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Tokenize__Includes_Parens__Assert_Correct_Tokens()  /* working */  {
            //**  groundwork  **//
            Lexer lexer = new Lexer();

            string text = "( store [2, 11, 10, 8, 9] and store [2, 5, 6, 8] ) then sum";

            string[] expected = { "(", "store", "[2, 11, 10, 8, 9]", "and", "store", "[2, 5, 6, 8]", ")", "then", "sum" };


            //**  exercising the code  **//
            string[] actual = lexer.Tokenize(text);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}