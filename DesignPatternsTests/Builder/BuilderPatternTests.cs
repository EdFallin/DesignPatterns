﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Builder;

namespace DesignPatternsTests.Builder
{
    [TestClass]
    public class BuilderPatternTests
    {
        #region Definitions
        
        private readonly string NL = Environment.NewLine;

        #endregion Definitions


        #region Fixtures

        public Queue<Token> TokenString()  /* verified */  {
            Queue<Token> queue = new Queue<Token>();

            queue.Enqueue(new Token(TokenType.SectionStart, null));
            queue.Enqueue(new Token(TokenType.StylingStart, "big"));
            queue.Enqueue(new Token(TokenType.Text, "Ed's Secret Website"));
            queue.Enqueue(new Token(TokenType.StylingEnd, "big"));
            queue.Enqueue(new Token(TokenType.SectionEnd, null));

            queue.Enqueue(new Token(TokenType.SectionStart, "What This Is"));
            queue.Enqueue(new Token(TokenType.Text, "Here is where I put "));
            queue.Enqueue(new Token(TokenType.StylingStart, "emphatic"));
            queue.Enqueue(new Token(TokenType.Text, "secret"));
            queue.Enqueue(new Token(TokenType.StylingEnd, "emphatic"));
            queue.Enqueue(new Token(TokenType.Text, " things."));
            queue.Enqueue(new Token(TokenType.SectionEnd, null));

            queue.Enqueue(new Token(TokenType.SectionStart, null));
            queue.Enqueue(new Token(TokenType.Text, "Secret Image"));
            queue.Enqueue(new Token(TokenType.Image, "~/secrets/images/secret.png"));
            queue.Enqueue(new Token(TokenType.SectionEnd, null));

            queue.Enqueue(new Token(TokenType.SectionStart, null));
            queue.Enqueue(new Token(TokenType.StylingStart, "whispery"));
            queue.Enqueue(new Token(TokenType.Text, "Secrets,"));
            queue.Enqueue(new Token(TokenType.Text, " secrets,"));
            queue.Enqueue(new Token(TokenType.Text, " and more secrets."));
            queue.Enqueue(new Token(TokenType.StylingEnd, "list"));
            queue.Enqueue(new Token(TokenType.SectionEnd, null));

            return (queue);
        }

        public string ExpectedHtml  /* verified */  {
            get {
                StringBuilder builder = new StringBuilder();

                builder.Append("<div>");
                builder.Append("<style class=\"big\" >");
                builder.Append("Ed's Secret Website");
                builder.Append("</style>");
                builder.Append("</div>");

                builder.Append("<div id=\"What This Is\" >");
                builder.Append("Here is where I put ");
                builder.Append("<style class=\"emphatic\" >");
                builder.Append("secret");
                builder.Append("</style>");
                builder.Append(" things.");
                builder.Append("</div>");

                builder.Append("<div>");
                builder.Append("Secret Image");
                builder.Append("<img src=\"~/secrets/images/secret.png\" />");
                builder.Append("</div>");

                builder.Append("<div>");
                builder.Append("<style class=\"whispery\" >");
                builder.Append($"Secrets, secrets, and more secrets.");
                builder.Append("</style>");
                builder.Append("</div>");

                return (builder.ToString());
            }
        }

        public string ExpectedPlainText  /* verified */  {
            get {
                StringBuilder builder = new StringBuilder();

                builder.AppendLine("Ed's Secret Website");
                builder.Append(NL);
                builder.AppendLine("What This Is");
                builder.AppendLine("Here is where I put secret things.");
                builder.Append("Secret Image");
                builder.Append(NL);
                builder.AppendLine("Image found at \"~/secrets/images/secret.png\"");
                builder.Append(NL);
                builder.Append($"Secrets, secrets, and more secrets.");
                builder.Append(NL);

                return (builder.ToString());
            }
        }

        #endregion Fixtures


        //** * * * * * * * **//


        #region Director .Build()

        [TestMethod()]
        public void Build__HtmlBuilder__Assert_Expected_Html()  /* working */  {
            //**  groundwork  **//
            ABuilder builder = new HtmlBuilder();
            Director director = new Director(builder);

            Queue<Token> input = this.TokenString();

            string expected = this.ExpectedHtml;


            //**  exercising the code  **//
            string actual = director.Build(input);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Build__PlainTextBuilder__Assert_Expected_Plain_Text()  /* working */  {
            //**  groundwork  **//
            ABuilder builder = new PlainTextBuilder();
            Director director = new Director(builder);

            Queue<Token> input = this.TokenString();

            string expected = this.ExpectedPlainText;


            //**  exercising the code  **//
            string actual = director.Build(input);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Director .Build()


        #region Director .DeleBuild()

        [TestMethod()]
        public void DeleBuild__HtmlBuilder__Assert_Expected_Html()  /* working */  {
            //**  groundwork  **//
            ABuilder builder = new HtmlBuilder();
            Director director = new Director(builder);

            Queue<Token> input = this.TokenString();

            string expected = this.ExpectedHtml;


            //**  exercising the code  **//
            string actual = director.DeleBuild(input);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void DeleBuild__PlainTextBuilder__Assert_Expected_Plain_Text()  /* working */  {
            //**  groundwork  **//
            ABuilder builder = new PlainTextBuilder();
            Director director = new Director(builder);

            Queue<Token> input = this.TokenString();

            string expected = this.ExpectedPlainText;


            //**  exercising the code  **//
            string actual = director.DeleBuild(input);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Director .DeleBuild()


    }
}
