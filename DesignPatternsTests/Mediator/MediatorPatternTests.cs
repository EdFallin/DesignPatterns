﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DesignPatterns;
using DesignPatterns.Mediator;

namespace DesignPatternsTests.Mediator
{
    [TestClass]
    public class MediatorPatternTests
    {
        [TestMethod()]
        public void Mediator_Pattern__Known_Happenings__Assert_Expected_States()  /* working */  {
            //**  groundwork  **//
            CommerceMediator mediator = new CommerceMediator();

            /* colleagues whose interactions are to be mediated; keeping it to a minimum for easier results */
            mediator.AddStore(new Store("Store 1"));
            mediator.AddWarehouse(new Warehouse("Warehouse A"));
            mediator.AddFactory(new Factory("Factory I"));

            /* aliasing for initiating state changes and for testing state afterward */
            Store store = mediator.Stores[0];
            Warehouse warehouse = mediator.Warehouses[0];
            Factory factory = mediator.Factories[0];


            //**  exercising the code  **//
            store.Sell("cat", 5); // --> store::'sold' --> warehouse.ship = warehouse::'shipped', h:'sold' --> store::'receiving', h:'shipped' 
            factory.Produce("snake", 12);  // --> factory::'produced' --> warehouse.receive::'received', h:'produced' --> factory::'ready' 


            //**  testing  **//
            Assert.AreEqual("Store 1 receiving 5 cats", store.State);  // state after cats sold, then new shipment of cats 
            Assert.AreEqual("On: Warehouse A shipped 5 cats;", store.History);  // transition to this state 

            /* earlier warehouse state replaced by later state, but its past state reflected in store history */

            Assert.AreEqual("Warehouse A received 12 snakes", warehouse.State);  // state after snakes produced by factory, then received by warehouse 
            Assert.AreEqual("On: Store 1 sold 5 cats; On: Factory I produced 12 snakes;", warehouse.History);  // old transition to shipped cats, then transition to this state 

            Assert.AreEqual("Factory I ready to make 12 snakes", factory.State);  // state after snakes shipped 
            Assert.AreEqual("On: Warehouse A received 12 snakes;", factory.History);  // transition to this state 
        }

    }
}
