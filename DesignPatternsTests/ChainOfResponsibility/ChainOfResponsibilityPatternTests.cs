﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.ChainOfResponsibility;

namespace DesignPatternsTests.ChainOfResponsibility
{
    [TestClass]
    public class ChainOfResponsibilityPatternTests
    {
        [TestMethod()]
        public void Pattern__Sentence_Handles__Assert_Sentence_Answer()  /* working */  {
            //**  groundwork  **//
            string question = "This is a sentence.";

            NumberInfoNode numberNode1 = new NumberInfoNode();
            WordInfoNode wordNode1 = new WordInfoNode();
            NumberInfoNode numberNode2 = new NumberInfoNode();
            SentenceInfoNode sentenceNode = new SentenceInfoNode();
            WordInfoNode wordNode2 = new WordInfoNode();  // should not be reached 

            string expected = "Your question was a sentence.";


            //**  exercising the code  **//
            /* linking chain */
            numberNode1.LinkTo(wordNode1);
            wordNode1.LinkTo(numberNode2);
            sentenceNode.LinkFrom(numberNode2);
            wordNode2.LinkFrom(sentenceNode);

            /* using chain to pass responsibility */
            string answer = numberNode1.Answer(question);


            //**  testing  **//
            Assert.AreEqual(expected, answer);
        }

        [TestMethod()]
        public void Pattern__Number_Handles__Assert_Number_Answer()  /* working */  {
            //**  groundwork  **//
            string question = "85627938";

            WordInfoNode wordNode1 = new WordInfoNode();
            SentenceInfoNode sentenceNode = new SentenceInfoNode();
            WordInfoNode wordNode2 = new WordInfoNode();
            NumberInfoNode numberNode1 = new NumberInfoNode();

            string expected = "Numbers are the answer.";


            //**  exercising the code  **//
            /* linking chain */
            wordNode1.LinkTo(sentenceNode);
            sentenceNode.LinkTo(wordNode2);
            numberNode1.LinkFrom(wordNode2);

            /* using chain to pass responsibility */
            string answer = numberNode1.Answer(question);


            //**  testing  **//
            Assert.AreEqual(expected, answer);
        }

        [TestMethod()]
        public void Pattern__Word_Handles__Assert_Word_Answer()  /* working */  {
            //**  groundwork  **//
            string question = "AllAsOneWord";

            NumberInfoNode numberNode1 = new NumberInfoNode();
            NumberInfoNode numberNode2 = new NumberInfoNode();
            SentenceInfoNode sentenceNode1 = new SentenceInfoNode();
            WordInfoNode wordNode = new WordInfoNode();
            SentenceInfoNode sentenceNode2 = new SentenceInfoNode();  // should not be reached 

            string expected = "Word!";


            //**  exercising the code  **//
            /* linking chain */
            numberNode1.LinkTo(numberNode2);
            numberNode2.LinkTo(sentenceNode1);
            wordNode.LinkFrom(sentenceNode1);
            sentenceNode2.LinkFrom(wordNode);

            /* using chain to pass responsibility */
            string answer = numberNode1.Answer(question);


            //**  testing  **//
            Assert.AreEqual(expected, answer);
        }

        [TestMethod()]
        public void Pattern__First_Object_Handles__Assert_Expected_Answer()  /* working */  {
            //**  groundwork  **//
            string question = "NoDigitsInThisText";

            WordInfoNode wordNode1 = new WordInfoNode();  // ~question contains number, so this should answer 
            NumberInfoNode numberNode2 = new NumberInfoNode();
            SentenceInfoNode sentenceNode1 = new SentenceInfoNode();
            WordInfoNode wordNode2 = new WordInfoNode();
            SentenceInfoNode sentenceNode2 = new SentenceInfoNode();

            string expected = "Word!";


            //**  exercising the code  **//
            /* linking chain */
            wordNode1.LinkTo(numberNode2);
            numberNode2.LinkTo(sentenceNode1);
            wordNode2.LinkFrom(sentenceNode1);
            sentenceNode2.LinkFrom(wordNode2);

            /* using chain to pass responsibility */
            string answer = wordNode1.Answer(question);


            //**  testing  **//
            Assert.AreEqual(expected, answer);
        }

        [TestMethod()]
        public void Chaining__Default_Handles__Assert_Default_Answer()  /* working */  {
            //**  groundwork  **//
            /* numbers but also spaces & chars, so a sentence; no sentence node in chain */
            string question = "Some 578495s mixed in.";

            NumberInfoNode numberNode1 = new NumberInfoNode();
            NumberInfoNode numberNode2 = new NumberInfoNode();
            WordInfoNode wordNode = new WordInfoNode();

            string expected = "There is no answer.";


            //**  exercising the code  **//
            /* linking chain */
            numberNode1.LinkTo(numberNode2);
            wordNode.LinkFrom(numberNode2);

            /* using chain to pass responsibility */
            string answer = numberNode1.Answer(question);


            //**  testing  **//
            Assert.AreEqual(expected, answer);
        }

    }
}
