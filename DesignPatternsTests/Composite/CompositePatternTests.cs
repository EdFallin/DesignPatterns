﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DesignPatterns;
using DesignPatterns.Composite;

namespace DesignPatternsTests.Composite
{
    [TestClass]
    public class CompositePatternTests
    {
        [TestMethod()]
        public void Composite_Pattern__Known_Structure__Assert_Expected_Output()  /* working */  {
            /* to test the Composite pattern, you just build a structure of Composite nodes 
             * and call the compositing method on the top / first one */

            //**  groundwork  **//
            #region Nodes for Composite structure

            /* root of tree */
            AParseNode parseRoot = new StructuralNode();

            /* ops before the branching */
            AParseNode numberA = new NumberNode("5");
            AParseNode identB = new IdentifierNode("Some-Three");
            AParseNode opcodeC = new OperatorNode("MUL");  // * 

            /* branching itself */
            AParseNode branchD = new BranchNode();

            /* true branch */
            AParseNode trueBranchE = new TrueBranchNode("end-of-false");
            AParseNode identF = new IdentifierNode("Some-Big");
            AParseNode identG = new IdentifierNode("Some-Small");
            AParseNode opCodeH = new OperatorNode("MIN");  // - 

            /* false branch */
            AParseNode falseBranchI = new FalseBranchNode();
            AParseNode numberJ = new NumberNode("8");
            AParseNode numberK = new NumberNode("10");
            AParseNode opCodeL = new OperatorNode("ADD");  // + 

            #endregion Nodes for Composite structure

            #region Constructing Composite structure

            parseRoot.Add(numberA, identB, opcodeC);  // before branch 
            parseRoot.Add(branchD);

            /* branches are children of branch structure, their ops children of branches */
            branchD.Add(trueBranchE, falseBranchI);

            trueBranchE.Add(identF, identG, opCodeH);
            falseBranchI.Add(numberJ, numberK, opCodeL);

            #endregion Constructing Composite structure

            #region Expected, as series of lines

            string expected = string.Join(
                Environment.NewLine,
                new string[] {
                    "5 to register;", "get @address for Some-Three to register;",
                    "get value at @address to register;", "MUL from register and register to register;",
                    "compare value at @address with register and save to register;",
                    "if register is all 000s jump to @jump-not-equal;", "when true ops ops ops;",
                    "get @address for Some-Big to register;", "get value at @address to register;",
                    "get @address for Some-Small to register;", "get value at @address to register;",
                    "MIN from register and register to register;", "long jump to @this-line + end-of-false;",
                    "when false first ops ops ops;", "8 to register;", "10 to register;",
                    "ADD from register and register to register;", "when false last ops ops ops;",
                    string.Empty  // for a final hard return at end 
                });

            #endregion Expected, as series of lines


            //**  exercising the code  **//
            string actual = parseRoot.ToAssembler();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

    }
}
